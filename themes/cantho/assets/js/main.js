'use strict';

(function ($) {
    $(document).ready(function () {
        let $body = $('body');
        let $html = $('html');
        let $window = $(window).eq(0);
        let $document = $(document).eq(0);

        toggleMenu();

        // loading all images of the page
        // $body.waitForImages(function () {
        //     setTimeout(function () {
        //         $('.ct-b-loading-wrapper').fadeOut(700, function () {
        //             $(this).remove();
        //             $body.addClass('add-header');
        //             $body.addClass('add-bottom-navi');
        //         });
        //     }, 700)
        //     // animation
        // });

        // Show menu for mobile
        $('#icon-menu').click(function () {
            $('div.menu-wrap').addClass('show-menu');
        })

        // Close menu for mobile
        $('.btn-close').click(function () {
            $('div.menu-wrap').removeClass('show-menu');
        });

        //close popup
        $(' html').on('click', '.ct-close-popup-content', function (e) {
            e.preventDefault();
            $('.ct-popup-confirm').remove();
        })

        $('html').click(function (e) {
            if ($(e.target).parents('.ct-popup-content').length == 0) {
                $('.ct-popup-confirm').remove();
            }
        });


        // menu lang
        $('.js-toggle-lang').on('click', function (e) {
            $('header').toggleClass('open-lang');
        })

        $('html').click(function (e) {
            if ($(e.target).parents('.ct-lang').length == 0) {
                $('header').removeClass('open-lang');
            }
        });

        $('.js-close-popup').on('click', function (e) {
            $('.ct-b-popup-wraper').remove();
        })

        $('html').click(function (e) {
            if ($(e.target).parents('.ct-b-popup-wraper .ct-img').length == 0) {
                $('.ct-b-popup-wraper').remove();
            }
        });

        

        // hotspot click
        $('html').on('click', '.js-hotspot-click', function (e) {
            e.preventDefault();
            $('.ct-hotspot').removeClass('active');
            let data = $(this).data('hotspot');
            $('.open-popup-' + data).toggleClass('active');
        })

        $('.js-thamquan').on('click', function (e) {
            if (!$body.hasClass('homepage')) {
                $('html, body').animate({
                    scrollTop: $(".ct-thamquan").offset().top
                }, 500);
            }

        })

        $('.js-back-to-top').on('click', function (e) {
            if (!$body.hasClass('homepage')) {
                $('html, body').animate({
                    scrollTop: 0,
                }, 500);
            }
        })

        // Slick slider function for home page
        $('.ct-home-slider').slick({
            dots: false,
            infinite: true,
            autoplaySpeed: 3000,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            cssEase: 'ease-in-out',
            pauseOnHover: false,
        });

        $('.ct-slider').slick({
            dots: false,
            infinite: true,
            autoplaySpeed: 4000,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            cssEase: 'ease-in-out',
            pauseOnHover: false,
        });


        $('.ct-content-slider').slick({
            dots: false,
            infinite: true,
            draggable: false,
            nextArrow: '<button class="btn slider-right-btn"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn slider-left-btn"><i class="fa fa-angle-left"></i></button>',
        });




        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            swipe: false,
        });
        $('.slider-nav').slick({
            asNavFor: '.slider-for',
            focusOnSelect: true,
            dots: false,
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: '<button class="btn slider-right-btn"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn slider-left-btn"><i class="fa fa-angle-left"></i></button>',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            swipe: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 4,
                    }
                }
            ]
        });


        $('.slider-for-gallery').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav-gallery',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            accessibility: false,
            swipe: false,
        });

        $('.slider-nav-gallery').slick({
            asNavFor: '.slider-for-gallery',
            focusOnSelect: true,
            dots: false,
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            nextArrow: '<button class="btn slider-right-btn"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn slider-left-btn"><i class="fa fa-angle-left"></i></button>',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        draggable: false,
                        draggable: false,
                        swipeToSlide: false,
                        touchMove: false,
                        accessibility: false,
                        swipe: false,
                    }
                }
            ]
        });


        $('.slider-for-gallery-video').slick({
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav-gallery-video',
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            accessibility: false,
            swipe: false,
        });

        $('.slider-nav-gallery-video').slick({
            asNavFor: '.slider-for-gallery-video',
            focusOnSelect: true,
            dots: true,
            infinite: true,
            arrows: false,
            slidesToShow: 4,
            slidesToScroll: 2,
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        draggable: false,
                        draggable: false,
                        swipeToSlide: false,
                        touchMove: false,
                        accessibility: false,
                        swipe: false,
                    }
                }
            ]
        });

        $('.ct-slide-gallery').slick({
            dots: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            rows: 2,
            draggable: false,
            swipeToSlide: false,
            touchMove: false,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        rows: 2,
                        dots: true,
                        draggable: false,
                        swipeToSlide: false,
                        touchMove: false,
                        accessibility: false,
                        swipe: false,
                    }
                }
            ]
        });

        $('.slider-news').slick({
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            rows: 2,
            arrows: true,
            nextArrow: '<button class="btn slider-right-btn"><i class="fa fa-angle-right"></i></button>',
            prevArrow: '<button class="btn slider-left-btn"><i class="fa fa-angle-left"></i></button>',
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        centerPadding: '40px',
                        slidesToShow: 1,
                        arrows: false,
                        rows: 3,
                        dots: true,
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        centerPadding: '40px',
                        slidesToShow: 1,
                        arrows: false,
                        rows: 2,
                        dots: true,
                    }
                }
            ]
        });

        $('.ct-facility-list-slider').slick({
            dots: false,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 2,
            arrows: false,
            rows: 2,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots: true,
                        rows: 1,
                    }
                }
            ]
        });

        $('.ct-type-house-slider').slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 2,
            arrows: false,
            rows: 2,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        dots: true,
                        infinite: true,
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false,
                        rows: 4,
                    }
                }
            ]
        });


        $('.ct-normal-slider').slick({
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
        });

        // calculate hotpot's positions for location page.
        var calcHotpotPos = function (map, checkWidth) {
            var sectionWrapper = map;
            var checkWidth = checkWidth;
            var scrollArea = map.parent();
            var sectionBg = new Image;
            sectionBg.src = sectionWrapper.attr('data-bg');
            var pxSize = parseInt(sectionWrapper.attr('data-px-ratio'));
            var originBgWidth, originBgHeight;
            var hotpots = sectionWrapper.find('.ct-hotspot');
            var curBg = map.find('.js-current-bg');
            var sectionWrapperX, sectionWrapperY, curBgWidth, curBgHeight, lastX, gapX;
            var mapRatio = curBg.width() / $window.width();

            var updateX = function (newX, newY) {
                sectionWrapper.css({
                    'margin-left': newX + 'px',
                    'margin-top': newY + 'px'
                });
            }

            var dragBg = function (x, y) {
                curBgWidth = curBg.width();
                var winWidth = $window.width();
                var minX, maxX;

                if (curBgWidth > winWidth) {
                    minX = -winWidth / 2;
                    maxX = winWidth / 2 - curBgWidth;
                    sectionWrapperX += (x - y) * mapRatio;
                    if (sectionWrapperX >= minX) {
                        sectionWrapperX = minX;
                    } else {
                        if (sectionWrapperX <= maxX) {
                            sectionWrapperX = maxX;
                        }
                    }
                    updateX(sectionWrapperX);
                }

                lastX = x;
            }

            sectionBg.onload = function (e) {
                curBgWidth = curBg.width();
                curBgHeight = curBg.height();
                var winW = $window.width();
                if (winW > curBgWidth && checkWidth == true) {
                    var scaleRatio = winW / curBgWidth;
                    curBgHeight = curBgHeight * scaleRatio;
                    curBgWidth = winW;
                    curBg.width(curBgWidth);
                    curBg.height(curBgHeight);
                }

                sectionWrapper.width(curBgWidth);
                sectionWrapper.height(curBgHeight);
                originBgWidth = sectionBg.width;
                originBgHeight = sectionBg.height;

                var scaleX = curBgWidth * pxSize / originBgWidth;
                var scaleY = curBgHeight * pxSize / originBgHeight;
                var scale = scaleX > scaleY ? scaleX : scaleY;
                sectionWrapperX = -curBgWidth / 2;
                sectionWrapperY = -curBgHeight / 2;
                updateX(sectionWrapperX, sectionWrapperY);
                hotpots.each(function () {
                    scaleMap.call(this, scale);
                });
            }

            // touchstart or mousedown
            // var canTouch = ('ontouchstart' in document.documentElement)  ? 'yes' : 'no';
            var canTouch = $('html').hasClass('mobile');
            $window.resize(function (e) {
                e.preventDefault();
                setTimeout(function () {
                    curBg.removeAttr('style');
                    sectionWrapper.removeAttr('style');
                    curBgWidth = curBg.width();
                    curBgHeight = curBg.height();
                    var winW = $window.width();
                    if (winW > curBgWidth && checkWidth == true) {
                        var scaleRatio = winW / curBgWidth;
                        curBgHeight = curBgHeight * scaleRatio;
                        curBgWidth = winW;
                        curBg.width(curBgWidth);
                        curBg.height(curBgHeight);
                    }

                    mapRatio = curBg.width() / $window.width();
                    sectionWrapper.width(curBgWidth);
                    sectionWrapper.height(curBgHeight);
                    var scaleX = curBgWidth * pxSize / originBgWidth;
                    var scaleY = curBgHeight * pxSize / originBgHeight;
                    var scale = scaleX > scaleY ? scaleX : scaleY;
                    sectionWrapperX = -curBgWidth / 2;
                    sectionWrapperY = -curBgHeight / 2;
                    updateX(sectionWrapperX, sectionWrapperY);
                    hotpots.each(function () {
                        scaleMap.call(this, scale);
                    });

                }, 100);




            });
            // drag the map
            if (canTouch) {
                scrollArea.on({
                    'touchstart': function (e) {
                        lastX = e.changedTouches[0].pageX;
                        // $.fn.fullpage.setAutoScrolling(false);
                    },
                    'touchmove': function (e) {
                        var curX = e.changedTouches[0].pageX;
                        dragBg(curX, lastX);
                    },
                    'touchend': function (e) {
                        // $.fn.fullpage.setAutoScrolling(true);
                    }
                });
            } else {
                sectionWrapper.draggable({
                    axis: "x",
                    start: function (e) {
                        lastX = e.pageX;
                    },
                    drag: function (e) {
                        var curX = e.pageX;
                        dragBg(curX, lastX);
                    },
                    stop: function () { }
                });
            }


        }

        // hotspot
        var scaleMap = function (scale) {
            var $this = $(this);
            var xy = $this.attr('data-xy').split(' ');
            var transX = parseFloat(xy[0]);
            var transY = parseFloat(xy[1]);
            var offsetX = $this.width() - $window.width();
            var offsetY = $this.height() - $window.height();
            if (offsetX > 0) {
                transX += (-offsetX / 2) / scale;
            }
            $this.css({
                'transform': 'scale(' + scale + ') translate( ' + transX + 'px,' + transY + 'px)',
                '-webkit-transform': 'scale(' + scale + ') translate( ' + transX + 'px,' + transY + 'px)',
                'moz-transform': 'scale(' + scale + ') translate( ' + transX + 'px,' + transY + 'px)',
            });
        }

        var hotspot = function () {
            var sectionWrapper = $('.ct-location-map');
            var hotpots = $('.ct-hotspot');
            var sectionBg = new Image;
            var imgRealWidth, imgRealHeight, curWidth, curHeight, mapRatio;
            var $window = $(window);
            sectionBg.onload = function (e) {
                imgRealWidth = sectionBg.width;
                imgRealHeight = sectionBg.height;
                curWidth = $('.ct-location-map img').width();
                curHeight = $('.ct-location-map img').height();
                $('.ct-b-hotspot').css({
                    'width': curWidth
                });

                mapRatio = curWidth / imgRealWidth;
                $('.ct-b-hotspot').width(curWidth);
                $('.ct-b-hotspot').height(curHeight);
                var scaleX = curWidth / imgRealWidth;
                var scaleY = curHeight / imgRealHeight;
                var scale = scaleX > scaleY ? scaleX : scaleY;
                hotpots.each(function () {
                    scaleMap.call(this, scale);
                });

            }
            sectionBg.src = sectionWrapper.attr('data-bg');
        }
        if ($body.hasClass('homepage') || $body.hasClass('facilitypage') || $body.hasClass('typeshousespage')) {
            hotspot();
        }
        $(window).resize(function () {
            setTimeout(function () {
                if ($body.hasClass('homepage') || $body.hasClass('facilitypage') || $body.hasClass('typeshousespage')) {
                    hotspot();
                }
            }, 100);
        });


        $('#fullpage').fullpage({
            navigation: true,
            navigationPosition: 'left',
            lockAnchors: false,
            verticalCentered: false,
            scrollingSpeed: 1000,
            // scrollOverflow: true,
            // responsive: 1280,
            anchors: ['trangchu', 'gioithieu', 'vitri', 'tienich', 'lido', 'matbang', 'maunha', 'tiendo', 'tintuc', 'thuvien', 'thamquanduan', 'lienhe'],


            onLeave: function (origin, destination, direction) {

                var width = $window.width();

                if (destination === 1) {
                    $body.addClass('add-bottom-navi');
                }
                if (destination === 2) {
                    animationFadeInUpIntrodution('.ct-s-introdution .ct-b-foot .ct-row');
                    $body.addClass('add-bottom-navi');
                }

                if (destination === 3) {
                    animationFadeInUp('.ct-s-location .ct-b-body .ct-foot .ct-item');
                    mapLeftToRight('.ct-s-location .ct-b-map img');
                    $body.addClass('add-bottom-navi');

                }

                if (destination === 4) {
                    animationFadeInUp('.ct-s-facilities .ct-box-item ');
                    $body.addClass('add-bottom-navi');
                }

                if (destination === 5) {
                    animationFadeInUp('.ct-s-facilities-2 .ct-reason-item ');
                    $body.addClass('add-bottom-navi');

                }
                if (destination === 6) {
                    $body.addClass('add-bottom-navi');
                }

                if (destination === 7) {
                    $body.addClass('add-bottom-navi');
                }

                if (destination === 8) {
                    $body.addClass('add-bottom-navi');
                    // $body.addClass('fp-nav-white');

                } else {
                    // $body.removeClass('fp-nav-white');

                }

                if (destination === 9) {
                    $body.addClass('add-bottom-navi');
                    animationFadeInUp('.ct-s-home-news .item');

                }

                if (destination === 10) {
                    $body.addClass('add-bottom-navi');
                    animationFadeInUpGallery('.ct-s-gallery .js-img-popup');
                }
                if (destination === 11) {
                    $body.addClass('add-bottom-navi');
                }
                if (destination === 12) {
                    $body.removeClass('add-bottom-navi');
                }
            },
        });
        // detect screen size
        $body.layoutController({
            onSmartphone: function () {
            },
            onTablet: function () {
            },
            onDesktop: function () {

            }
        });


        // popup img
        $('.js-img-popup, .ct-process-popup').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            zoom: {
                enabled: true,
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out' // CSS transition
            }
        });

        //popup for video
        $('.js-iframe').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'iframe',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            zoom: {
                enabled: true,
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out' // CSS transition
            }
        });


        $(document).ready(function() {
            $('.flexiContactForm button').prop('disabled', true);
            $('.flexiContactForm input').keyup(function() {
               if($(".flexiContactForm input[name='name']").val() != '' 
               && $(".flexiContactForm input[name='email']").val() != '' 
               && $(".flexiContactForm input[name='phone']").val() != '') {
                $('.flexiContactForm button').prop('disabled', false);
               } else {
                $('.flexiContactForm button').prop('disabled', true);
               }
            });



        });


        $('.js-frame').click(function(){
            for(i = 0; i < 20; i++) {
                if($('.ct-frame-video')[i]){
                    $('.ct-frame-video')[i].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');

                }
            }
        });






    });//end document ready

    // animation
    function animationFadeInUp(element) {
        TweenMax.staggerFrom(element, 1, { opacity: 0, y: 30, delay: 0.1, ease: Power2.easeOut }, 0.2)
    }

    function animationFadeInUpIntrodution(element) {
        TweenMax.staggerFrom(element, 1, { opacity: 0, y: 30, delay: 0.1, ease: Power2.easeOut }, 0.4)
    }

    function animationFadeInUpGallery(element) {
        TweenMax.staggerFrom(element, 1, { opacity: 0, y: 20, delay: 0, ease: Power2.easeOut }, 0.1)
    }

    function fadeInUp(element) {
        var sttFrom = {
            opacity: 0,
            y: 50,
            delay: 0.5,
            ease: Power2.easeOut
        };
        TweenMax.staggerFrom(element, 1, sttFrom);
    }

    function rightToLeft(element) {
        var sttFrom = {
            opacity: 0,
            x: 30,
            delay: 0.5,
            ease: Expo.easeOut
        };
        TweenMax.staggerFrom(element, 1, sttFrom);
    }

    function leftToRight(element) {
        var sttFrom = {
            opacity: 0,
            x: -30,
            delay: 0.5,
            ease: Expo.easeOut
        };
        TweenMax.staggerFrom(element, 0.6, sttFrom);
    }

    function mapLeftToRight(element) {
        var sttFrom = {
            opacity: 0,
            x: -30,
            delay: 0.5,
            ease: Power2.easeOut
        };
        TweenMax.staggerFrom(element, 1.2, sttFrom);
    }
    function fadeInDown(element) {
        var sttFrom = {
            opacity: 0,
            y: -50,
            delay: 1,
            ease: Power4.easeOut
        };
        TweenMax.staggerFrom(element, 1, sttFrom, 0.2);
    }


    function fadeIn(element) {
        var sttFrom = {
            opacity: 0,
            y: 5,
            delay: 0.5,
            ease: Power4.easeOut
        };
        TweenMax.staggerFrom(element, 1, sttFrom, 0.7);
    }

    function toggleMenu() {
        $('.js-toggle-menu').on('click', () => {
            $('body').toggleClass('show-menu');
        })
    }

})(jQuery);
