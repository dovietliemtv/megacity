-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 06, 2019 at 11:04 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `megacity`
--

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_contact_pages`
--

CREATE TABLE `alipo_cms_contact_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_design_pages`
--

CREATE TABLE `alipo_cms_design_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_developer_pages`
--

CREATE TABLE `alipo_cms_developer_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_duplexes`
--

CREATE TABLE `alipo_cms_duplexes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_duplex_as`
--

CREATE TABLE `alipo_cms_duplex_as` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_duplex_b1s`
--

CREATE TABLE `alipo_cms_duplex_b1s` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_duplex_bs`
--

CREATE TABLE `alipo_cms_duplex_bs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_general_options`
--

CREATE TABLE `alipo_cms_general_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `footer_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_home_pages`
--

CREATE TABLE `alipo_cms_home_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_media_pages`
--

CREATE TABLE `alipo_cms_media_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_neighbourhood_pages`
--

CREATE TABLE `alipo_cms_neighbourhood_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_penthouses`
--

CREATE TABLE `alipo_cms_penthouses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_penthouse_as`
--

CREATE TABLE `alipo_cms_penthouse_as` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_penthouse_bs`
--

CREATE TABLE `alipo_cms_penthouse_bs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_service_pages`
--

CREATE TABLE `alipo_cms_service_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_unit2_bedrooms`
--

CREATE TABLE `alipo_cms_unit2_bedrooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_unit3_bedrooms`
--

CREATE TABLE `alipo_cms_unit3_bedrooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_unit3_bedroom_type_as`
--

CREATE TABLE `alipo_cms_unit3_bedroom_type_as` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_unit3_bedroom_type_bs`
--

CREATE TABLE `alipo_cms_unit3_bedroom_type_bs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_unit3_bedroom_type_cs`
--

CREATE TABLE `alipo_cms_unit3_bedroom_type_cs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_unit4_bedrooms`
--

CREATE TABLE `alipo_cms_unit4_bedrooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_generaloption_contact_infos`
--

CREATE TABLE `alipo_generaloption_contact_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_generaloption_generals`
--

CREATE TABLE `alipo_generaloption_generals` (
  `id` int(10) UNSIGNED NOT NULL,
  `title1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `des1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_network` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `des2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_generaloption_generals`
--

INSERT INTO `alipo_generaloption_generals` (`id`, `title1`, `des1`, `phone`, `social_network`, `title2`, `des2`, `copyright`, `created_at`, `updated_at`) VALUES
(1, 'STELLA MEGA CITY', '<p class=\"ct-foot-info ct-address\">Đường Đặng Văn Dầy, Q. Bình Thuỷ, TP.Cần Thơ</p>\r\n\r\n<p class=\"ct-foot-info ct-web\">stella-megacity.com</p>\r\n\r\n<p class=\"ct-foot-info ct-phone\"><a href=\"tel:+84908885888\">090 888 5 888</a></p>', '090 888 5 888', '[{\"link\":\"#\"},{\"link\":\"#\"},{\"link\":\"#\"}]', 'CÔNG TY CỔ PHẦN KITA INVEST', '<p class=\"ct-foot-info ct-address\">27 Lê Quý Đôn, P. 7, Q. 3, TP. HCM</p>\r\n\r\n<p class=\"ct-foot-info ct-web\">kita-group.com</p>\r\n\r\n<p class=\"ct-foot-info ct-fax\"><a href=\"tel:+842839302855\">(028) 3930 2855</a></p>', 'Copyright © 2019 by KITA Group', '2019-07-08 07:02:28', '2019-09-30 02:58:20');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_chu_dau_tus`
--

CREATE TABLE `alipo_page_chu_dau_tus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_chu_dau_tus`
--

INSERT INTO `alipo_page_chu_dau_tus` (`id`, `title`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Chu dau tu', 'chu-dau-tu', '[{\"description_image\":\"\\/ChuDauTu\\/06Kita_CongChao.png\",\"description_title\":\"T\\u00c2M HUY\\u1ebeT T\\u1eea NG\\u01af\\u1edcI KI\\u1ebeN T\\u1ea0O\",\"description_des\":\"<p>\\u0110\\u01b0\\u1ee3c th\\u00e0nh l\\u1eadp t\\u1eeb n\\u0103m 2014, KITA Group ng\\u00e0y c\\u00e0ng kh\\u1eb3ng \\u0111\\u1ecbnh v\\u1ecb th\\u1ebf c\\u1ee7a m\\u00ecnh tr\\u00ean th\\u1ecb tr\\u01b0\\u1eddng B\\u1ea5t \\u0110\\u1ed9ng S\\u1ea3n khi s\\u1edf h\\u1eefu nhi\\u1ec1u qu\\u1ef9 \\u0111\\u1ea5t c\\u00f3 v\\u1ecb tr\\u00ed \\u0111\\u1eafc \\u0111\\u1ecba tr\\u1ea3i d\\u00e0i t\\u1eeb B\\u1eafc v\\u00e0o Nam, ti\\u1ebfn g\\u1ea7n h\\u01a1n v\\u1edbi m\\u1ee5c ti\\u00eau tr\\u1edf th\\u00e0nh c\\u00f4ng ty B\\u1ea5t \\u0110\\u1ed9ng S\\u1ea3n s\\u1ed1 1 t\\u1ea1i Vi\\u1ec7t Nam.<\\/p>\\r\\n\\r\\n<p>V\\u1edbi ti\\u1ec1m l\\u1ef1c t\\u00e0i ch\\u00ednh d\\u1ed3i d\\u00e0o c\\u00f9ng \\u0111\\u1ed9i ng\\u0169 nh\\u00e2n s\\u1ef1 d\\u00e0y d\\u1ea1n kinh nghi\\u1ec7m v\\u00e0 h\\u1ee3p t\\u00e1c v\\u1edbi c\\u00e1c \\u0111\\u01a1n v\\u1ecb cung c\\u1ea5p d\\u1ecbch v\\u1ee5 chuy\\u00ean nghi\\u1ec7p, KITA Group \\u0111\\u00e3 v\\u00e0 \\u0111ang mang \\u0111\\u1ebfn cho kh\\u00e1ch h\\u00e0ng nh\\u1eefng s\\u1ea3n ph\\u1ea9m v\\u00e0 d\\u1ecbch v\\u1ee5 t\\u1ed1t nh\\u1ea5t, \\u0111em \\u0111\\u1ebfn kh\\u00f4ng gian s\\u1ed1ng ti\\u1ec7n nghi, \\u0111\\u1eb3ng c\\u1ea5p m\\u00e0 v\\u1eabn th\\u00e2n thi\\u1ec7n v\\u1edbi m\\u00f4i tr\\u01b0\\u1eddng.<\\/p>\\r\\n\"},{\"description_image\":\"\\/ChuDauTu\\/06Kita_CongChao2.png\",\"description_title\":\"T\\u1ea6M NH\\u00ccN\",\"description_des\":\"<p>S\\u1ef1 ki\\u00ean \\u0111\\u1ecbnh c\\u1ee7a KITA Group l\\u00e0 mang l\\u1ea1i nh\\u1eefng s\\u1ea3n ph\\u1ea9m b\\u1ea5t \\u0111\\u1ed9ng s\\u1ea3n \\u0111\\u1ed9t ph\\u00e1 cho th\\u1ecb tr\\u01b0\\u1eddng, t\\u1eeb \\u0111\\u00f3 x\\u00e2y d\\u1ef1ng ni\\u1ec1m tin c\\u1ee7a kh\\u00e1ch h\\u00e0ng v\\u00e0o m\\u1ed9t kh\\u00e1t v\\u1ecdng cho cu\\u1ed9c s\\u1ed1ng ch\\u1ea5t l\\u01b0\\u1ee3ng t\\u1ed1t h\\u01a1n.<\\/p>\\r\\n\"}]', '2019-08-25 02:15:44', '2019-09-25 20:14:48');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_contacts`
--

CREATE TABLE `alipo_page_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dvpp` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_contacts`
--

INSERT INTO `alipo_page_contacts` (`id`, `title`, `slug`, `description`, `dvpp`, `created_at`, `updated_at`) VALUES
(1, 'Contact', 'contact', '<p>Quý khách vui lòng điền đầy đủ thông tin bên dưới, chúng tôi sẽ tư vấn và gửi thông tin mới của dự án cho Quý khách trong thời gian sớm nhất.</p>', '[{\"logo\":\"\\/lienhe\\/datxanh.png\",\"hotline\":\"0898 331 179\"},{\"logo\":\"\\/lienhe\\/cenland.png\",\"hotline\":\"0906 010 858\"},{\"logo\":\"\\/logo daily edit\\/BNG.png\",\"hotline\":\"0931 1717 99\"},{\"logo\":\"\\/logo daily edit\\/Mland.png\",\"hotline\":\"0932 88 11 99\"},{\"logo\":\"\\/logo daily edit\\/HP Land.png\",\"hotline\":\"0965 038 795 - 0911 586 999\"},{\"logo\":\"\\/logo daily edit\\/AV land.png\",\"hotline\":\"0904 570 777 - 0983 999 168\"},{\"logo\":\"\\/logo daily edit\\/IDC.png\",\"hotline\":\"0928 226 939\"},{\"logo\":\"\\/lienhe\\/dkrs.png\",\"hotline\":\"0933 570 666 - 0933 560 666\"},{\"logo\":\"\",\"hotline\":\"\"}]', '2019-07-09 01:49:53', '2019-10-05 02:51:05');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_facilities`
--

CREATE TABLE `alipo_page_facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_facilities`
--

INSERT INTO `alipo_page_facilities` (`id`, `title`, `slug`, `content`, `created_at`, `updated_at`) VALUES
(1, 'TIỆN ÍCH', 'facility', '<p>Đây dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>\r\n\r\n<p>Điểm nhấn nổi trội của dự án là khu hành chính và các cơ quan công an nằm ngay trong Stella Mega City Cần Thơ, hơn nữa với việc trang bị hệ thống an ninh nghiêm ngặt, chốt canh gác, bảo vệ sẽ đảm bảo an toàn cho tất cả các hộ dân sinh sống và kinh doanh tại đây. Trong vòng bán kính 2km cách dự án đất nền Stella Mega City, cư dân dễ dàng tiếp cận tới chuỗi tiện ích ngoại khu hiện hữu như: Trường học, bệnh viện, trung tâm y tế, chợ, khu hành chính, trung tâm thương mại, công viên, nhà trẻ, Chi cục Thuế, Kho bạc Nhà nước,…Điểm nhấn nổi trội của dự án là khu hành chính và các cơ quan công an nằm ngay trong Stella Mega City Cần Thơ, hơn nữa với việc trang bị hệ thống an ninh nghiêm ngặt, chốt canh gác, bảo vệ sẽ đảm bảo an toàn cho tất cả các hộ dân sinh sống và kinh doanh tại đây. Trong vòng bán kính 2km cách dự án đất nền Stella Mega City, cư dân dễ dàng tiếp cận tới chuỗi tiện ích ngoại khu hiện hữu như: Trường học, bệnh viện, trung tâm y tế, chợ, khu hành chính, trung tâm thương mại, công viên, nhà trẻ, Chi cục Thuế, Kho bạc Nhà nước,…</p>', '2019-07-08 19:45:54', '2019-07-11 02:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_facility_details`
--

CREATE TABLE `alipo_page_facility_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_facility_details`
--

INSERT INTO `alipo_page_facility_details` (`id`, `title`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Quảng trường', 'quan-truong', '<p>Đây dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>', '2019-07-08 20:07:58', '2019-09-06 09:34:30'),
(2, 'Hệ thống KS 5 sao & TTTM', 'he-thong-ks-5-sao-vs-tttm', '<p>Đây dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>', '2019-07-08 20:08:35', '2019-09-06 09:35:55'),
(3, 'Trung tâm hội nghị', 'trung-tam-hoi-nghi', '<p>Đây dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>', '2019-07-08 20:09:10', '2019-09-06 09:34:08'),
(4, 'Vườn Bonsai', 'vuon-bonsai', '<p>Đây dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>', '2019-07-11 02:12:36', '2019-08-25 01:34:41'),
(5, 'Công viên cây xanh', 'cong-vien-cay-xanh', '<p>Đây dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>', '2019-08-10 10:21:13', '2019-08-25 01:35:24'),
(6, 'Hệ thống trường học các cấp', 'he-thong-truong-hoc-cac-cap', '<p>Đây dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>', '2019-08-10 10:22:13', '2019-08-25 01:35:06');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_galleries`
--

CREATE TABLE `alipo_page_galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `videos` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_galleries`
--

INSERT INTO `alipo_page_galleries` (`id`, `title`, `slug`, `images`, `videos`, `created_at`, `updated_at`) VALUES
(1, 'Thư viện', 'thu-vien', '[{\"image\":\"\\/Thu vien hinh anh\\/3d du an 1.jpg\"},{\"image\":\"\\/Thu vien hinh anh\\/3d du an 2.jpg\"},{\"image\":\"\\/Thu vien hinh anh\\/3d du an 3.jpg\"},{\"image\":\"\\/Thu vien hinh anh\\/3d du an 4.jpg\"},{\"image\":\"\\/Thu vien hinh anh\\/3d du an 5.jpg\"},{\"image\":\"\\/Thu vien hinh anh\\/3d du an 6.jpg\"},{\"image\":\"\\/Thu vien hinh anh\\/3d du an 7.jpg\"},{\"image\":\"\\/Thu vien hinh anh\\/3d du an 5.jpg\"}]', '[{\"image\":\"\\/Thu vien hinh anh\\/3d du an 1.jpg\",\"youtube_id\":\"SwFWX9CjuDo\"},{\"image\":\"\\/1.png\",\"youtube_id\":\"8SsZlZ9B_J0\"},{\"image\":\"\\/Thu vien hinh anh\\/2.png\",\"youtube_id\":\"9_x7vp7d_6w\"},{\"image\":\"\\/Thu vien hinh anh\\/3.png\",\"youtube_id\":\"pLCIS-QohCQ\"}]', '2019-08-24 12:02:05', '2019-09-18 00:07:12');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_homes`
--

CREATE TABLE `alipo_page_homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction_des` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facilities_des` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lydoluachon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_house_dinfo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_homes`
--

INSERT INTO `alipo_page_homes` (`id`, `title`, `slug`, `background`, `introduction_des`, `facilities_des`, `lydoluachon`, `type_house_dinfo`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', 'image', '<p>Hòa mình trong không gian thiên nhiên với mặt tiền là dòng sông Hậu trải dài, Stella Mega City là khu đô thị hiện đại với không khí trong lành tươi mát quanh năm và thiên nhiên là khởi nguồn cho mọi sức sống. Với tầm nhìn đưa Stella Mega City trở thành một đại đô thị năng động, hiện đại – trung tâm kinh tế tại khu vực phía Tây Bắc thành phố Cần Thơ, Stella Mega City tự hào mang trong mình sứ mệnh đem đến cuộc sống chất lượng, đẳng cấp hơn cùng những giá trị thực sự khác biệt.</p>', '<p>Stella Mega City là dự án đại đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega City được đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>', '[{\"image\":\"\\/lydoluachon\\/icon-tracdia.png\",\"text\":\"V\\u1ecb tr\\u00ed \\u0111\\u1ed9c t\\u00f4n\"},{\"image\":\"\\/lydoluachon\\/icon-vuottroi.png\",\"text\":\"Ti\\u1ec7n \\u00edch n\\u1ed9i khu \\u0111\\u1eb3ng c\\u1ea5p 5 sao\"},{\"image\":\"\\/lydoluachon\\/icon.png\",\"text\":\"Quy ho\\u1ea1ch \\u0111\\u1ed3ng b\\u1ed9\"},{\"image\":\"\\/lydoluachon\\/icon-phaply.png\",\"text\":\"S\\u1ed5 \\u0111\\u1ecf t\\u1eebng n\\u1ec1n\"},{\"image\":\"\\/lydoluachon\\/icon-camket.png\",\"text\":\"Ch\\u1ee7 \\u0111\\u1ea7u t\\u01b0 cam k\\u1ebft \\u0111\\u1ed3g h\\u00e0nh c\\u00f9ng d\\u1ef1 \\u00e1n\"},{\"image\":\"\\/lydoluachon\\/icon-dautu.png\",\"text\":\"Gi\\u00e1 tr\\u1ecb \\u0111\\u1ea7u t\\u01b0 gia t\\u0103ng v\\u01b0\\u1ee3t tr\\u1ed9i\"}]', '[{\"number\":\"01\",\"position\":\"-805 30\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\\/matbang\\/fa-he-thong-truong.png\",\"title\":\"\\u0110\\u1ea1i si\\u00eau th\\u1ecb\"},{\"number\":\"02\",\"position\":\"-390 30\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\\/matbang\\/fa-vuon-bonsai.png\",\"title\":\"V\\u01b0\\u1eddn nhi\\u1ec7t \\u0111\\u1edbi\"},{\"number\":\"03\",\"position\":\"-70 -290\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\\/matbang\\/fa-day-nha-biet-thu.png\",\"title\":\"Khu bi\\u1ec7t th\\u1ef1 Compound\"},{\"number\":\"04\",\"position\":\"40 -205\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\\/matbang\\/fa-tttm-he-thong-5-start.png\",\"title\":\"Qu\\u1eadn u\\u1ef7\"},{\"number\":\"05\",\"position\":\"110 -355\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\\/matbang\\/fa-trung-tam-cay-xanh.png\",\"title\":\"Trung t\\u00e2m ph\\u1ee9c h\\u1ee3p TMDV\"},{\"number\":\"06\",\"position\":\"110 -220\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\\/matbang\\/fa-quang-truong.png\",\"title\":\"Qu\\u1ea3ng tr\\u01b0\\u1eddng trung t\\u00e2m\"},{\"number\":\"07\",\"position\":\"210 -215\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\\/matbang\\/fa-trung-tam-hoi-nghi.png\",\"title\":\"U\\u1ef7 ban nh\\u00e2n d\\u00e2n\"},{\"number\":\"08\",\"position\":\"95 -130\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\\/matbang\\/fa-day-nha-pho.png\",\"title\":\"C\\u01a1 quan h\\u00e0nh ch\\u00ednh\"},{\"number\":\"09\",\"position\":\"70 -10\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\",\"title\":\"Khu ph\\u1ee9c h\\u1ee3p kh\\u00e1ch s\\u1ea1n chu\\u1ea9n 4-5* v\\u00e0 TTTMDV\"},{\"number\":\"10\",\"position\":\"185 -140\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\",\"title\":\"Khu ph\\u1ee9c h\\u1ee3p TMDV\"},{\"number\":\"11\",\"position\":\"350 95\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\",\"title\":\"V\\u01b0\\u1eddn Nh\\u1eadt B\\u1ea3n\"},{\"number\":\"12\",\"position\":\"400 -365\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\",\"title\":\"C\\u00f4ng vi\\u00ean c\\u00e2y xanh\"},{\"number\":\"13\",\"position\":\"420 -320\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\",\"title\":\"B\\u1ec7nh vi\\u1ec7n\"},{\"number\":\"14\",\"position\":\"465 -100\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\",\"title\":\"Tr\\u01b0\\u1eddng h\\u1ecdc c\\u1ea5p I-II-III\"},{\"number\":\"15\",\"position\":\"500 20\",\"image\":\"\\/Typeofhouse\\/facilities-slider-01.jpg\",\"icon\":\"\",\"title\":\"Khu ph\\u1ee9c h\\u1ee3p th\\u1ec3 thao gi\\u1ea3i tr\\u00ed H\\u1ed3 b\\u01a1i\"}]', '2019-07-08 06:32:47', '2019-09-29 03:33:51');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_introductions`
--

CREATE TABLE `alipo_page_introductions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_introductions`
--

INSERT INTO `alipo_page_introductions` (`id`, `title`, `slug`, `description`, `video_id`, `created_at`, `updated_at`) VALUES
(1, 'STELLA MEGA CITY', 'khu-do-thi-van-phuc', '[{\"description_image\":\"\\/tongquan\\/SMCT-Desktop Tong quan-200819-1.jpg\",\"description_title\":\"PH\\u1ed2N VINH CU\\u1ed8C S\\u1ed0NG T\\u00c2Y \\u0110\\u00d4\",\"description_des\":\"<p>C\\u1ea7n Th\\u01a1 \\u2013 Th\\u00e0nh ph\\u1ed1 hi\\u1ec7n \\u0111\\u1ea1i v\\u00e0 ph\\u00e1t tri\\u1ec3n nh\\u1ea5t mi\\u1ec1n T\\u00e2y nam B\\u1ed9, \\u0111\\u01b0\\u1ee3c m\\u1ec7nh danh l\\u00e0 T\\u00e2y \\u0110\\u00f4 v\\u1edbi b\\u1ec1 d\\u00e0y l\\u1ecbch s\\u1eed tr\\u00ean 270 n\\u0103m. C\\u1ea7n Th\\u01a1 h\\u1ed9i t\\u1ee5 \\u0111\\u1ea7y \\u0111\\u1ee7 c\\u00e1c y\\u00eau t\\u1ed1 v\\u1ec1 kinh t\\u1ebf, v\\u0103n h\\u00f3a, gi\\u00e1o d\\u1ee5c, thi\\u00ean nhi\\u00ean v\\u00e0 con ng\\u01b0\\u1eddi, t\\u1ea1o \\u0111i\\u1ec1u ki\\u1ec7n thu\\u1eadn l\\u1ee3i cho s\\u1ef1 ph\\u00e1t tri\\u1ec3n kinh t\\u1ebf v\\u00e0 thu h\\u00fat ngu\\u1ed3n \\u0111\\u1ea7u t\\u01b0 trong v\\u00e0 ngo\\u00e0i n\\u01b0\\u1edbc. &nbsp;Theo k\\u1ebf ho\\u1ea1ch, \\u0111\\u1ebfn n\\u0103m 2025, TP.C\\u1ea7n Th\\u01a1 s\\u1ebd tr\\u1edf th\\u00e0nh trung t\\u00e2m C\\u00f4ng Nghi\\u1ec7p \\u2013 Th\\u01b0\\u01a1ng m\\u1ea1i \\u2013 D\\u1ecbch v\\u1ee5 - Gi\\u00e1o d\\u1ee5c \\u2013 \\u0110\\u00e0o t\\u1ea1o v\\u00e0 Khoa h\\u1ecdc c\\u00f4ng ngh\\u1ec7 c\\u1ee7a v\\u00f9ng \\u0110BSCL, \\u0111\\u1ed3ng th\\u1eddi l\\u00e0 \\u0111\\u00f4 th\\u1ecb c\\u1eeda ng\\u00f5 c\\u1ee7a v\\u00f9ng h\\u1ea1 l\\u01b0u s\\u00f4ng M\\u00ea K\\u00f4ng, l\\u00e0 \\u0111\\u1ea7u m\\u1ed1i quan tr\\u1ecdng trong giao th\\u00f4ng v\\u1eadn t\\u1ea3i n\\u1ed9i v\\u00f9ng v\\u00e0 li\\u00ean v\\u1eadn qu\\u1ed1c t\\u1ebf. Ti\\u1ec1m n\\u0103ng c\\u1ee7a th\\u1ecb tr\\u01b0\\u1eddng b\\u1ea5t \\u0111\\u1ed9ng s\\u1ea3n C\\u1ea7n Th\\u01a1 \\u0111\\u01b0\\u1ee3c \\u0111\\u00e1nh gi\\u00e1 cao kh\\u00f4ng ch\\u1ec9 nh\\u1edd v\\u00e0o v\\u1ecb tr\\u00ed trung t\\u00e2m v\\u00f9ng \\u0110BSCL, m\\u00e0 c\\u00f2n nh\\u1edd v\\u00e0o t\\u00ednh ch\\u1ea5t k\\u1ebft n\\u1ed1i th\\u00f4ng su\\u1ed1t v\\u1ec1 h\\u1ea1 t\\u1ea7ng giao th\\u00f4ng v\\u1edbi c\\u00e1c \\u0111\\u1ecba ph\\u01b0\\u01a1ng trong v\\u00f9ng.<\\/p>\\r\\n\"},{\"description_image\":\"\\/tongquan\\/SMCT-Desktop Tong quan-1.jpg\",\"description_title\":\"T\\u00c2M HUY\\u1ebeT NG\\u01af\\u1edcI KI\\u1ebeN T\\u1ea0O\",\"description_des\":\"<p>V\\u1edbi t\\u00e2m huy\\u1ebft c\\u1ee7a m\\u1ed9t nh\\u00e0 ph\\u00e1t tri\\u1ec3n b\\u1ea5t \\u0111\\u1ed9ng s\\u1ea3n b\\u1ec1n v\\u1eefng, KITA Invest mong mu\\u1ed1n T\\u1ea0O D\\u1ef0NG \\u0110I\\u1ec2M \\u0110\\u1ebeN, KI\\u1ebeN T\\u1ea0O C\\u00d4NG \\u0110\\u1ed2NG, VUN \\u0110\\u1eaeP CHO CU\\u1ed8C S\\u1ed0NG T\\u1ed0T \\u0110\\u1eb8P H\\u01a0N<\\/p>\\r\\n\"}]', 'SwFWX9CjuDo', '2019-07-08 19:03:17', '2019-09-25 18:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_locations`
--

CREATE TABLE `alipo_page_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `node_position` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_lat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_long` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_locations`
--

INSERT INTO `alipo_page_locations` (`id`, `title`, `slug`, `node_position`, `description`, `map_lat`, `map_long`, `created_at`, `updated_at`) VALUES
(1, 'VỊ TRÍ', 'vi-tri', '[{\"time\":\"1 ph\\u00fat\",\"node1\":\"Trung t\\u00e2m h\\u00e0nh ch\\u00ednh Q.B\\u00ecnh Th\\u1ee7y\",\"image1\":\"\\/Thu vien hinh anh\\/hinh vi tri\\/hc.png\",\"node2\":\"Khu ph\\u1ee9c h\\u1ee3p th\\u1ec3 thao - gi\\u1ea3i tr\\u00ed\",\"image2\":\"\\/vitri\\/giaitri.jpg\"},{\"time\":\"3 ph\\u00fat\",\"node1\":\"S\\u00e2n bay qu\\u1ed1c t\\u1ebf C\\u1ea7n Th\\u01a1\",\"image1\":\"\\/Thu vien hinh anh\\/hinh vi tri\\/catho-airport.png\",\"node2\":\"TTTM Vincom - Si\\u00eau th\\u1ecb Big C - Lotte Mart\",\"image2\":\"\\/vitri\\/tttm.jpg\"},{\"time\":\"5 ph\\u00fat\",\"node1\":\"B\\u1ec7nh vi\\u1ec7n qu\\u1ed1c t\\u1ebf\",\"image1\":\"\\/Thu vien hinh anh\\/hinh vi tri\\/hospital.png\",\"node2\":\"H\\u1ec7 th\\u1ed1ng tr\\u01b0\\u1eddng h\\u1ecdc c\\u00e1c c\\u1ea5p\",\"image2\":\"\\/Thu vien hinh anh\\/hinh vi tri\\/09Kita_TruongQuocTe.jpg\"},{\"time\":\"10 ph\\u00fat\",\"node1\":\"C\\u00e1c \\u0111\\u1ecba danh du l\\u1ecbch n\\u1ed5i ti\\u1ebfng\",\"image1\":\"\\/vitri\\/dulich.jpg\",\"node2\":\"Trung t\\u00e2m th\\u00e0nh ph\\u1ed1 C\\u1ea7n Th\\u01a1\",\"image2\":\"\\/ChuDauTu\\/tpcantho.jpeg\"}]', '<p>Tọa lạc tại mặt tiền đường Đặng Văn Đầy, kết nối hai trục trường chính Võ Văn Kiệt và Lê Hồng Phong của quận Bình Thủy. Nằm ngay cạnh sân bay quốc tế Cần Thơ, trung tâm hành chính quận đặt ngay tại dự án và đi vào hoạt động. Stella Mega City được đánh giá là sở hữu vị trí vàng vô cùng đắc địa tại TP.Cần Thơ.</p>', '<p>\r\n	<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15713.196750019883!2d105.7311711!3d10.0745531!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a087cf110f5f23%3A0xaa076c3fca26289b!2zS0RDIE5nw6JuIFRodcOibiAoU3RlbGxhIE1lZ2EgQ2l0eSkgLSDEkOG6pXQgWGFuaCBUw6J5IE5hbSBC4buZ!5e0!3m2!1sen!2s!4v1562777574724!5m2!1sen!2s\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>\r\n</p>', '106.7179815', '2019-07-08 19:14:23', '2019-09-25 21:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_news`
--

CREATE TABLE `alipo_page_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_news`
--

INSERT INTO `alipo_page_news` (`id`, `title`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Tin tức', 'tin-tuc', '', '2019-07-09 01:09:54', '2019-07-09 01:09:54');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_processes`
--

CREATE TABLE `alipo_page_processes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_processes`
--

INSERT INTO `alipo_page_processes` (`id`, `title`, `slug`, `info`, `created_at`, `updated_at`) VALUES
(1, 'Tiến Độ', 'tien-do', '[{\"process_date\":\"08\\/2019\",\"process_images\":[{\"pro_image\":\"\\/Tien do T08 2019\\/new\\/slide-4.jpg\"},{\"pro_image\":\"\\/Tien do T08 2019\\/new\\/Slide1-1.jpg\"}]},{\"process_date\":\"09\\/2019\",\"process_images\":[{\"pro_image\":\"\\/Tien Do T9\\/slide-1-2.jpg\"},{\"pro_image\":\"\\/Tien Do T9\\/slide-2-2.jpg\"},{\"pro_image\":\"\\/Tien Do T9\\/slide-3-2.jpg\"},{\"pro_image\":\"\\/Tien Do T9\\/slide-5.jpg\"}]}]', '2019-07-27 22:19:02', '2019-09-22 20:02:41');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_page_type_of_houses`
--

CREATE TABLE `alipo_page_type_of_houses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_page_type_of_houses`
--

INSERT INTO `alipo_page_type_of_houses` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Mặt bằng', 'mat-bang', '2019-07-09 00:03:07', '2019-07-09 00:03:07');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_post_categories`
--

CREATE TABLE `alipo_post_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_post_categories`
--

INSERT INTO `alipo_post_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'All', 'all', '2019-07-09 09:31:45', '2019-07-09 09:31:45');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_post_category_posts`
--

CREATE TABLE `alipo_post_category_posts` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_post_category_posts`
--

INSERT INTO `alipo_post_category_posts` (`category_id`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(1, 4, NULL, NULL),
(1, 5, NULL, NULL),
(1, 6, NULL, NULL),
(1, 7, NULL, NULL),
(1, 8, NULL, NULL),
(1, 9, NULL, NULL),
(1, 10, NULL, NULL),
(1, 11, NULL, NULL),
(1, 12, NULL, NULL),
(1, 13, NULL, NULL),
(1, 14, NULL, NULL),
(1, 15, NULL, NULL),
(1, 16, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `alipo_post_posts`
--

CREATE TABLE `alipo_post_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_post_posts`
--

INSERT INTO `alipo_post_posts` (`id`, `title`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Stella Mega City thu hút giới đầu tư với pháp lý rõ ràng, sổ đỏ từng nền', '', '<p style=\"text-align: justify;\">Với tình hình thị trường BĐS xuất hiện hàng loạt các sản phẩm vướng rủi ro về mặt pháp lý như hiện nay, người mua nhà đã trở nên cẩn trọng hơn trong việc lựa chọn các dự án để đầu tư.</p>\r\n\r\n<p style=\"text-align: justify;\">Bên cạnh các yếu tố về giá cả, vị trí, tiện ích của dự án, tính minh bạch về mặt giấy tờ của dự án trở thành điểm mấu chốt quan trọng quyết định đến việc nhà đầu tư và người mua nhà có lựa chọn dự án đó hay không.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Tính pháp lý của dự án - lá chắn bảo vệ nhà đầu tư</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Theo quy định của luật Kinh doanh bất động sản (BĐS), các dự án phải thực hiện xong phần móng đối với chung cư và hạ tầng đối với các khu đô thị mới, đồng thời có xác nhận về việc sản phẩm đủ điều kiện được bán nhà ở hình thành trong tương lai như giấy chứng nhận sử dụng đất, giấy phép xây dựng và giấy phép bán hàng từ cơ quan chức năng thì mới được mở bán cho khách hàng.Tuy nhiên qua báo cáo của Hiệp hội BĐS, trong hai năm gần đây, thị trường vẫn liên tục xảy ra nhiều đợt sốt ảo giá đất nền. Lao theo những cơn sốt ảo và lợi nhuận trước mắt, cùng với việc không nắm rõ luật, đã có không ít trường hợp nhà đầu tư phải đau đầu vì mua phải dự án với tình trạng pháp lý sơ sài, dẫn đến hàng loạt rắc rối về sau khi “bán không được, đầu tư không xong”.Chính điều này đã hình thành tâm lý bất an, hồi hộp của người mua nhà khi lạc giữa ma trận BĐS như hiện nay. Một điều dễ hiểu khi các dự án BĐS từ các chủ đầu tư uy tín, sở hữu giấy tờ pháp lý rõ ràng, minh bạch sẽ thu hút sự quan tâm đặc biệt của người mua nhà và giới đầu tư và là yếu tố tiên quyết dẫn đến quyết định có đầu tư vào dự án hay không.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>An tâm với dự án Stella Mega City</strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Stella-2.jpg\" style=\"width: 643px;\" class=\"fr-fic fr-dib\"></strong></p>\r\n\r\n<p style=\"text-align: justify;\">Với tổng đầu tư hơn 8.000 tỉ đồng, Stella Mega City Cần Thơ là dự án khu đô thị cao cấp cùng loạt tiện ích nội khu năm sao, được đánh giá là một trong những dự án đầu tư an toàn và hiệu quả khi chủ đầu tư Kita Invest trực thuộc Tập đoàn Kita Group thể hiện mục tiêu đặt quyền lợi hợp pháp về sở hữu tài sản cho khách hàng lên hàng đầu bằng việc thể hiện tính pháp lý minh bạch, rõ ràng của dự án và cam kết cung cấp ngay sổ đỏ từng nền, hạ tầng hoàn thiện cho khách hàng.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Phap-ly-ro-rang-so-do-tung-nen--Stella-Mega-City-diem-hut-moi-cua-gioi-dau-tu-BdS-12-1562913425-961-width660height372.jpg\" style=\"width: 646px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Tọa lạc bên bờ sông Hậu thuộc quận Bình Thủy, nằm cách trung tâm thành phố 3 km và cách sân bay quốc tế Cần Thơ khoảng 2 km, Stella Mega City còn sở hữu vị trí đắc địa và là dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại - dịch vụ, khu liên hợp thể thao, trung tâm sự kiện…</p>\r\n\r\n<p style=\"text-align: justify;\">Stella Mega City được kỳ vọng sẽ trở thành một khu dân cư hiện đại, đẳng cấp góp phần tạo nên mỹ quan đô thị hiện đại, năng động và là tâm điểm mới tại quận Bình Thủy nói riêng và toàn thành phố Cần Thơ nói chung, thu hút dân cư trong khu vực đến sinh sống và làm việc.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/megacity3_oeso.jpg\" style=\"width: 641px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Dự án Stella Mega City Cần Thơ sẽ được chính thức mở bán vào ngày 13.7.2019 tại khách sạn Vinpearl Cần Thơ và được phân phối chính thức bởi 4 đối tác của Kita Invest: Công ty CP BĐS Cen Sài Gòn, Công ty CP BĐS Danh Khôi, Công ty CP BĐS BNG và Công ty CP Đất Xanh Tây Nam Bộ.</p>\r\n\r\n<p style=\"text-align: justify;\">Nguồn : thanhnien.vn</p>', '2019-07-09 01:23:42', '2019-09-30 02:52:45'),
(4, 'Kita Invest chính thức ra mắt siêu dự án Stella Mega City tại Cần Thơ', 'kita-invest-chinh-thuc-ra-mat-sieu-du-stella-mega-city-tai-can-tho', '<p style=\"text-align: justify;\">Sáng 13/7/2019, tại khách sạn Vinpearl Cần Thơ, Công ty CP Kita Invest trực thuộc Kita Group đã tổ chức lễ mở bán dự án khu đô thị Stella Mega City. Lễ mở bán diễn ra với sự tham gia của đại diện các cấp chính quyền, cơ quan truyền thông báo chí, các đối tác của Kita Group và các đại lý phân phối chính thức cùng với ban giám đốc và nhân viên của Công ty.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Với tổng&nbsp;<a href=\"https://baodautu.vn/\">đầu tư</a> hơn 8.000 tỷ đồng, Stella Mega City là Dự án khu đô thị cao cấp thu hút các nhà&nbsp;<a href=\"https://dautubds.baodautu.vn/dautubds.baodautu.vn\">đầu tư bất động sản</a> tại thành phố Cần Thơ nói riêng và các tỉnh Tây Nam Bộ nói chung.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Stella Mega City được đánh giá là một trong những <a href=\"https://baodautu.vn/dau-tu-d2/\">dự án</a> đầu tư an toàn tuyệt đối khi chủ đầu tư- công ty CP Kita Invest thể hiện mục tiêu đặt quyền lợi cho khách hàng lên hàng đầu bằng việc thể hiện tính pháp lý minh bạch, rõ ràng của dự án và cam kết cung cấp sổ đỏ từng nền, hạ tầng hoàn thiện cho khách hàng.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Stella.jpg\" style=\"width: 791px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Dự án sở hữu vị trí vàng tại quận Bình Thủy, cách trung tâm TP 3km và sân bay quốc tế Cần Thơ khoảng 2km, với tổng diện tích hơn 150 hecta cùng hơn 5.000 nền nhà phố liên kết và biệt thự có diện tích đa dạng từ 80 – 170m2 được xây dưới dạng tự do.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Với mục tiêu biến nơi đây thành tổ hợp khu đô thị đẳng cấp nhất khu vực Tây Nam Bộ, Stella Mega City sở hữu hàng loạt tiện ích nội khu cao cấp với hệ thống khách sạn 5 sao, khu trung tâm thương mại, khu liên hợp thể thao, trung tâm sự kiện, quảng trường, vườn nhiệt đới, nhà hàng trên cao,… cùng không gian được phủ xanh kín, mang đến môi trường sống mát mẻ và thoáng đãng cho cư dân.&nbsp;</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Không những thế, khu đô thị đẳng cấp này còn nằm gần các trung tâm hành chính của quận Bình Thủy, UBND Quận, Quận ủy, trường học, Trung tâm y tế đã đi vào hoạt động, giúp cư dân tương lai kết nối dễ dàng với các tiện ích xung quanh.&nbsp;</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Stella-1.jpg\" style=\"width: 798px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">\r\n	<br>\r\n</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Các lô đất nền Stella Mega City được phân chia hợp lý, diện tích đa dạng nhằm đáp ứng mọi nhu cầu sở hữu để ở, kinh doanh, cho thuê hay đầu tư.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Là một trong những dự án chiến lược của tập đoàn Kita Group trong năm 2019, Stella Mega City được chủ đầu tư chăm chút hết sức tỉ mỉ trong từng hạng mục xây dựng, cũng như trong việc chọn lựa các đối tác chiến lược để cùng phát triển, đáp ứng những tiêu chuẩn đảm bảo chất lượng và tiến độ công trình nhằm hướng đến mục tiêu mang đến những sản phẩm <a href=\"http://dautubds.baodautu.vn/thi-truong--nhan-dinh-c31/\">bất động sản</a> đột phá cho thị trường, từ đó xây dựng niềm tin của khách hàng vào một khát vọng cho một cuộc sống chất lượng tốt hơn.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">\r\n	<br>\r\n</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Stella-2.jpg\" style=\"width: 819px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Dự án Stella Mega City Cần Thơ được phân phối chính thức bởi 4 đối tác của Kita Invest: công ty CPBĐS Cen Sài Gòn, công ty CPBĐS Danh Khôi, công ty CPBĐS BNG và công ty CP Đất Xanh Tây Nam Bộ.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align: justify;\">Nguồn : dautubds.baodautu.vn</p>', '2019-07-17 01:33:35', '2019-09-30 02:52:30'),
(5, 'Kita Invest chính thức ra mắt dự án Stella Mega City tại Cần Thơ', 'kita-invest-chinh-thuc-ra-mat-du-stella-mega-city-tai-can-tho', '<p style=\"text-align: justify;\"><strong>Sáng 13/07/2019, tại khách sạn Vinpearl Cần Thơ, Công ty CP Kita Invest trực thuộc Kita Group đã tổ chức lễ mở bán dự án khu đô thị Stella Mega City.</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Lễ mở bán diễn ra với sự tham gia của đại diện các cấp chính quyền, cơ quan truyền thông báo chí, các đối tác của Kita Group và các đại lý phân phối chính thức cùng với ban giám đốc và nhân viên của công ty.</p>\r\n\r\n<p style=\"text-align: justify;\">Với tổng đầu tư hơn 8.000 tỷ đồng, Stella Mega City là dự án khu đô thị cao cấp thu hút các nhà đầu tư bất động sản tại thành phố Cần Thơ nói riêng và các tỉnh Tây Nam Bộ nói chung. Stella Mega City được đánh giá là một trong những dự án đầu tư an toàn khi chủ đầu tư- công ty CP Kita Invest thể hiện mục tiêu đặt quyền lợi cho khách hàng lên hàng đầu bằng việc thể hiện tính pháp lý minh bạch, rõ ràng của dự án và cam kết cung cấp sổ đỏ từng nền, hạ tầng hoàn thiện cho khách hàng.</p>\r\n\r\n<p style=\"text-align: justify;\">Dự án sở hữu vị trí đắc địa tại quận Bình Thủy, cách trung tâm TP 3km và sân bay quốc tế Cần Thơ khoảng 2km, với tổng diện tích hơn 150 hecta cùng hơn 5.000 nền nhà phố liên kết và biệt thự có diện tích đa dạng từ 80 – 170m2 được xây dưới dạng tự do. Với mục tiêu biến nơi đây thành tổ hợp khu đô thị đẳng cấp nhất khu vực Tây Nam Bộ, Stella Mega City sở hữu hàng loạt tiện ích nội khu cao cấp với hệ thống khách sạn 5 sao, khu trung tâm thương mại, khu liên hợp thể thao, trung tâm sự kiện, quảng trường, vườn nhiệt đới, nhà hàng trên cao,… cùng không gian được phủ xanh kín, mang đến môi trường sống mát mẻ và thoáng đãng cho cư dân. Không những thế, khu đô thị đẳng cấp này còn nằm gần các trung tâm hành chính của quận Bình Thủy, UBND Quận, Quận ủy, trường học, Trung tâm y tế đã đi vào hoạt động, giúp cư dân tương lai kết nối dễ dàng với các tiện ích xung quanh.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Stella-1.jpg\" style=\"width: 726px;\" class=\"fr-fic fr-dib\"></p>\r\n<div style=\"text-align: justify;\" type=\"Photo\">\r\n	<br>\r\n</div>\r\n\r\n<p style=\"text-align: justify;\">Nguồn : cafef.vn</p>', '2019-07-17 01:48:48', '2019-09-30 02:52:12'),
(6, 'Stella Mega City Cần Thơ- Phồn vinh cuộc sống Tây Đô', 'stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do', '<p>Hạ tầng giao thông TP.Cần Thơ phát triển; nhiều dự án đường cao tốc, hàng không, cảng biển… khiến BĐS cao cấp nơi đây thu hút mạnh đầu tư. Nhu cầu tăng lên cùng hạ tầng ngày càng đồng bộ khiến thị trường BĐS Cần Thơ chuyển mình tích cực.</p>\r\n\r\n<p><strong>Tây Đô- thị trường mới đầy tiềm năng</strong></p>\r\n\r\n<p>Ở thị trường BĐS phía Nam, quỹ đất TP.HCM không còn nhiều, cộng với sự kiểm soát tính pháp lý của các dự án BĐS chặt chẽ từ phía nhà nước dẫn đến các nhà đầu tư BĐS đang có xu hướng tìm đến các tỉnh lân cận, trong đó có khu vực ĐBSCL, đặc biệt là TP.Cần Thơ. Đây còn là khu vực nằm trong chương trình phát triển của Chính phủ về kinh tế, do đó, trong những năm tới, khu vực này hứa hẹn sẽ gây nên những cơn sốt mớicho thị trường.Theo quy hoạch, Cần Thơ tới năm 2025 sẽ là trọng tâm công nghiệp, thương mại, dịch vụ, du lịch, giáo dục đào tạo, kỹ thuật công nghệ, y tế, văn hóa của cả vùng ĐBSCL.</p>\r\n\r\n<p>Cần Thơ là địa phương có nhiều tiềm năng về kinh tế, bao gồm cả tiềm năng về BĐS nên việc các doanh nghiệp đầu tư tại thị trường này là xu hướng tất yếu. Tiềm năng của thị trường BĐS Cần Thơ được đánh giá cao không chỉ nhờ vào vị trí trung tâm vùng ĐBSCL, mà còn nhờ vào tính chất kết nối thông suốt về hạ tầng giao thông với các địa phương trong vùng.</p>\r\n\r\n<p><strong>Khu đô thị đẳng cấp nhất Cần Thơ</strong></p>\r\n\r\n<p>Là dự án chiến lược của tập đoàn Kita Group trong năm 2019, Stella Mega City là khu đô thị 5 sao với số vốn đầu tư lên đến hơn 8.000 tỷ, đáp ứng đầy đủ tiêu chuẩn của một khu đô thị thông minh, mang đến một chuẩn mực sống mới cho người dân tại khu vực Tây Nam Bộ. Giá trị của Stella Mega City nằm ở vị trí vàng khi sở hữu một vị trí đắc địatrên trục đường chính Võ Văn Kiệt- Lê Hồng Phong của quận Bình Thủy, cách trung tâm thành phố 3km và cách sân bay quốc tế Cần Thơ khoảng 2km.</p>\r\n\r\n<p><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do.jpg\" style=\"width: 714px;\" class=\"fr-fic fr-dib\"></p>\r\n\r\n<p>Đây dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại - dịch vụ, khu liên hợp thể thao, trung tâm sự kiện… Stella Mega Cityđược đầu tư hạ tầng nội khu hoàn hảo với đường nội khu trải nhựa lộ giới 6m, vỉa hè cây xanh, đèn điện chiếu sáng, cảnh quan môi trường sống được chú trọng nhằm mang đến không gian tươi mát, tầm nhìn đẹp và rộng mở cho cộng đồng dân cư sinh sống tại đây.</p>\r\n\r\n<p><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do-1.jpg\" style=\"width: 706px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p>Điểm nhấn nổi trội của dự án là khu hành chính và các cơ quan công an nằm ngay trong Stella Mega City Cần Thơ, hơn nữa với việc trang bị hệ thống an ninh nghiêm ngặt, chốt canh gác, bảo vệ sẽ đảm bảo an toàn cho tất cả các hộ dân sinh sống và kinh doanh tại đây.Trong vòng bán kính 2km cách dự án đất nền Stella Mega City, cư dân dễ dàng tiếp cận tới chuỗi tiện ích ngoại khu hiện hữu như: Trường học, bệnh viện, trung tâm y tế, chợ, khu hành chính, trung tâm thương mại, công viên, nhà trẻ, Chi cục Thuế, Kho bạc Nhà nước,…</p>\r\n\r\n<p>Đặc biệt, đây còn là dự án nổi bật tại thị trường Tây Nam Bộ khi được chủ đầu tư cam kết tính pháp lý minh bạch, rõ ràng, đảm bảo sự an tâm cao nhất cho các nhà đầu tư.</p>\r\n\r\n<p>Dự án Stella Mega City Cần Thơ chính thức mở bán vào ngày 13/07/2019 tại tại khách sạn Vinpearl Cần Thơ và được phân phối chính thức bởi 4 đối tác của Kita Invest: công ty CPBĐS Cen Sài Gòn, công ty CPBĐS Danh Khôi, công ty CPBDS BNG và công ty CP Đất Xanh Tây Nam Bộ.</p>\r\n\r\n<p>Nguồn : vietnamnet.vn</p>', '2019-07-17 01:54:36', '2019-07-17 01:55:47'),
(7, 'Stella Mega City thu hút giới đầu tư với pháp lý rõ ràng, sổ đỏ từng nền', 'stella-mega-city-thu-hut-gioi-dau-tu-voi-phap-ly-ro-rang-so-do-tung-nen', '<p>Với tình hình thị trường BĐS xuất hiện hàng loạt các sản phẩm vướng rủi ro về mặt pháp lý như hiện nay, người mua nhà đã trở nên cẩn trọng hơn trong việc lựa chọn các dự án để đầu tư.</p>\r\n\r\n<p>Bên cạnh các yếu tố về giá cả, vị trí, tiện ích của dự án, tính minh bạch về mặt giấy tờ của dự án trở thành điểm mấu chốt quan trọng quyết định đến việc nhà đầu tư và người mua nhà có lựa chọn dự án đó hay không.</p>\r\n\r\n<p><strong>Tính pháp lý của dự án - lá chắn bảo vệ nhà đầu tư</strong></p>\r\n\r\n<p>Theo quy định của luật Kinh doanh bất động sản (BĐS), các dự án phải thực hiện xong phần móng đối với chung cư và hạ tầng đối với các khu đô thị mới, đồng thời có xác nhận về việc sản phẩm đủ điều kiện được bán nhà ở hình thành trong tương lai như giấy chứng nhận sử dụng đất, giấy phép xây dựng và giấy phép bán hàng từ cơ quan chức năng thì mới được mở bán cho khách hàng.Tuy nhiên qua báo cáo của Hiệp hội BĐS, trong hai năm gần đây, thị trường vẫn liên tục xảy ra nhiều đợt sốt ảo giá đất nền. Lao theo những cơn sốt ảo và lợi nhuận trước mắt, cùng với việc không nắm rõ luật, đã có không ít trường hợp nhà đầu tư phải đau đầu vì mua phải dự án với tình trạng pháp lý sơ sài, dẫn đến hàng loạt rắc rối về sau khi “bán không được, đầu tư không xong”.Chính điều này đã hình thành tâm lý bất an, hồi hộp của người mua nhà khi lạc giữa ma trận BĐS như hiện nay. Một điều dễ hiểu khi các dự án BĐS từ các chủ đầu tư uy tín, sở hữu giấy tờ pháp lý rõ ràng, minh bạch sẽ thu hút sự quan tâm đặc biệt của người mua nhà và giới đầu tư và là yếu tố tiên quyết dẫn đến quyết định có đầu tư vào dự án hay không.</p>\r\n\r\n<p><strong>An tâm với dự án Stella Mega City</strong></p>\r\n\r\n<p><strong><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Stella-2.jpg\" style=\"width: 741px;\" class=\"fr-fic fr-dib\"></strong></p>\r\n\r\n<p>Với tổng đầu tư hơn 8.000 tỉ đồng, Stella Mega City Cần Thơ là dự án khu đô thị cao cấp cùng loạt tiện ích nội khu năm sao, được đánh giá là một trong những dự án đầu tư an toàn và hiệu quả khi chủ đầu tư Kita Invest trực thuộc Tập đoàn Kita Group thể hiện mục tiêu đặt quyền lợi hợp pháp về sở hữu tài sản cho khách hàng lên hàng đầu bằng việc thể hiện tính pháp lý minh bạch, rõ ràng của dự án và cam kết cung cấp ngay sổ đỏ từng nền, hạ tầng hoàn thiện cho khách hàng.</p>\r\n\r\n<p><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Phap-ly-ro-rang-so-do-tung-nen--Stella-Mega-City-diem-hut-moi-cua-gioi-dau-tu-BdS-12-1562913425-961-width660height372.jpg\" style=\"width: 743px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p>Tọa lạc bên bờ sông Hậu thuộc quận Bình Thủy, nằm cách trung tâm thành phố 3 km và cách sân bay quốc tế Cần Thơ khoảng 2 km, Stella Mega City còn sở hữu vị trí đắc địa và là dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại - dịch vụ, khu liên hợp thể thao, trung tâm sự kiện…</p>\r\n\r\n<p>Stella Mega City được kỳ vọng sẽ trở thành một khu dân cư hiện đại, đẳng cấp góp phần tạo nên mỹ quan đô thị hiện đại, năng động và là tâm điểm mới tại quận Bình Thủy nói riêng và toàn thành phố Cần Thơ nói chung, thu hút dân cư trong khu vực đến sinh sống và làm việc.</p>\r\n\r\n<p><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/megacity3_oeso.jpg\" style=\"width: 750px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p>Dự án Stella Mega City Cần Thơ sẽ được chính thức mở bán vào ngày 13.7.2019 tại khách sạn Vinpearl Cần Thơ và được phân phối chính thức bởi 4 đối tác của Kita Invest: Công ty CP BĐS Cen Sài Gòn, Công ty CP BĐS Danh Khôi, Công ty CP BĐS BNG và Công ty CP Đất Xanh Tây Nam Bộ.</p>\r\n\r\n<p>Nguồn : thanhnien.vn</p>', '2019-07-17 01:58:44', '2019-07-17 01:59:15'),
(8, 'Stella Mega City: Điểm hút mới của giới đầu tư bất động sản', 'stella-mega-city-diem-hut-moi-cua-gioi-dau-tu-bat-dong-san', '<p style=\"text-align: justify;\"><strong>Với tình hình thị trường bất động sản xuất hiện hàng loạt các sản phẩm vướng rủi ro về mặt pháp lý như hiện nay, người mua nhà đã trở nên cẩn trọng hơn trong việc lựa chọn các dự án để đầu tư. Bên cạnh các yếu tố về giá cả, vị trí, tiện ích của dự án, tính minh bạch về mặt giấy tờ của dự án trở thành điểm mấu chốt quan trọng quyết định đến việc nhà đầu tư và người mua nhà có lựa chọn dự án đó hay không.</strong></p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Stella-2.jpg\" style=\"width: 842px;\" class=\"fr-fic fr-dib\"></p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Tính pháp lý của dự án - Lá chắn bảo vệ nhà đầu tư</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Theo quy định của Luật Kinh doanh bất động sản, các dự án phải thực hiện xong phần móng đối với chung cư và hạ tầng đối với các khu đô thị mới, đồng thời có xác nhận về việc sản phẩm đủ điều kiện được bán nhà ở hình thành trong tương lai như giấy chứng nhận sử dụng đất, giấy phép xây dựng và giấy phép bán hàng từ cơ quan chức năng thì mới được mở bán cho khách hàng.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Phap-ly-ro-rang-so-do-tung-nen--Stella-Mega-City-diem-hut-moi-cua-gioi-dau-tu-BdS-12-1562913425-961-width660height372.jpg\" style=\"width: 839px;\" class=\"fr-fic fr-dib\"></p>\r\n\r\n<p style=\"text-align: justify;\">Tuy nhiên, qua báo cáo của Hiệp hội bất động sản, trong hai năm gần đây thị trường vẫn liên tục xảy ra nhiều đợt sốt ảo giá đất nền. Lao theo những cơn sốt ảo và lợi nhuận trước mắt, cùng với việc không nắm rõ luật, đã có không ít trường hợp nhà đầu tư phải đau đầu vì mua phải dự án với tình trạng &nbsp;pháp lý sơ sài, dẫn đến hàng loạt rắc rối về sau khi “bán không được, đầu tư không xong”.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Chính điều này đã hình thành tâm lý bất an, hồi hộp của người mua nhà khi lạc giữa ma trận bất động sản như hiện nay. Một điều dễ hiểu khi các dự án bất động sản từ các chủ đầu tư uy tín, sở hữu giấy tờ pháp lý rõ ràng, minh bạch sẽ thu hút sự quan tâm đặc biệt của người mua nhà và giới đầu tư và là yếu tố tiên quyết dẫn đến quyết định có đầu tư vào dự án hay không.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>An tâm với pháp lý minh bạch của Stella Mega City&nbsp;</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Với tổng đầu tư hơn 8.000 tỷ đồng, Stella Mega City Cần Thơ là dự án khu đô thị cao cấp cùng loạt tiện ích nội khu năm sao, được đánh giá là một trong những dự án đầu tư an toàn và hiệu quả khi chủ đầu tư Kita Invest trực thuộc tập đoàn Kita Group thể hiện mục tiêu đặt quyền lợi hợp pháp về sở hữu tài sản cho khách hàng lên hàng đầu bằng việc thể hiện tính pháp lý minh bạch, rõ ràng của dự án và cam kết cung cấp ngay sổ đỏ từng nền, hạ tầng hoàn thiện cho khách hàng.</p>\r\n\r\n<p style=\"text-align: justify;\">Tọa lạc bên bờ sông Hậu thuộc quận Bình Thủy, nằm cách trung tâm thành phố 3km và cách sân bay quốc tế Cần Thơ khoảng 2km, Stella Mega City còn sở hữu vị trí đắc địa và là dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại - dịch vụ, khu liên hợp thể thao, trung tâm sự kiện…&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/bds3-2919-1563184524.jpg\" style=\"width: 850px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Stella Mega City được kỳ vọng sẽ trở thành một khu dân cư hiện đại, đẳng cấp góp phần tạo nên mỹ quan đô thị hiện đại, năng động và là tâm điểm mới tại quận Bình Thủy nói riêng và toàn thành phố Cần Thơ nói chung, thu hút dân cư trong khu vực đến sinh sống và làm việc.</p>\r\n\r\n<p style=\"text-align: justify;\">Dự án Stella Mega City Cần Thơ sẽ được chính thức mở bán vào ngày 13/7/2019 tại Khách sạn Vinpearl Cần Thơ và được phân phối chính thức bởi 4 đối tác của Kita Invest: Công ty CP BĐS Cen Sài Gòn, Công ty CP BĐS Danh Khôi, Công ty CP BĐS BNG và Công ty CP Đất Xanh Tây Nam Bộ.</p>\r\n\r\n<p style=\"text-align: justify;\">Kita Group được thành lập từ năm 2014, bao gồm 4 công ty thành viên hoạt động trong nhiều lĩnh vực: Kita Land (quản lý tài sản, phát triển dự án, phân phối sản phẩm), Kita Invest (mua bán sát nhập, đầu tư bất động sản, đầu tư tài chính), Kita Cons (xây dựng tòa nhà cao tầng, xây dựng khu đô thị, hoàn thiện nội thất) và Kita Link. Với tầm nhìn kiên định nhằm mang lại những sản phẩm bất động sản đột phá cho thị trường, từ đó xây dựng niềm tin của khách hàng vào một khát vọng cho một cuộc sống chất lượng tốt hơn, Kita Group đã và đang không ngừng phát triển với nhiều dự án bất động sản khác nhau như Stella Mega City (Cần Thơ), Stella 927 (TP.HCM), Quốc Oai (Hà Nội), Golden Hills (Đà Nẵng).</p>\r\n\r\n<p style=\"text-align: justify;\">Nguồn : doanhnhansaigon.vn</p>', '2019-07-17 02:02:18', '2019-09-30 03:11:14'),
(9, 'Pháp lý rõ ràng, sổ đỏ từng nền, Stella Mega City hút giới đầu tư BĐS', 'phap-ly-ro-rang-so-do-tung-nen-stella-mega-city-hut-gioi-dau-tu-bds', '<article>\r\n\r\n	<p><strong>Với tình hình thị trường bất động sản xuất hiện hàng loạt các sản phẩm vướng rủi ro về mặt pháp lý như hiện nay, người mua nhà đã trở nên cẩn trọng hơn trong việc lựa chọn các dự án để đầu tư. Bên cạnh các yếu tố về giá cả, vị trí, tiện ích của dự án, tính minh bạch về mặt giấy tờ của dự án trở thành điểm mấu chốt quan trọng quyết định đến việc nhà đầu tư và người mua nhà có lựa chọn dự án đó hay không.</strong></p>\r\n\r\n	<p><strong>Tính pháp lý của dự án - lá chắn bảo vệ nhà đầu tư</strong></p>\r\n\r\n	<p>Theo quy định của Luật Kinh doanh bất động sản, các dự án phải thực hiện xong phần móng đối với chung cư và hạ tầng đối với các khu đô thị mới, đồng thời có xác nhận về việc sản phẩm đủ điều kiện được bán nhà ở hình thành trong tương lai như giấy chứng nhận sử dụng đất, giấy phép xây dựng và giấy phép bán hàng từ cơ quan chức năng thì mới được mở bán cho khách hàng.</p>\r\n\r\n	<p>Tuy nhiên qua báo cáo của Hiệp hội bất động sản, trong hai năm gần đây, thị trường vẫn liên tục xảy ra nhiều đợt sốt ảo giá đất nền. Lao theo những cơn sốt ảo và lợi nhuận trước mắt, cùng với việc không nắm rõ luật, đã có không ít trường hợp nhà đầu tư phải đau đầu vì mua phải dự án với tình trạng &nbsp;pháp lý sơ sài, dẫn đến hàng loạt rắc rối về sau khi “bán không được, đầu tư không xong\".</p>\r\n\r\n	<p>Chính điều này đã hình thành tâm lý bất an, hồi hộp của người mua nhà khi lạc giữa ma trận bất động sản như hiện nay. Một điều dễ hiểu khi các dự án bất động sản từ các chủ đầu tư uy tín, sở hữu giấy tờ pháp lý rõ ràng, minh bạch sẽ thu hút sự quan tâm đặc biệt của người mua nhà và giới đầu tư và là yếu tố tiên quyết dẫn đến quyết định có đầu tư vào dự án hay không.</p>\r\n\r\n	<p><strong>An tâm với pháp lý minh bạch của Stella Mega City</strong></p>\r\n\r\n	<p><strong><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Stella-2.jpg\" style=\"width: 740px;\" class=\"fr-fic fr-dib\"></strong>\r\n		<br>\r\n	</p>\r\n\r\n	<p>Với tổng đầu tư hơn 8.000 tỷ đồng, Stella Mega City Cần Thơ là dự án khu đô thị cao cấp cùng loạt tiện ích nội khu năm sao, được đánh giá là một trong những dự án đầu tư an toàn và hiệu quả khi chủ đầu tư Kita Invest trực thuộc tập đoàn Kita Group thể hiện mục tiêu đặt quyền lợi hợp pháp về sở hữu tài sản cho khách hàng lên hàng đầu bằng việc thể hiện tính pháp lý minh bạch, rõ ràng của dự án và cam kết cung cấp ngay sổ đỏ từng nền, hạ tầng hoàn thiện cho khách hàng.</p>\r\n\r\n	<p>\r\n		<br>\r\n	</p>\r\n\r\n	<p><strong><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Phap-ly-ro-rang-so-do-tung-nen--Stella-Mega-City-diem-hut-moi-cua-gioi-dau-tu-BdS-12-1562913425-961-width660height372.jpg\" style=\"width: 739px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></strong></p>\r\n\r\n	<p>Tọa lạc bên bờ sông Hậu thuộc quận Bình Thủy, nằm cách trung tâm thành phố 3km và cách sân bay quốc tế Cần Thơ khoảng 2km, Stella Mega City còn sở hữu vị trí đắc địa và là dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế như: trường học, sân vận động, trung tâm triển lãm hội nghị đa chức năng, trung tâm thương mại – dịch vụ, khu liên hợp thể thao, trung tâm sự kiện…</p>\r\n\r\n	<p>Stella Mega City được kỳ vọng sẽ trở thành một khu dân cư hiện đại, đẳng cấp góp phần tạo nên mỹ quan đô thị hiện đại, năng động và là tâm điểm mới tại quận Bình Thủy nói riêng và toàn thành phố Cần Thơ nói chung, thu hút dân cư trong khu vực đến sinh sống và làm việc.</p>\r\n\r\n	<p><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/Phap-ly-ro-rang-so-do-tung-nen--Stella-Mega-City-diem-hut-moi-cua-gioi-dau-tu-BdS-13-1562913425-101-width660height372.jpg\" style=\"width: 728px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n	<p>Dự án Stella Mega CityCần Thơ sẽ được chính thức mở bán vào ngày 13/07/2019 tại tại khách sạn Vinpearl Cần Thơ và được phân phối chính thức bởi 4 đối tác của Kita Invest: công ty CPBĐS Cen Sài Gòn, công ty CPBĐS Danh Khôi, công ty CPBĐS BNG và công ty CP Đất Xanh Tây Nam Bộ.</p>\r\n\r\n	<p>Kita Group được thành lập từ năm 2014, bao gồm 4 công ty thành viên hoạt động trong nhiều lĩnh vực: Kita Land (Quản lý tài sản, Phát triển dự án, Phân phối sản phẩm), Kita Invest (Mua bán sát nhập, Đầu tư bất động sản, Đầu tư tài chính), Kita Cons (Xây dựng tòa nhà cao tầng, Xây dựng khu đô thị, Hoàn thiện nội thất), Kita Link. Với tầm nhìn kiên định nhằm mang lại những sản phẩm bất động sản đột phá cho thị trường, từ đó xây dựng niềm tin của khách hàng vào một khát vọng cho một cuộc sống chất lượng tốt hơn, Kita Group đã và đang không ngừng phát triển với nhiều dự án bất động sản khác nhau như Stella Mega City (Cần Thơ), Stella 927 (TP.HCM), Quốc Oai (Hà Nội), Golden Hills (Đà Nẵng).</p>\r\n\r\n	<p>Nguồn : 24h.com.vn</p>\r\n</article>', '2019-07-17 02:05:07', '2019-07-17 02:05:07'),
(10, 'KITA Group: Kinh doanh bất động sản muốn bứt phá cần phải có tầm nhìn tốt', 'kita-group-kinh-doanh-bat-dong-san-muon-pha-can-phai-co-tam-nhin-tot', '<p style=\"text-align: justify;\">Tuy là cái tên mới trong “làng bất động sản”, nhưng bằng tầm nhìn đúng đắn đang giúp KITA Group tạo được dấu ấn vững chắc trên thị trường, với hàng loạt dự án chất lượng nằm ở vị trí đắc địa trải dài từ Bắc vào Nam.</p>\r\n<article style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569827648907.png\" style=\"width: 480px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></article>\r\n\r\n<p style=\"text-align: justify;\"><strong>Dấu ấn mới trên thị trường bất động sản Việt Nam</strong></p>\r\n\r\n<p style=\"text-align: justify;\">KITA Group là doanh nghiệp trẻ trên thị bất động sản Việt Nam nhưng đã có bước phát triển mạnh mẽ và đang vươn lên khẳng định vị thế tại thị trường trong nước. KITA được viết tắt từ 4 yếu tố: Kiên định (Keeness) - Đột phá (Innovation) - Niềm tin (Trust) - Khát vọng (Aspriration).&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Hiện tại, KITA Group gồm có 4 công ty thành viên hoạt động trong nhiều lĩnh vực: KITA Land (quản lý tài sản, phát triển dự án, phân phối sản phẩm), KITA Invest (mua bán sát nhập, đầu tư bất động sản, đầu tư tài chính), KITA Cons (xây dựng tòa nhà cao tầng, xây dựng khu đô thị, hoàn thiện nội thất) và KITA Link (thu đổi ngoại tệ, phát triển dự án C2C).</p>\r\n\r\n<p style=\"text-align: justify;\">Với tầm nhìn chiến lược, KITA Group đang không ngừng mở rộng đầu tư trong lĩnh vực địa ốc với tâm thế của một doanh nghiệp có tiềm lực tài chính dồi dào, nguồn lực vững chắc trong đầu tư, uy tín đảm bảo chất lượng xây dựng và năng lực trong quản lý vận hành các dự án của mình.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Bà Đặng Thị Thùy Trang - CEO của KITA Group cho biết: “Bất kỳ một doanh nghiệp nào cũng cần tài chính mạnh và tầm nhìn tốt để phát triển bền vững. Chúng tôi hiểu được tầm quan trọng của nó, đặc biệt trong việc phát triển dự án bất động sản. Tài chính vững mạnh không những đảm bảo cho sự phát triển của chúng tôi, nó còn đảm bảo cho mối quan hệ của chúng tôi với các đối tác. Tầm nhìn tốt sẽ giúp doanh nghiệp bất động sản phát triển các sản phẩm bền vững và độc đáo”.</p>\r\n\r\n<p style=\"text-align: justify;\">Ghi nhận thực tế cho thấy, hiện KITA Group đang nắm giữ quỹ đất hơn 230ha với vị trí đắc địa trải dài từ Bắc vào Nam. Đây là tiền đề vững chắc để đơn vị này phát triển triển và cạnh tranh trên “sân chơi” bất động sản tại Việt Nam.</p>\r\n\r\n<p style=\"text-align: justify;\">Với định hướng phát triển thị trường và đẩy mạnh năng lực cạnh tranh hiệu quả, KITA Group đã phát triển nhiều dự án chất lượng nhận được sự đón nhận tích cực từ thị trường như: Dự án căn hộ cao cấp 5 sao - Stella 927 tọa lạc trên đường Trần Hưng Đạo; Dự án căn hộ cao cấp 5 sao - Stella 520 tại một vị trí đẹp ở mặt tiền Đại Lộ Võ Văn Kiệt.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569827672767.png\" style=\"width: 486px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Dự án khu đô thị mới tại Quốc Oai thuộc thủ đô Hà Nội, khu đô thị sinh thái Golden Hills thuộc huyện Hòa Vang và quận Liên Chiểu, là cửa ngõ phía Tây Bắc thành phố Đà Nẵng.</p>\r\n\r\n<p style=\"text-align: justify;\">Hơn thế, doanh nghiệp này còn luôn nhận được sự ủng hộ mạnh mẽ từ các đối tác tài chính, xây dựng cũng như những ngành nghề khác. KITA Group đang là đối tác chiến lược của nhiều tên tuổi lớn như Trung Nam Group, Sacombank, VietinBank…</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Kiên định để tạo nên giá trị khác biệt</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Khi được hỏi về bí quyết để hoạt động và đẩy mạnh năng lực cạnh tranh trên thị trường, CEO của KITA Group chia sẻ: “Sự kiên định của chúng tôi mang lại những sản phẩm bất động sản đột phá cho thị trường, từ đó xây dựng niềm tin của khách hàng vào một khát vọng cho một cuộc sống chất lượng tốt hơn”.</p>\r\n\r\n<p style=\"text-align: justify;\">Được biết, đến thời điểm hiện tại, KITA Group &nbsp;tập trung vào các giá trị gia tăng bền vững cho các nhà đầu tư cũng như tiện ích sống, pháp lý an toàn cho cư dân của các dự án do KITA Group phát triển, nhằm thực hiện mục tiêu trở thành công ty bất động sản số top đầu tại Việt Nam.</p>\r\n\r\n<p style=\"text-align: justify;\">Bên cạnh đó, KITA Group cũng đẩy mạnh phát triển theo hướng tập trung và tối ưu nguồn lực để trở thành nhà đầu tư và phát triển dự án có tiềm lực mạnh, không chỉ cung cấp các sản phẩm tốt ra thị trường mà còn đồng hành cùng người tiêu dùng trong quá trình sử dụng, trải nghiệm và duy trì sản phẩm.</p>\r\n\r\n<p style=\"text-align: justify;\">Đặc biệt, trong tầm nhìn phát triển lâu dài, con người vẫn là tài sản quý giá nhất đối với KITA Group. Cũng như bộ khung của cơ thể, tại KITA Group, người giỏi thôi chưa đủ, mà phải đúng việc thì mới là chìa khóa cho sự thành công.</p>\r\n\r\n<p style=\"text-align: justify;\">Giữa năm 2019, KITA Group tạo được nhiều dấu ấn trên thị trường bất động sản khi đầu tư hơn 8.000 tỷ đồng để phát triển dự án Stella Mega City - đại đô thị tọa lạc ngay tại trung tâm thành phố Cần Thơ. Trao đổi với KITA Group, chúng tôi được biết dự án nằm tại khu vực quận Bình Thủy, là một trong những quỹ đất đắc địa ngay cạnh sân bay quốc tế Cần Thơ và trung tâm hành chính quận Bình Thủy tọa lạc ngay tại dự án.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569827691935.png\" style=\"width: 673px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Đây là dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế. Hơn thế nữa, chủ đầu tư luôn đặt quyền lợi hợp pháp về sở hữu tài sản cho khách hàng lên hàng đầu bằng việc thể hiện cam kết ba sạch: đất sạch - pháp lý sạch - môi trường sống sạch. Stella Mega City nổi bật với tính chất pháp lý rõ ràng với: sổ đỏ trao tay ngay từng nền và chủ đầu tư KITA Invest (thuộc KITA Group) đã cam kết sẽ đồng hành cùng nhà đầu tư - điều mà hiện nay rất hiếm dự án nào trên thị trường có thể đạt được.</p>', '2019-09-30 00:15:34', '2019-09-30 02:40:55'),
(11, 'Tầm nhìn trong kinh doanh bất động sản của KITA Group', 'tam-nhin-trong-kinh-doanh-bat-dong-san-cua-kita-group', '<p style=\"text-align: justify;\">KITA Group hiện có quỹ đất 230 hecta với vị trí đắc địa trải dài từ Bắc vào Nam là lợi thế cạnh tranh trên thị trường bất động sản Việt Nam.</p>\r\n\r\n<p style=\"text-align: justify;\">\r\n	<br><strong>Dấu ấn mới trên thị trường&nbsp;</strong></p>\r\n\r\n<p style=\"text-align: justify;\">KITA Group là doanh nghiệp trẻ trên thị bất động sản Việt Nam và đang vươn lên khẳng định vị thế tại thị trường trong nước. KITA được viết tắt từ 4 yếu tố: Kiên định (Keeness) - &nbsp;Đột phá (Innovation) - Niềm tin (Trust) - &nbsp;Khát vọng (Aspriration).</p>\r\n\r\n<p style=\"text-align: justify;\">Hiện tập đoàn gồm 4 công ty thành viên hoạt động trong nhiều lĩnh vực: KITA Land (quản lý tài sản, phát triển dự án, phân phối sản phẩm), KITA Invest (mua bán sát nhập, đầu tư bất động sản, đầu tư tài chính), KITA Cons (xây dựng tòa nhà cao tầng, xây dựng khu đô thị, hoàn thiện nội thất) và KITA Link (thu đổi ngoại tệ, phát triển dự án C2C).</p>\r\n\r\n<p style=\"text-align: justify;\">Với tầm nhìn chiến lược, KITA Group đang không ngừng mở rộng đầu tư trong lĩnh vực địa ốc với tâm thế của một doanh nghiệp có tiềm lực tài chính dồi dào, nguồn lực vững chắc trong đầu tư, uy tín đảm bảo chất lượng xây dựng và năng lực trong quản lý vận hành các dự án của mình.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569827952663.png\" style=\"width: 456px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Bà Đặng Thị Thùy Trang - CEO của KITA Group cho biết, bất kì một doanh nghiệp nào cũng cần tài chính mạnh và tầm nhìn tốt để phát triển bền vững. \"Tài chính vững mạnh không những đảm bảo cho sự phát triển của chúng tôi, nó còn đảm bảo cho mối quan hệ của chúng tôi với các đối tác. Tầm nhìn tốt sẽ giúp doanh nghiệp bất động sản phát triển các sản phẩm bền vững\", bà Trang nói.</p>\r\n\r\n<p style=\"text-align: justify;\">Doanh nghiệp cho biết đang nắm giữ quỹ đất hơn 230 ha với vị trí đắc địa trải dài từ Bắc vào Nam. Đây là tiền đề để đơn vị phát triển triển và cạnh tranh trên \"sân chơi\" bất động sản tại Việt Nam.</p>\r\n\r\n<p style=\"text-align: justify;\">Với định hướng phát triển thị trường và đẩy mạnh năng lực cạnh tranh KITA Group đã phát triển nhiều dự án trên thị trường như: dự án căn hộ cao cấp 5 sao - Stella 927 tọa lạc trên đường Trần Hưng Đạo (TP HCM); dự án căn hộ cao cấp 5 sao - Stella 520 ở mặt tiền Đại lộ Võ Văn Kiệt.</p>\r\n\r\n<p style=\"text-align: justify;\">Dự án khu đô thị mới tại Quốc Oai thuộc thủ đô Hà Nội, khu đô thị sinh thái Golden Hills thuộc huyện Hòa Vang và quận Liên Chiểu, là cửa ngõ phía Tây Bắc thành phố Đà Nẵng. KITA đang là đối tác chiến lược của nhiều tên tuổi lớn như Trung Nam Group, Sacombank, VietinBank...</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Tạo nên giá trị khác biệt</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Về quyết cạnh tranh trên thị trường, CEO của KITA Group chia sẻ: \"Sự kiên định của chúng tôi mang lại những sản phẩm bất động sản mới cho thị trường, từ đó xây dựng niềm tin của khách hàng vào một khát vọng cho một cuộc sống chất lượng tốt hơn\".</p>\r\n\r\n<p style=\"text-align: justify;\">Hiện KITA tập trung vào các giá trị gia tăng bền vững cho các nhà đầu tư cũng như tiện ích sống, pháp lý cho cư dân tại các dự án do tập đoàn phát triển. Mục tiêu của doanh nghiệp là vào top đầu các công ty bất động sản tại Việt Nam.</p>\r\n\r\n<p style=\"text-align: justify;\">Bên cạnh đó, KITA cũng đẩy mạnh phát triển theo hướng tập trung và tối ưu nguồn lực để trở thành nhà đầu tư và phát triển dự án có tiềm lực mạnh, không chỉ cung cấp các sản phẩm tốt ra thị trường mà còn đồng hành cùng người tiêu dùng trong quá trình sử dụng, trải nghiệm và duy trì sản phẩm.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569828015999.png\" style=\"width: 520px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Giữa năm 2019, KITA Group tạo dấu ấn trên thị trường bất động sản khi đầu tư hơn 8.000 tỷ đồng phát triển dự án Stella Mega City - đại đô thị tọa lạc ngay tại trung tâm thành phố Cần Thơ. Dự án nằm tại khu vực quận Bình Thuỷ, là một trong những quỹ đất đắc địa ngay cạnh sân bay quốc tế Cần Thơ và trung tâm hành chính quận Bình Thủy.</p>\r\n\r\n<p style=\"text-align: justify;\">Đây là một trong số dự án khu đô thị đẳng cấp nhất Cần Thơ với chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế. Chủ đầu tư cam kết tính pháp lý cho khách hàng với 3 tiêu chí: đất sạch, pháp lý sạch và nôi trường sống sạch.</p>', '2019-09-30 00:21:31', '2019-09-30 02:37:15'),
(12, 'Dự án mới tại Cần Thơ giúp người dân chạm tay đến ước mơ cuộc sống “an lành – thịnh vượng”', 'du-moi-tai-can-tho-giup-nguoi-dan-cham-tay-den-uoc-mo-cuoc-song-lanh-thinh-vuong', '<p>Vốn đầu tư lên tới 8.000 tỉ, dự án đại đô thị tại trung tâm quận Bình Thuỷ sẽ thúc đẩy quá trình đô thị hóa tại Cần Thơ diễn ra nhanh hơn và thu hút nhà đầu tư trên khắp cả nước, bên cạnh đó tạo nên không gian sống an lành và thịnh vượng cho cư dân nơi đây.</p>\r\n\r\n<p><strong>Chạm tay đến giấc mơ “Sống an lành và thịnh vượng”</strong></p>\r\n\r\n<p>Đô thị hóa là quá trình bắt buộc và tất yếu với mọi thành phố lớn nhỏ trên cả nước, với vị thế là cửa ngõ kinh tế của khu vực Đồng bằng sông Cửu Long, thành phố (TP) Cần Thơ đang từng bước thúc đẩy nhanh quá trình đô thị hóa để bắt kịp cả về kinh tế lẫn xã hội với những TP lớn như: Hà Nội, Hồ Chí Minh, Đà Nẵng...</p>\r\n\r\n<p>Là 1 trong 10 thành phố có hệ thống kênh, rạch đẹp nhất thế giới, định hướng phát triển đến năm 2025, Cần Thơ sẽ là trung tâm công nghiệp, thương mại, dịch vụ, du lịch, giáo dục đào tạo, khoa học công nghệ, y tế, văn hóa của vùng Đồng bằng Sông Cửu Long.</p>\r\n\r\n<p>Hiện thành phố đã có gần 1,5 triệu dân với tỷ lệ đô thị hóa cao. Đặc biệt, sau khi có thông tin đầu tư hạ tầng như: tuyến đường sắt cao tốc nối Cần Thơ với TP.Hồ Chí Minh, tuyến cao tốc Trung Lương – Mỹ Thuận, Mỹ Thuận – Cần Thơ, khởi công cầu Mỹ Thuận 2,... và những chính sách mở cửa của lãnh đạo khi phê duyệt danh mục đầu tư xây dựng các khu đô thị mới trên địa bàn làm cơ sở mời gọi các nhà đầu tư – cũng là nguyên nhân chính giúp thị trường bất động sản tại Cần Thơ vụt lên thành tâm điểm đầu tư trên cả nước.</p>\r\n\r\n<p>Chỉ trong thời gian ngắn, từ cuối 2018 đến đầu 2019, hàng loạt dự án đô thị lớn đổ bộ vào Cần Thơ, tăng thêm sức nóng cho thị trường bất động sản tại đây. Trong đó, dự án đại đô thị 5 sao Stella Mega City với số vốn ban đầu lên tới 8.000 nghìn tỉ VNĐ của chủ đầu tư đầy tiềm lực KITA Invest (thuộc tập đoàn KITA Group) sẽ là dự án tiên phong giúp cư dân chạm tay đến giấc mơ “Sống an lành và thịnh vượng”.</p>\r\n\r\n<p>Nằm tại trung tâm Quận Bình Thủy, là một trong ba quận trung tâm của TP Cần Thơ với 2 nhà máy lớn và sân bay Quốc tế, Stella Mega City ẩn chứa sức hút dân cư mạnh mẽ. Tọa lạc trên mặt đường Đặng Văn Đầy và bên cạnh dòng sông Hậu yên bình, cơ quan hành chính quận Bình Thủy nằm ngay tại dự án và đã đi vào hoạt động, cách sân bay quốc tế Cần Thơ chỉ 5 phút, 7 phút tới hệ thống bệnh viện, trường học, cách địa danh du lịch nổi tiếng... Vị trí mà Stella Mega City sở hữu chính là trái tim trung tâm với tiềm năng phát triển to lớn cả về kinh tế lẫn xã hội. Tương lai không xa sẽ là đô thị lõi lớn mạnh của cả khu vực.</p>\r\n\r\n<p><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569829770559.png\" style=\"width: 608px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: center;\">Đại đô thị 5 sao có tổng vốn đầu tư 8.000 tỉ tại Cần Thơ sẽ trở thành đô thị lõi lớn mạnh trong tương lai</p>\r\n\r\n<p>Với quyết tâm trở thành khu đô thị hiện đại, năng động và góp phần giúp Cần Thơ phát triển xứng tầm với vị thế là cửa ngõ vùng Đồng bằng sông Cửu Long trong thời gian ngắn nhất có thể, Stella Mega City đã hoàn thiện cơ sở hạ tầng, hệ thống đường nội khu đã hình thành và đang được nâng cấp đạt chuẩn đô thị hiện đại.</p>\r\n\r\n<p>Với quy mô hơn 150 ha, dự án được trang bị những tiện ích nội khu 5 sao đẳng cấp: trung tâm thương mại phức hợp 5 sao, trung tâm hội nghị đa chức năng, khu phố thương mại, trường học các cấp chuẩn quốc tế, bệnh viện đa khoa quốc tế... Xen kẽ những tiện ích xa hoa đấy là không gian xanh được qui hoạch chỉn chu và tỉ mỉ theo nhiều chủ đề: clubhouse với bể bơi tràn vô cực, quảng trường thời đại, công viên cây xanh và hồ điều hòa Tropical Garden... Tất cả đều nhằm mục đích cung cấp đầy đủ những yếu tố cần thiết cho một không gian sống xanh, an lành, đẳng cấp và thịnh vượng cho cư dân nơi đây.</p>\r\n\r\n<p><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569829838702.png\" style=\"width: 655px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: center;\">Khu vực Vườn nhiệt đới theo phong cách Nhật Bản đem lại không gian sống xanh và an lành cho cư dân</p>\r\n\r\n<p style=\"text-align: left;\"><strong>Xuất hiện đầy ấn tượng với pháp lý minh bạch</strong></p>\r\n\r\n<p>Trong lúc thị trường bất động sản tại Cần Thơ đang sôi sục vì hàng chục dự án đô thị lớn nhỏ,&nbsp;<a href=\"http://stella-megacity.com/\" rel=\"nofollow\" target=\"_blank\">Stella Mega City</a> nổi bật với quy mô đặc biệt lớn cả về diện tích lẫn tiện ích. Tất cả đều là những yếu tố then chốt giúp Stella Mega City trở thành dự án tiên phong, giữ vị thế độc tôn trên thị trường bất động sản Cần Thơ – là cú huých lớn tới các nhà đầu tư đất nền ngay khi vừa ra mắt.</p>\r\n\r\n<p>Bên cạnh đó, điểm nổi bật khiến Stella Mega City trở thành ngôi sao sáng đáng chú ý với giới đầu tư bất động sản trên cả nước chứ không riêng gì Cần Thơ, đấy chính là tính pháp lý rõ ràng của dự án: Chủ đầu tư KITA Invest cam kết đồng hành cùng nhà đầu tư và cung cấp ngay sổ đỏ chứng nhận quyền sử dụng đất cho từng nền.</p>\r\n\r\n<p>Là cơ hội đầu tư an toàn và đầy tiềm năng với giấy tờ pháp lý đầy đủ hoàn chỉnh, quy mô dự án lớn và chỉn chu với tầm nhìn trở thành đô thị lõi của Cần Thơ – Stella Mega City dù chỉ vừa mới xuất hiện đã gây được tiếng vang lớn trên thị trường bất động sản Cần Thơ.</p>\r\n\r\n<p>Đặc biệt, để tri ân các nhà đầu tư và khách hàng đã dành sự quân tâm sâu sắc, chủ đầu tư KITA invest sẽ mở bán giới hạn một số nền đất tọa lạc gần không gian vườn Nhật Bản với giá khởi điểm không thể tốt hơn tại thị trường hiện nay. Với số tiền đầu tư chỉ tương đương một căn chung cư tầm thấp ở TP.Hồ Chí Minh và Hà Nội, bạn đã sở hữu một sản phẩm bất động sản với pháp lý sổ đỏ trao tay – lợi nhuận liền tay.</p>\r\n\r\n<p><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569829905049.png\" style=\"width: 465px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: center;\">Stella Mega City với pháp lý sổ đỏ từng nền tạo an tâm cho nhà đầu tư</p>\r\n\r\n<p style=\"text-align: left;\">Ở thời điểm giới kinh doanh bất động sản đã dần chuyển dòng vốn đến với những thành phố đang phát triển như Cần Thơ, Stella Mega City chính là điểm đến đầy tiềm năng và là cơ hội đầu tư hiếm có trong giai đoạn này.</p>\r\n\r\n<p>\r\n	<br>\r\n</p>\r\n\r\n<p style=\"text-align: center;\">\r\n	<br>\r\n</p>', '2019-09-30 00:53:03', '2019-09-30 00:53:03');
INSERT INTO `alipo_post_posts` (`id`, `title`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(13, 'Bình Thuỷ - Đất rồng vượng khí Tây Đô', 'binh-thuy-dat-rong-vuong-khi-tay-do', '<p style=\"text-align: justify;\">Đồng bằng sông Cửu Long có vị trí và vai trò đặc biệt quan trọng trong kế hoạch xây dựng, phát triển kinh tế - xã hội của cả nước. Đây là một trong những đồng bằng lớn và màu mỡ nhất Đông Nam Á với điều kiện tự nhiên và vị trí thuận lợi để phát triển không chỉ nông nghiệp mà còn giữ vai trò then chốt làm giàu mạnh về công nghiệp, dịch vụ.</p>\r\n\r\n<p style=\"text-align: justify;\">Trong những năm qua, ĐBSCL không ngừng vươn mạnh và phát triển, luôn có tỷ lệ đô thị hóa cao nhất cả nước với tốc độ tăng trưởng kinh tế cao hơn tốc độ tăng trưởng bình quân của cả nước 1,3 đến 1,5 lần.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Thừa hưởng thiên nhiên trù phú, địa thế phong thuỷ hài hòa cộng với chính sách và chủ trương nhà nước, ĐBSCL nói chung và thành phố Cần Thơ nói riêng đang trở thành vùng đất tiềm năng không thể bỏ qua của những nhà đầu tư bất động sản trên cả nước.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Vùng đất có sông Rồng yên bình</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Quận Bình Thủy với trung tâm hành chính đặt tại phường Bình Thủy là khu vực kinh tế trọng yếu của thành phố Cần Thơ khi sở hữu 2 KCN lớn Trà Nóc 1 và Trà Nóc 2. Toàn quận có 2 cảng lớn là Cần Thơ, Hoàng Diệu và sân bay quốc tế Cần Thơ với 2 đường bay mới đến Hàn Quốc và Đài Loan. Tổng số khách du lịch đã đến Cần Thơ năm 2018 là 8,3 triệu lượt người, cao hơn cả Đà Nẵng.</p>\r\n\r\n<p style=\"text-align: justify;\">Thuở xưa, Bình Thủy là vùng đất với tên gọi khác là Long Tuyền. Tương truyền rằng Quan khâm sai Huỳnh Mẫn Đạt đã đổi tên nơi này thành Bình Thủy vì xét thấy địa thế tốt đẹp, an lành, hoa màu thịnh vượng, dân lạc nghiệp an cư. Năm Giáp Thìn 1906, với địa thế có phần thay đổi, tuy nhiên hai chữ Bình Thủy là đầy ý vị, quan tri phủ nơi đây đã một lần nữa đổi tên, đưa vùng đất này về “Bình Thủy - Long Tuyền”, với Bình Thủy là con sông yên bình và Long Tuyền mang ý nghĩa “sông Rồng”.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569830043840.png\" style=\"width: 534px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: center;\"><em>Bình Thủy là con sông yên bình và Long Tuyền mang ý nghĩa “sông Rồng”</em></p>\r\n\r\n<p style=\"text-align: justify;\">Những năm gần đây, Bình Thủy chuyển mình phát triển và trở thành một trong ba trung tâm công nghiệp chiến lược của Cần Thơ. Như thể rằng, Rồng thiêng Bình Thuỷ đã thổi một luồng sinh khí mới, để nơi đây không chỉ là vùng đất an lành, mà còn là nơi khởi đầu của nền kinh tế thịnh vượng, phát triển vượt bậc.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Đại đô thị thừa hưởng phúc khí từ địa thế “Rồng chầu nhả ngọc”</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Stella Mega City – đại đô thị văn minh cao cấp đã ra đời bên cạnh những giá trị lịch sử giàu đẹp. Vùng đất của dự án nằm bên con sông Bình Thủy nước chảy uốn khúc như thân rồng với hai hàm miệng rồng là Đình Bình Thuỷ và Chùa Nam Nhã đang nhả viên ngọc minh châu (biểu tượng chính là Cồn Linh).&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Dọc theo chiều sông là thân rồng với những đoạn rạch cắt ngang tượng trưng cho bốn chân rồng. Rạch Cam chia làm hai ngả tỏa ra như đuôi rồng với một bên kéo dài tận cầu Bông Vang. Đoạn sông từ miệng đến đuôi rồng kéo dài khoảng 11km. Nước sông tuy sâu nhưng đúng như cái tên, quanh năm mặt nước yên bình, sóng gợn lăn tăn như vảy rồng lấp lánh dưới ánh mặt trời.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569830142221.png\" style=\"width: 513px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: center;\"><em>Vùng đất nằm bên con sông Bình Thủy nước chảy uốn khúc như thân rồng&nbsp;</em></p>\r\n\r\n<p style=\"text-align: justify;\">Thật không quá lời khi nói Stella Mega City chính là vùng đất nằm trong lòng thế Rồng chầu nhả ngọc, thuộc chân Long - chính mạch. Nơi đây được xem như viên ngọc sáng mang đến cuộc sống Thịnh Đạt - An Lành - Phước Thọ, đặc biệt thích hợp để an cư lạc nghiệp.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Vùng đất này sẽ mang đến phúc lộc dồi dào, cơ nghiệp thịnh đạt và sinh khí cát lợi. Không chỉ thành đạt trong công danh mà còn thành công trong cử nghiệp cho con cháu; giúp gia thế an lành, phước thọ và ngày ngày hưng thịnh.</p>\r\n\r\n<p style=\"text-align: justify;\">Vị trí tọa lạc của Stella Mega City với ưu ái trọn vẹn sinh khí tốt lành của dòng chảy sông Hậu và sông Bình Thủy. Đường mặt tiền lớn Đặng Văn Đầy chạy xuyên qua khu đô thị được thiết kế khắt khe nhất về mặt phong thủy tạo thành Trục Thần Đạo.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Đường thẳng này có một phía chạy ra thẳng sông Hậu nhìn vào Cồn Sơn - Án Thơ, một đầu khác là khu đền Hùng linh thiêng. Trục Thần đạo này là một thiết kế về phong thủy mang tính huyền thuật Phương Đông mà chắc chắn rằng không có khu đô thị nào khác ở Việt Nam sở hữu. Chính điều này đã biến khu đô thị&nbsp;<a href=\"https://cafeland.vn/du-an/khu-do-thi-stella-mega-city-can-tho-2295.html\">Stella Mega City</a> trở thành dự án xứng đáng được đầu tư bậc nhất của khu vực ĐBSCL.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Cột đồng hồ Stella Mega City vượng khí tốt lành</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Nằm giữa Trục Thần đạo là trung tâm của khu Đại đô thị với một công trình Cột đồng hồ mang kiến trúc vô cùng đặc sắc. Đỉnh cột đặt một ngôi sao lớn biểu tượng cho cái tên của khu đô thị - Stella Mega City. Bên dưới là viên Ngọc minh châu đang tỏa sáng cùng bốn mặt đồng hồ với hai mặt hướng theo trục thần đạo, 2 mặt đồng hồ hướng theo hai phương vị còn lại.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Chân cột đồng hồ xếp vòng tròn 12 hình đại diện cho 12 con giáp, được lắp đặt phun nước theo cung giờ nhất định. Đặc biệt, khi đồng hồ điểm 12h trưa, toàn bộ 12 tượng hình sẽ đồng loạt phun nước trong tiếng chuông ngân từ chiếc đồng hồ bên trên.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569830216083.png\" style=\"width: 524px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Cột đồng hồ là một trong những kiến trúc đặc biệt của Stella Mega City mang ý nghĩa phong thủy chiêu nạp vận khí tốt lành theo 24 sơn (12 địa chi). Nơi đây sẽ trở thành một biểu tượng tốt lành không chỉ cho vùng đất Bình Thuỷ mà còn là biểu tượng thịnh vượng của thành phố Cần Thơ. Cột đồng hồ này sẽ do nghệ nhân được KITA Group mời hợp tác để hoàn chỉnh cho bức tranh vô giá nơi đây.</p>\r\n\r\n<p style=\"text-align: justify;\">Stella Mega City (SMC) - đại đô thị 5 sao được xem là một trong những dự án tiêu biểu của KITA Group trong năm 2019. Với quỹ đất rộng lên đến 150ha và hơn 5.000 quyền sử dụng đất, cùng với sinh khí tốt lành của vùng đất Bình Thuỷ - Long Tuyền, Stella Mega City thu hút giới đầu tư để đón đầu dòng chảy dịch chuyển đầu tư tại thành phố Cần Thơ.</p>', '2019-09-30 00:58:34', '2019-09-30 02:42:37'),
(14, 'Stella Mega City: Dự án sở hữu yếu tố phong thủy \"vàng\" trong bất động sản', 'stella-mega-city-du-so-huu-yeu-phong-thuy-vang-trong-bat-dong-san', '<p style=\"text-align: justify;\">Stella Mega City không chỉ sở hữu vị trí tam cận \"cận thị - cận giang - cận lộ\", mà còn là nằm ở thế đất đẹp, trong lòng thế Rồng chầu nhả ngọc, thuộc chân Long - chính mạch của dòng sông Bình Thủy.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569830371281.png\" style=\"width: 766px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: justify;\">Theo các chuyên gia phong thủy, việc lựa chọn nơi để an cư cần phải quan tâm đến thế đất, thế đất tốt sẽ mang lại vượng khí, tài lộc và sức khỏe cho gia chủ. Đấy chính là một trong những lý do khiến Stella Mega City trở thành dự án thu hút khách bậc nhất khu vực Đồng bằng sông Cửu Long ngay khi vừa ra mắt.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Stella Mega City sở hữu địa thế \"Rồng chầu nhả ngọc\"</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Vị trí tọa lạc của Stella Mega City được sinh ra với ưu ái trọn vẹn tất cả sinh khí tốt lành của dòng chảy sông Hậu và sông Bình Thủy. Đường mặt tiền lớn Đặng Văn Đầy chạy xuyên qua khu Đại đô thị được thiết kế khắt khe nhất về mặt phong thủy tạo thành Trục Thần Đạo. Đường thẳng này có một phía chạy ra thẳng sông Hậu nhìn vào Cồn Sơn - Án Thơ, một đầu khác là khu đền Hùng linh thiêng. Trục Thần đạo này là một thiết kế nổi bật về phong thủy được nhiều chuyên gia phong thủy đánh giá cao.</p>\r\n\r\n<p style=\"text-align: justify;\">Theo khái niệm phong thủy, Stella Mega City nằm bên con sông Bình Thủy nước chảy uốn lượn như thân rồng với hai hàm miệng rồng là Đình Bình Thủy và Chùa Nam Nhã đang nhả ngọc, cho thấy đây là vùng đấy địa linh.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569830449508.png\" style=\"width: 787px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n<div type=\"Photo\">\r\n\r\n	<p data-placeholder=\"[nhập chú thích]\" style=\"text-align: justify;\">Bình Thủy là con sông yên bình và Long Tuyền mang ý nghĩa \"sông Rồng\".</p>\r\n</div>\r\n\r\n<p style=\"text-align: justify;\">Ông cha ta xưa có câu:\"Cao một tấc là sơn, thấp một tấc là thủy, rồng gặp nước sẽ dừng, sinh khí gặp nước sẽ tụ\". Điều này cho thấy, Stella Mega City có thể đất nổi bật hơn cả so với các vùng đất xung quanh, lại nằm bên dòng sông Bình Thủy vượng khí. Điều này giúp gia chủ sống tại vùng đất này có sức khỏe, hút khí tài và lộc vượng.</p>\r\n\r\n<p style=\"text-align: justify;\"><strong>Thiết kế cột đồng hồ mang lại vượng khí tốt lành</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Tại dự án Stella Mega City cũng mang thiết kế đặc biệt đó chính là cột đồng hồ ngay tại trung tâm của khu đô thị. Theo thiết kế, cột đồng hồ nằm giữa Trục Thần, đỉnh cột đặt một ngôi sao lớn biểu tượng cho cái tên của khu đô thị - Stella Mega City. Bên dưới là viên Ngọc minh châu đang tỏa sáng cùng bốn mặt đồng hồ, với hai mặt hướng theo trục thần đạo, 2 mặt đồng hồ hướng theo hai phương vị còn lại. Chân cột đồng hồ xếp vòng tròn 12 hình đại diện cho 12 con giáp, được lắp đặt phun nước theo cung giờ nhất định. Đặc biệt, khi đồng hồ điểm 12h trưa, toàn bộ 12 tượng hình sẽ đồng loạt phun nước trong tiếng chuông ngân từ chiếc đồng hồ bên trên.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569830496041.png\" style=\"width: 782px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n<div type=\"Photo\">\r\n\r\n	<p data-placeholder=\"[nhập chú thích]\" style=\"text-align: justify;\">Stella Mega City được thiết kế một cột đồng hồ ngay tại trung tâm của khu đô thị điều này giúp mang lại vượng khí tốt lành cho cư dân.</p>\r\n</div>\r\n\r\n<p style=\"text-align: justify;\">Ngoài ra, cột đồng hồ còn là một trong những kiến trúc đặc biệt của Stella Mega City mang ý nghĩa phong thủy chiêu nạp vận khí tốt lành theo 24 sơn (12 địa chi). Nơi đây hứa hẹn sẽ trở thành một biểu tượng tốt lành không chỉ cho vùng đất Bình Thuỷ mà còn là biểu tượng thịnh vượng của thành phố Cần Thơ.</p>\r\n\r\n<p style=\"text-align: justify;\">Theo các chuyên gia, tầm nhìn của Stella Mega City được coi là có cảnh quan đẹp, lại được dòng sông Bình Thủy bao quanh – điều này là vô cùng tốt, vì đây chính là tượng trung cho một vùng đất an lành, nơi khởi đầu của nền kinh tế thịnh vượng, phát triển vượt bậc.</p>\r\n\r\n<p style=\"text-align: justify;\">Như vậy, dự án hội tụ đầy đủ những yếu tố quan trọng trong phong thủy, cùng với đó, Stella Mega City còn đạt các tiêu chí đắt giá khác trong phong thủy: Thiên thời - địa lợi - nhân hòa... Bởi vậy, đây đang là dự án được giới đầu tư quan tâm hàng đầu để đón đầu dòng chảy tài lộc.</p>\r\n\r\n<p style=\"text-align: justify;\">Được biết, Stella Mega City là khu đô thị 5 sao đang gây chú ý tại thị trường Cần Thơ. Đây được xem là dự án tâm huyết của KITA Group trong năm 2019. Dự án được phát triển trên quỹ đất rộng lên đến 150 hecta và hơn 5.000 quyền sử dụng đất.</p>\r\n\r\n<p style=\"text-align: justify;\">Dự án còn được đầu tư chuỗi tiện ích tiện nghi nội khu 5 sao đạt tiêu chuẩn quốc tế. Bao gồm: trường học chuẩn quốc tế các cấp, bệnh viên quốc tế, đại siêu thị Châu Á, vườn sinh thái và hồ điều hòa nhiệt đới tropical garden... khu vực nhà ở được quy hoạch đồng bộ với khu biệt thự Compound, khu phố thương mại kết hợp với những không gian giải trí theo chủ đề: clubhouse với bể bơi vô cực, khu vườn Bonsai Nhật Bản...</p>\r\n\r\n<p style=\"text-align: justify;\">Bên cạnh đó, điểm nổi bật khiến Stella Mega City trở thành ngôi sao sáng đáng chú ý với giới đầu tư bất động sản trên cả nước chứ không riêng gì Cần Thơ, đấy chính là tính pháp lý rõ ràng của dự án: Chủ đầu tư KITA Invest cam kết đồng hành cùng nhà đầu tư và cung cấp ngay sổ đỏ chứng nhận quyền sử dụng đất cho từng nền.</p>', '2019-09-30 01:04:19', '2019-09-30 03:10:31'),
(15, 'Dự án mới tại Cần Thơ giúp người dân chạm tay đến ước mơ  cuộc sống “an lành – thịnh vượng”', 'du-moi-tai-can-tho-giup-nguoi-dan-cham-tay-den-uoc-mo-cuoc-song-lanh-thinh-vuong', '<p style=\"text-align: justify;\"><strong><em>Vốn đầu tư lên tới 8.000 tỉ, dự án đại đô thị tại trung tâm quận Bình Thuỷ sẽ thúc đẩy quá trình đô thị hóa tại Cần Thơ diễn ra nhanh hơn và thu hút nhà đầu tư trên khắp cả nước, bên cạnh đó tạo nên không gian sống an lành và thịnh vượng cho cư dân nơi đây.</em></strong></p>\r\n\r\n<p style=\"text-align: justify;\"><strong>►Chạm tay đến giấc mơ “Sống an lành và thịnh vượng”</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Đô thị hóa là quá trình bắt buộc và tất yếu với mọi thành phố lớn nhỏ trên cả nước, với vị thế là cửa ngõ kinh tế của khu vực Đồng bằng sông Cửu Long, thành phố (TP) Cần Thơ đang từng bước thúc đẩy nhanh quá trình đô thị hóa để bắt kịp cả về kinh tế lẫn xã hội với những TP lớn như: Hà Nội, Hồ Chí Minh, Đà Nẵng...</p>\r\n\r\n<p style=\"text-align: justify;\">Là 1 trong 10 thành phố có hệ thống kênh, rạch đẹp nhất thế giới, định hướng phát triển đến năm 2025, Cần Thơ sẽ là trung tâm công nghiệp, thương mại, dịch vụ, du lịch, giáo dục đào tạo, khoa học công nghệ, y tế, văn hóa của vùng Đồng bằng Sông Cửu Long.</p>\r\n\r\n<p style=\"text-align: justify;\">Hiện thành phố đã có gần 1,5 triệu dân với tỷ lệ đô thị hóa cao. Đặc biệt, sau khi có thông tin đầu tư hạ tầng như: tuyến đường sắt cao tốc nối Cần Thơ với TP.Hồ Chí Minh, tuyến cao tốc Trung Lương – Mỹ Thuận, Mỹ Thuận – Cần Thơ, khởi công cầu Mỹ Thuận 2,... và những chính sách mở cửa của lãnh đạo khi phê duyệt danh mục đầu tư xây dựng các khu đô thị mới trên địa bàn làm cơ sở mời gọi các nhà đầu tư – cũng là nguyên nhân chính giúp thị trường bất động sản tại Cần Thơ vụt lên thành tâm điểm đầu tư trên cả nước.</p>\r\n\r\n<p style=\"text-align: justify;\">Chỉ trong thời gian ngắn, từ cuối 2018 đến đầu 2019, hàng loạt dự án đô thị lớn đổ bộ vào Cần Thơ, tăng thêm sức nóng cho thị trường bất động sản tại đây. Trong đó, dự án đại đô thị 5 sao Stella Mega City với số vốn ban đầu lên tới 8.000 nghìn tỉ VNĐ của chủ đầu tư đầy tiềm lực Kita Invest (thuộc tập đoàn KITA Group) sẽ là dự án tiên phong giúp cư dân chạm tay đến giấc mơ “Sống an lành và thịnh vượng”.</p>\r\n\r\n<p style=\"text-align: justify;\"><em>Nằm&nbsp;</em><em>tại</em><em>&nbsp;trung tâm Quận Bình Thủy, là&nbsp;</em><em>một trong ba quận trung tâm&nbsp;</em><em>của TP Cần Thơ&nbsp;</em>với 2 nhà máy lớn và sân bay Quốc tế, Stella Mega City ẩn chứa sức hút dân cư mạnh mẽ. Tọa lạc trên mặt đường Đặng Văn Dầy và bên cạnh dòng sông Hậu yên bình, cơ quan hành chính quận Bình Thủy nằm ngay tại dự án và đã đi vào hoạt động, cách sân bay quốc tế Cần Thơ chỉ 5 phút, 7 phút tới hệ thống bệnh viện, trường học, cách địa danh du lịch nổi tiếng... Vị trí mà Stella Mega City sở hữu chính là trái tim trung tâm với tiềm năng phát triển to lớn cả về kinh tế lẫn xã hội. Tương lai không xa sẽ là đô thị lõi lớn mạnh của cả khu vực.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569830931065.png\" style=\"width: 653px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: center;\"><em>Đại đô thị 5 sao có tổng vốn đầu tư 8.000 tỉ</em><em>&nbsp;tại Cần Thơ</em><em>&nbsp;sẽ trở thành đô thị lõi lớn mạnh trong tương lai.</em></p>\r\n\r\n<p style=\"text-align: justify;\">Với quyết tâm trở thành khu đô thị hiện đại, năng động và góp phần giúp Cần Thơ phát triển xứng tầm với vị thế là cửa ngõ vùng Đồng bằng sông Cửu Long trong thời gian ngắn nhất có thể, Stella Mega City đã hoàn thiện cơ sở hạ tầng, hệ thống đường nội khu đã hình thành và đang được nâng cấp đạt chuẩn đô thị hiện đại.</p>\r\n\r\n<p style=\"text-align: justify;\">Với quy mô hơn 150 ha, dự án được trang bị những tiện ích nội khu 5 sao đẳng cấp: trung tâm thương mại phức hợp 5 sao, trung tâm hội nghị đa chức năng, khu phố thương mại, trường học các cấp chuẩn quốc tế, bệnh viện đa khoa quốc tế... Xen kẽ những tiện ích xa hoa đấy là không gian xanh được qui hoạch chỉn chu và tỉ mỉ theo nhiều chủ đề: clubhouse với bể bơi tràn vô cực, quảng trường thời đại, công viên cây xanh và hồ điều hòa Tropical Garden... Tất cả đều nhằm mục đích cung cấp đầy đủ những yếu tố cần thiết cho một không gian sống xanh, an lành, đẳng cấp và thịnh vượng cho cư dân nơi đây.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569830953480.png\" style=\"width: 640px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: center;\"><em>Khu vực Vườn nhiệt đới theo phong cách Nhật Bản đem lại không gian sống xanh và an lành cho cư dân.</em></p>\r\n\r\n<p style=\"text-align: justify;\"><strong>►Xuất hiện đầy ấn tượng với pháp lý minh bạch</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Trong lúc thị trường bất động sản tại Cần Thơ đang sôi sục vì hàng chục dự án đô thị lớn nhỏ,&nbsp;<a href=\"http://stella-megacity.com/\">Stella Mega City</a> nổi bật với quy mô đặc biệt lớn cả về diện tích lẫn tiện ích. Tất cả đều là những yếu tố then chốt giúp Stella Mega City trở thành dự án tiên phong, giữ vị thế độc tôn trên thị trường bất động sản Cần Thơ – là cú huých lớn tới các nhà đầu tư đất nền ngay khi vừa ra mắt.</p>\r\n\r\n<p style=\"text-align: justify;\">Bên cạnh đó, điểm nổi bật khiến Stella Mega City trở thành ngôi sao sáng đáng chú ý với giới đầu tư bất động sản trên cả nước chứ không riêng gì Cần Thơ, đấy chính là tính pháp lý rõ ràng của dự án: Chủ đầu tư Kita Invest cam kết đồng hành cùng nhà đầu tư và cung cấp ngay sổ đỏ chứng nhận quyền sử dụng đất cho từng nền.&nbsp;</p>\r\n\r\n<p style=\"text-align: justify;\">Là cơ hội đầu tư an toàn và đầy tiềm năng với giấy tờ pháp lý đầy đủ hoàn chỉnh, quy mô dự án lớn và chỉn chu với tầm nhìn trở thành đô thị lõi của Cần Thơ – Stella Mega City dù chỉ vừa mới xuất hiện đã gây được tiếng vang lớn trên thị trường bất động sản Cần Thơ. Đặc biệt, để tri ân các nhà đầu tư và khách hàng đã dành sự quân tâm sâu sắc, chủ đầu tư KITA Invest sẽ mở bán giới hạn một số nền đất tọa lạc gần không gian vườn Nhật Bản với giá khởi điểm không thể tốt hơn tại thị trường hiện nay. Với số tiền đầu tư chỉ tương đương một căn chung cư tầm thấp ở TP.Hồ Chí Minh và Hà Nội, bạn đã sở hữu một sản phẩm bất động sản với pháp lý sổ đỏ trao tay – lợi nhuận liền tay.</p>\r\n\r\n<p style=\"text-align: justify;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569831001178.png\" style=\"width: 587px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p align=\"center\" style=\"text-align: center;\"><em>Stella Mega City&nbsp;</em><em>với pháp lý sổ đỏ từng nền tạo an tâm cho nhà đầu tư</em></p>\r\n\r\n<p style=\"text-align: justify;\">Ở thời điểm giới kinh doanh bất động sản đã dần chuyển dòng vốn đến với những thành phố đang phát triển như Cần Thơ, Stella Mega City chính là điểm đến đầy tiềm năng và là cơ hội đầu tư hiếm có trong giai đoạn này.</p>', '2019-09-30 01:11:59', '2019-09-30 02:50:22'),
(16, 'Xuôi về Cần Thơ ghé vùng đất “Rồng chầu nhả ngọc”', 'xuoi-ve-can-tho-ghe-vung-dat-rong-chau-nha-ngoc', '<p style=\"text-align: justify;\"><strong>Đồng bằng sông Cửu Long (ĐBSCL) có vị trí và vai trò đặc biệt quan trọng trong kế hoạch xây dựng, phát triển kinh tế - xã hội của cả nước. Đây là một trong những đồng bằng lớn và màu mỡ nhất Đông Nam Á với điều kiện tự nhiên và vị trí thuận lợi để phát triển không chỉ nông nghiệp mà còn giữ vai trò then chốt làm giàu mạnh về công nghiệp, dịch vụ.</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Trong những năm qua, ĐBSCL không ngừng vươn mạnh và phát triển, luôn có tỷ lệ đô thị hóa cao nhất cả nước với tốc độ tăng trưởng kinh tế cao hơn tốc độ tăng trưởng bình quân của cả nước 1,3-1,5 lần. Thừa hưởng thiên nhiên trù phú, địa thế phong thủy hài hoà cộng với chính sách và chủ trương nhà nước, ĐBSCL nói chung và thành phố Cần Thơ nói riêng trở thành vùng đất tiềm năng không thể bỏ qua của những nhà đầu tư bất động sản trên cả nước.</p>\r\n\r\n<p><strong>Bình Thủy&nbsp;- Cần Thơ: Vùng đất có sông Rồng yên bình</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Quận Bình Thủy với trung tâm hành chính đặt tại phường Bình Thủy là khu vực kinh tế trọng yếu của thành phố Cần Thơ khi sở hữu 2 khu công nghiệp lớn Trà Nóc 1 và Trà Nóc 2. Toàn quận có 2 cảng lớn là Cần Thơ, Hoàng Diệu và sân bay quốc tế Cần Thơ với 2 đường bay mới đến Hàn Quốc và Đài Loan (Trung Quốc). Tổng số khách du lịch đã đến Cần Thơ năm 2018 là 8,3 triệu lượt người.</p>\r\n\r\n<p style=\"text-align: justify;\">Thuở xưa Bình Thủy là vùng đất có tên gọi khác là Long Tuyền. Tương truyền rằng, quan Khâm sai Huỳnh Mẫn Đạt đã đổi tên nơi này thành Bình Thủy vì xét thấy địa thế tốt đẹp, an lành, hoa màu thịnh vượng, dân lạc nghiệp an cư. Năm Giáp Thìn (1906), với địa thế có phần thay đổi, tuy nhiên hai chữ Bình Thủy là đầy ý vị, quan tri phủ nơi đây đã một lần nữa đổi tên, đưa vùng đất này về “Bình Thủy - Long Tuyền”, với Bình Thủy là con sông yên bình và Long Tuyền mang ý nghĩa “sông Rồng”.</p>\r\n\r\n<p style=\"text-align: center;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569831188799.png\" style=\"width: 798px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n<figure>\r\n	<figcaption style=\"text-align: center;\">Bình Thủy là con sông yên bình và Long Tuyền mang ý nghĩa “sông Rồng”.</figcaption>\r\n</figure>\r\n\r\n<p style=\"text-align: justify;\">Những năm gần đây, Bình Thủy chuyển mình phát triển và trở thành một trong ba trung tâm công nghiệp chiến lược của Cần Thơ. Như thể rằng, Rồng thiêng Bình Thủy đã thổi một luồng sinh khí mới, để nơi đây không chỉ là vùng đất an lành, mà còn là nơi khởi đầu của nền kinh tế thịnh vượng, phát triển vượt bậc.</p>\r\n\r\n<p><strong>Đại đô thị thừa hưởng phúc khí từ địa thế “Rồng chầu nhả ngọc”</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Stella Mega City - Đại đô thị văn minh cao cấp đã ra đời bên cạnh những giá trị lịch sử giàu đẹp. Vùng đất của dự án nằm bên con sông Bình Thủy nước chảy uốn khúc như thân rồng với hai hàm miệng rồng là đình Bình Thủy và chùa Nam Nhã đang nhả viên ngọc Minh Châu (biểu tượng chính là Cồn Linh). Dọc theo chiều sông là thân rồng với những đoạn rạch cắt ngang tượng trưng cho bốn chân rồng. Rạch Cam chia làm hai ngả tỏa ra như đuôi rồng với một bên kéo dài tận cầu Bông Vang. Đoạn sông từ miệng đến đuôi rồng kéo dài khoảng 11km. Nước sông tuy sâu nhưng đúng như cái tên, quanh năm mặt nước yên bình, sóng gợn lăn tăn như vảy rồng lấp lánh dưới ánh mặt trời.</p>\r\n\r\n<p style=\"text-align: center;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569831211867.png\" style=\"width: 877px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n<figure>\r\n	<figcaption style=\"text-align: center;\">Vùng đất nằm bên con sông Bình Thủy nước chảy uốn khúc như thân rồng với hai hàm miệng rồng là đình Bình Thủy và chùa Nam Nhã đang nhả viên ngọc Minh Châu (biểu tượng chính là Cồn Linh).</figcaption>\r\n</figure>\r\n\r\n<p style=\"text-align: justify;\">Thật không quá lời khi nói Stella Mega City chính là vùng đất nằm trong lòng thế “Rồng chầu nhả ngọc”, nơi đây được xem như viên ngọc sáng mang đến cuộc sống \"Thịnh đạt - An lành - Phước thọ\", đặc biệt thích hợp để an cư lạc nghiệp. Vùng đất có “Rồng chầu nhả ngọc” được cho là sẽ mang đến phúc lộc dồi dào, cơ nghiệp thịnh đạt và sinh khí cát lợi cho người ở và cả cho con cháu; giúp gia thế an lành, phước thọ và ngày càng hưng thịnh.</p>\r\n\r\n<p style=\"text-align: justify;\">Vị trí tọa lạc của Stella Mega City được sinh ra và lĩnh nhận trọn vẹn tất cả sinh khí tốt lành của dòng chảy sông Hậu và sông Bình Thủy. Đường mặt tiền lớn Đặng Văn Đầy chạy xuyên qua khu Đại đô thị được thiết kế khắt khe nhất về mặt phong thủy tạo thành \"Trục thần đạo\". Đường thẳng này có một phía chạy ra thẳng sông Hậu nhìn vào Cồn Sơn - Án Thơ, một đầu khác là khu Đền Hùng linh thiêng. \"Trục thần đạo\" này là một thiết kế tột bậc về phong thủy mang tính huyền thuật Phương Đông. Chính điều này đã biến khu đô thị Stella Mega City trở thành dự án xứng đáng được đầu tư bậc nhất của khu vực ĐBSCL.</p>\r\n\r\n<p><strong>Cột đồng hồ Stella Mega City vượng khí tốt lành</strong></p>\r\n\r\n<p style=\"text-align: justify;\">Nằm giữa \"Trục thần đạo\" là trung tâm của khu Đại đô thị với một công trình Cột đồng hồ mang kiến trúc vô cùng đặc sắc. Đỉnh cột đặt một ngôi sao lớn biểu tượng cho cái tên của khu đô thị - Stella Mega City. Bên dưới là viên ngọc Minh Châu đang tỏa sáng cùng bốn mặt đồng hồ với hai mặt hướng theo trục thần đạo, hai mặt đồng hồ hướng theo hai phương vị còn lại. Chân cột đồng hồ xếp vòng tròn 12 hình đại diện cho 12 con giáp, được lắp đặt phun nước theo cung giờ nhất định. Đặc biệt, khi đồng hồ điểm 12h trưa, toàn bộ 12 tượng hình sẽ đồng loạt phun nước trong tiếng chuông ngân từ chiếc đồng hồ bên trên.</p>\r\n\r\n<p style=\"text-align: center;\"><img src=\"http://stella-megacity.com/storage/app/media/uploaded-files/1569831252651.png\" style=\"width: 809px;\" class=\"fr-fic fr-dib\" data-result=\"success\"></p>\r\n\r\n<p style=\"text-align: center;\">\r\n	<br>\r\n</p>\r\n\r\n<p style=\"text-align: justify;\">Cột đồng hồ là một trong những kiến trúc đặc biệt của Stella Mega City mang ý nghĩa phong thủy chiêu nạp vận khí tốt lành theo 24 sơn (12 địa chi). Nơi đây sẽ trở thành một biểu tượng tốt lành không chỉ cho vùng đất Bình Thủy mà còn là biểu tượng thịnh vượng của thành phố Cần Thơ. Cột đồng hồ này sẽ do nghệ nhân xuất sắc (KITA Group mời) hợp tác để hoàn chỉnh cho bức tranh vô giá nơi đây.</p>\r\n\r\n<p style=\"text-align: justify;\">Stella Mega City (SMC) - Đại đô thị 5 sao được xem là một trong những dự án tiêu biểu của KITA Group trong năm 2019 và là ngôi sao sáng trong thị trường bất động sản Cần Thơ trong thời gian tới. Với quỹ đất rộng lên đến 150ha và hơn 5.000 quyền sử dụng đất, cùng với sinh khí tốt lành của vùng đất Bình Thủy - Long Tuyền, Stella Mega City thu hút giới đầu tư để đón đầu dòng chảy dịch chuyển đầu tư tại thành phố Cần Thơ.</p>', '2019-09-30 01:15:03', '2019-09-30 02:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_project_projects`
--

CREATE TABLE `alipo_project_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_floor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `architecture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_project_projects`
--

INSERT INTO `alipo_project_projects` (`id`, `title`, `slug`, `num_floor`, `area`, `architecture`, `description`, `created_at`, `updated_at`) VALUES
(1, 'NHÀ PHỐ THƯƠNG MẠI HIỆN ĐẠI', 'nha-pho-thuong-mai-hien-dai', 'N/A', 'N/A', 'N/A', '<p>Là những thiết kế tâm huyết của đội ngũ kiến trúc sư hàng đầu , mỗi căn nhà phố đều được thiết kế chỉnh chu, chú trọng vào sự hài hòa với thiên nhiên cùng sự tối ưu hóa công năng sử dụng, với mặt tiền cực rộng từ 7m, đảm bảo không gian đón nắng gió tự nhiên, đem lại vượng khí cho gia chủ. Với mô hình lối vào hầm nguyên khối nhưng vẫn tách biệt theo từng căn và thuận tiện với lối dẫn riêng lên từng nhà, hiện đại nhưng vẫn đảm bảo sự thuận tiện, riêng tư.</p>', '2019-07-09 00:20:16', '2019-07-17 02:08:44');

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2019-07-07 10:08:14', '2019-07-07 10:08:14'),
(2, 1, '115.77.187.74', '2019-07-09 22:08:20', '2019-07-09 22:08:20'),
(3, 1, '118.71.170.224', '2019-07-09 22:37:42', '2019-07-09 22:37:42'),
(4, 1, '118.71.170.224', '2019-07-10 01:51:02', '2019-07-10 01:51:02'),
(5, 1, '118.71.170.224', '2019-07-10 02:03:12', '2019-07-10 02:03:12'),
(6, 1, '171.235.143.123', '2019-07-13 23:35:38', '2019-07-13 23:35:38'),
(7, 1, '171.235.143.123', '2019-07-14 00:13:52', '2019-07-14 00:13:52'),
(8, 1, '171.235.143.123', '2019-07-14 00:17:14', '2019-07-14 00:17:14'),
(9, 1, '171.235.143.123', '2019-07-14 00:17:56', '2019-07-14 00:17:56'),
(10, 1, '171.235.143.123', '2019-07-14 00:18:24', '2019-07-14 00:18:24'),
(11, 1, '118.71.170.225', '2019-07-14 21:17:24', '2019-07-14 21:17:24'),
(12, 1, '116.109.32.79', '2019-07-15 17:35:55', '2019-07-15 17:35:55'),
(13, 1, '116.109.32.79', '2019-07-15 17:37:31', '2019-07-15 17:37:31'),
(14, 1, '1.53.114.49', '2019-07-17 01:30:43', '2019-07-17 01:30:43'),
(15, 1, '118.68.123.21', '2019-07-22 04:19:29', '2019-07-22 04:19:29'),
(16, 1, '118.68.123.21', '2019-07-22 04:20:07', '2019-07-22 04:20:07'),
(17, 1, '1.53.114.48', '2019-07-23 01:29:11', '2019-07-23 01:29:11'),
(18, 1, '::1', '2019-07-27 21:44:52', '2019-07-27 21:44:52'),
(19, 1, '116.109.17.28', '2019-07-28 07:09:57', '2019-07-28 07:09:57'),
(20, 1, '1.53.160.151', '2019-08-10 10:10:51', '2019-08-10 10:10:51'),
(21, 1, '113.176.62.219', '2019-08-11 19:15:38', '2019-08-11 19:15:38'),
(22, 1, '::1', '2019-08-23 20:45:37', '2019-08-23 20:45:37'),
(23, 1, '::1', '2019-09-01 23:27:45', '2019-09-01 23:27:45'),
(24, 1, '::1', '2019-09-02 09:59:16', '2019-09-02 09:59:16'),
(25, 1, '::1', '2019-09-02 10:01:12', '2019-09-02 10:01:12'),
(26, 2, '::1', '2019-09-02 10:04:47', '2019-09-02 10:04:47'),
(27, 1, '::1', '2019-09-02 10:05:24', '2019-09-02 10:05:24'),
(28, 2, '::1', '2019-09-02 10:08:23', '2019-09-02 10:08:23'),
(29, 1, '::1', '2019-09-02 10:08:48', '2019-09-02 10:08:48'),
(30, 2, '::1', '2019-09-02 10:10:00', '2019-09-02 10:10:00'),
(31, 2, '27.78.224.18', '2019-09-02 10:35:21', '2019-09-02 10:35:21'),
(32, 1, '118.71.170.240', '2019-09-02 22:04:22', '2019-09-02 22:04:22'),
(33, 2, '118.71.170.240', '2019-09-02 22:07:18', '2019-09-02 22:07:18'),
(34, 2, '113.176.62.219', '2019-09-02 23:55:14', '2019-09-02 23:55:14'),
(35, 1, '118.71.170.240', '2019-09-02 23:58:51', '2019-09-02 23:58:51'),
(36, 1, '118.71.170.240', '2019-09-03 00:00:47', '2019-09-03 00:00:47'),
(37, 1, '113.176.62.219', '2019-09-03 00:11:23', '2019-09-03 00:11:23'),
(38, 1, '113.176.62.219', '2019-09-04 19:07:41', '2019-09-04 19:07:41'),
(39, 1, '113.176.62.219', '2019-09-06 03:02:55', '2019-09-06 03:02:55'),
(40, 2, '14.234.78.107', '2019-09-06 08:35:18', '2019-09-06 08:35:18'),
(41, 1, '14.234.78.107', '2019-09-06 09:11:05', '2019-09-06 09:11:05'),
(42, 1, '::1', '2019-09-11 08:44:54', '2019-09-11 08:44:54'),
(43, 2, '103.199.57.114', '2019-09-16 17:25:49', '2019-09-16 17:25:49'),
(44, 1, '::1', '2019-09-18 16:07:15', '2019-09-18 16:07:15'),
(45, 1, '115.77.187.74', '2019-09-18 23:00:42', '2019-09-18 23:00:42'),
(46, 1, '115.73.213.13', '2019-09-21 22:08:20', '2019-09-21 22:08:20'),
(47, 1, '103.199.56.229', '2019-09-21 22:21:21', '2019-09-21 22:21:21'),
(48, 1, '116.102.17.183', '2019-09-22 08:04:14', '2019-09-22 08:04:14'),
(49, 1, '116.102.17.183', '2019-09-22 08:07:49', '2019-09-22 08:07:49'),
(50, 2, '118.68.62.95', '2019-09-22 11:22:08', '2019-09-22 11:22:08'),
(51, 1, '118.68.62.95', '2019-09-22 11:22:22', '2019-09-22 11:22:22'),
(52, 1, '118.71.170.231', '2019-09-22 19:47:24', '2019-09-22 19:47:24'),
(53, 1, '115.77.187.74', '2019-09-22 21:03:56', '2019-09-22 21:03:56'),
(54, 1, '113.176.62.219', '2019-09-25 18:45:11', '2019-09-25 18:45:11'),
(55, 1, '1.53.114.151', '2019-09-25 19:22:53', '2019-09-25 19:22:53'),
(56, 1, '113.176.62.219', '2019-09-25 19:29:04', '2019-09-25 19:29:04'),
(57, 1, '1.53.114.151', '2019-09-25 21:30:09', '2019-09-25 21:30:09'),
(58, 1, '42.116.118.224', '2019-09-28 03:43:41', '2019-09-28 03:43:41'),
(59, 1, '103.199.33.107', '2019-09-30 02:33:21', '2019-09-30 02:33:21'),
(60, 1, '113.176.62.219', '2019-09-30 03:26:59', '2019-09-30 03:26:59'),
(61, 1, '42.117.235.29', '2019-09-30 06:27:31', '2019-09-30 06:27:31');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `is_superuser`) VALUES
(1, 'Admin', 'Person', 'admin', 'admin@domain.tld', '$2y$10$3JN.su6hOr4zbQS6R2uYXOFenRcoez.GKUcyXvbiTiTsOmapxOOLu', NULL, '$2y$10$SksMofB81pUUfqkm6cq3kOeQ6RL9zwf8GodEjc.aws.boCxEPBHKK', NULL, '', 1, 2, NULL, '2019-09-30 06:27:31', '2019-07-07 10:08:09', '2019-09-30 06:27:31', NULL, 1),
(2, 'Content', 'Role', 'content', 'admin@domain.co', '$2y$10$luUC/q83dDkoFYDKMT31WupaC6fwKdfMALwCmNMMdlIDlZX5NXpt2', NULL, '$2y$10$AcsFB/3PeRQXQcd0vJHr4uoMFO2Vd6eHK0zgyKiePK1mhQuJYBlSG', NULL, '{\"cms.manage_content\":-1,\"cms.manage_theme_options\":-1,\"cms.manage_assets\":-1,\"cms.manage_pages\":-1,\"cms.manage_layouts\":-1,\"cms.manage_partials\":-1,\"cms.manage_themes\":-1,\"anandpatel.wysiwygeditors.settings\":-1,\"system.manage_mail_templates\":-1,\"backend.access_dashboard\":-1,\"system.manage_mail_settings\":-1,\"system.access_logs\":-1,\"system.manage_updates\":-1,\"backend.manage_default_dashboard\":-1,\"backend.manage_branding\":-1,\"backend.manage_editor\":-1,\"backend.manage_preferences\":-1,\"backend.impersonate_users\":-1,\"backend.manage_users\":-1,\"laminsanneh.flexicontact.access_settings\":-1,\"benfreke.menumanager.access_menumanager\":1}', 0, 1, NULL, '2019-09-22 11:22:08', '2019-09-02 10:04:35', '2019-09-22 11:22:08', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2019-07-07 10:08:09', '2019-07-07 10:08:09', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2019-07-07 10:08:09', '2019-07-07 10:08:09'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2019-07-07 10:08:09', '2019-07-07 10:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL),
(2, 1, '115.77.187.74', 0, NULL, 0, NULL, 0, NULL),
(3, 1, '118.71.170.224', 0, NULL, 0, NULL, 0, NULL),
(4, 1, '113.23.87.230', 0, NULL, 0, NULL, 0, NULL),
(5, 1, '27.78.230.97', 0, NULL, 0, NULL, 0, NULL),
(6, 1, '118.71.170.218', 0, NULL, 0, NULL, 0, NULL),
(7, 1, '116.109.77.165', 0, NULL, 0, NULL, 0, NULL),
(8, 1, '118.71.170.221', 0, NULL, 0, NULL, 0, NULL),
(9, 1, '171.235.143.123', 0, NULL, 0, NULL, 0, NULL),
(10, 1, '118.71.170.225', 0, NULL, 0, NULL, 0, NULL),
(11, 1, '116.109.32.79', 0, NULL, 0, NULL, 0, NULL),
(12, 1, '1.53.114.49', 0, NULL, 0, NULL, 0, NULL),
(13, 1, '118.68.123.21', 0, NULL, 0, NULL, 0, NULL),
(14, 1, '113.185.77.57', 0, NULL, 0, NULL, 0, NULL),
(15, 1, '1.53.114.48', 0, NULL, 0, NULL, 0, NULL),
(16, 1, '116.109.17.28', 0, NULL, 0, NULL, 0, NULL),
(17, 1, '1.53.114.61', 0, NULL, 0, NULL, 0, NULL),
(18, 1, '1.53.114.107', 0, NULL, 0, NULL, 0, NULL),
(19, 1, '113.185.75.65', 0, NULL, 0, NULL, 0, NULL),
(20, 1, '42.118.232.127', 0, NULL, 0, NULL, 0, NULL),
(21, 1, '1.53.114.106', 0, NULL, 0, NULL, 0, NULL),
(22, 1, '1.53.160.151', 0, NULL, 0, NULL, 0, NULL),
(23, 1, '113.176.62.219', 0, NULL, 0, NULL, 0, NULL),
(24, 1, '116.109.183.94', 0, NULL, 0, NULL, 0, NULL),
(25, 2, '::1', 0, NULL, 0, NULL, 0, NULL),
(26, 2, '27.78.224.18', 0, NULL, 0, NULL, 0, NULL),
(27, 1, '118.71.170.240', 0, NULL, 0, NULL, 0, NULL),
(28, 2, '118.71.170.240', 0, NULL, 0, NULL, 0, NULL),
(29, 2, '113.176.62.219', 0, NULL, 0, NULL, 0, NULL),
(30, 1, '118.71.170.244', 0, NULL, 0, NULL, 0, NULL),
(31, 2, '14.234.78.107', 0, NULL, 0, NULL, 0, NULL),
(32, 1, '14.234.78.107', 0, NULL, 0, NULL, 0, NULL),
(33, 1, '58.187.175.226', 0, NULL, 0, NULL, 0, NULL),
(34, 1, '115.79.227.159', 0, NULL, 0, NULL, 0, NULL),
(35, 1, '118.71.170.64', 0, NULL, 0, NULL, 0, NULL),
(36, 1, '1.53.114.129', 0, NULL, 0, NULL, 0, NULL),
(37, 1, '116.109.14.19', 0, NULL, 0, NULL, 0, NULL),
(38, 2, '103.199.57.114', 0, NULL, 0, NULL, 0, NULL),
(39, 1, '103.199.57.114', 0, NULL, 0, NULL, 0, NULL),
(40, 1, '1.53.114.211', 0, NULL, 0, NULL, 0, NULL),
(41, 1, '118.71.170.232', 0, NULL, 0, NULL, 0, NULL),
(42, 1, '116.102.17.183', 0, NULL, 0, NULL, 0, NULL),
(43, 1, '118.71.170.227', 0, NULL, 0, NULL, 0, NULL),
(44, 1, '115.73.213.13', 0, NULL, 0, NULL, 0, NULL),
(45, 1, '103.199.56.229', 0, NULL, 0, NULL, 0, NULL),
(46, 2, '118.68.62.95', 0, NULL, 0, NULL, 0, NULL),
(47, 1, '118.68.62.95', 0, NULL, 0, NULL, 0, NULL),
(48, 1, '118.71.170.231', 0, NULL, 0, NULL, 0, NULL),
(49, 1, '1.53.114.151', 0, NULL, 0, NULL, 0, NULL),
(50, 1, '42.116.118.224', 0, NULL, 0, NULL, 0, NULL),
(51, 1, '118.71.170.79', 0, NULL, 0, NULL, 0, NULL),
(52, 1, '103.199.33.107', 0, NULL, 0, NULL, 0, NULL),
(53, 1, '42.117.235.29', 0, NULL, 0, NULL, 0, NULL),
(54, 1, '171.233.182.189', 0, NULL, 0, NULL, 0, NULL),
(55, 1, '118.71.170.73', 0, NULL, 0, NULL, 0, NULL),
(56, 1, '42.112.185.175', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `benfreke_menumanager_menus`
--

CREATE TABLE `benfreke_menumanager_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_external` tinyint(1) NOT NULL DEFAULT '0',
  `link_target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `enabled` int(11) NOT NULL DEFAULT '1',
  `parameters` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `query_string` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benfreke_menumanager_menus`
--

INSERT INTO `benfreke_menumanager_menus` (`id`, `parent_id`, `title`, `description`, `url`, `nest_left`, `nest_right`, `nest_depth`, `created_at`, `updated_at`, `is_external`, `link_target`, `enabled`, `parameters`, `query_string`) VALUES
(1, NULL, 'main menu', '', NULL, 1, 22, 0, '2019-07-08 07:15:00', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(2, 1, 'Trang chủ', '', 'home', 2, 3, 1, '2019-07-08 07:15:10', '2019-08-24 11:00:36', 0, '_self', 1, '', ''),
(3, 1, 'Tổng quan', '', 'introduction', 4, 5, 1, '2019-07-08 07:15:53', '2019-07-08 07:17:23', 0, '_self', 1, '', ''),
(4, 1, 'Vị trí', '', 'location', 6, 7, 1, '2019-07-08 07:16:16', '2019-07-08 07:17:25', 0, '_self', 1, '', ''),
(5, 1, 'Tiện ích', '', 'facilities', 8, 9, 1, '2019-07-08 07:16:31', '2019-07-08 07:17:26', 0, '_self', 1, '', ''),
(6, 1, 'Mặt bằng', '', 'types-houses', 10, 11, 1, '2019-07-08 07:16:48', '2019-07-08 07:17:28', 0, '_self', 1, '', ''),
(7, 1, 'Tin tức', '', 'news', 18, 19, 1, '2019-07-08 07:17:01', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(8, 1, 'Liên hệ', '', 'contact', 20, 21, 1, '2019-07-08 07:17:13', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(9, 1, 'Tiến độ', '', 'tien-do', 12, 13, 1, '2019-08-01 02:21:05', '2019-08-01 02:21:20', 0, '_self', 1, '', ''),
(10, 1, 'Chủ đầu tư', '', 'chu-dau-tu', 16, 17, 1, '2019-08-24 00:36:25', '2019-08-24 11:27:52', 0, '_self', 1, '', ''),
(11, NULL, 'Left menu', '', NULL, 23, 34, 0, '2019-08-24 11:02:10', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(12, 11, 'Trang chủ', '', 'home', 24, 25, 1, '2019-08-24 11:02:27', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(13, 11, 'Tổng quan', '', 'introduction', 26, 27, 1, '2019-08-24 11:02:41', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(14, 11, 'Vị trí', '', 'location', 28, 29, 1, '2019-08-24 11:02:53', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(15, 11, 'Tiện ích', '', 'facilities', 30, 31, 1, '2019-08-24 11:03:02', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(16, 11, 'Mặt bằng', '', 'types-houses', 32, 33, 1, '2019-08-24 11:03:14', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(17, NULL, 'Right menu', '', NULL, 35, 46, 0, '2019-08-24 11:03:46', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(18, 17, 'Tiến độ', '', 'tien-do', 36, 37, 1, '2019-08-24 11:03:59', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(19, 17, 'Chủ đầu tư', '', 'chu-dau-tu', 40, 41, 1, '2019-08-24 11:04:11', '2019-08-24 11:28:02', 0, '_self', 1, '', ''),
(21, 17, 'Liên hệ', '', 'contact', 44, 45, 1, '2019-08-24 11:04:39', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(22, 17, 'Thư viện', '', 'thu-vien', 38, 39, 1, '2019-08-24 11:04:59', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(23, 17, 'Tin tức', '', 'news', 42, 43, 1, '2019-08-24 11:06:13', '2019-08-24 11:27:10', 0, '_self', 1, '', ''),
(24, 1, 'Thư viện', '', 'thu-vien', 14, 15, 1, '2019-08-24 11:26:59', '2019-08-24 11:27:10', 0, '_self', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_templates`
--

CREATE TABLE `cms_theme_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(10) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deferred_bindings`
--

INSERT INTO `deferred_bindings` (`id`, `master_type`, `master_field`, `slave_type`, `slave_id`, `session_key`, `is_bind`, `created_at`, `updated_at`) VALUES
(9, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '50', 'WtNmWEtX7lrTOifQA3UQe9woxVqSdvAEmBc3G3IT', 1, '2019-07-09 22:56:30', '2019-07-09 22:56:30'),
(28, 'Alipo\\Page\\Models\\News', 'banner', 'System\\Models\\File', '59', 'q6WsIBmuyNZ1RyFzPf3gljKn0vln8T1nn2zM91BG', 1, '2019-07-09 23:22:34', '2019-07-09 23:22:34'),
(29, 'Alipo\\Page\\Models\\News', 'banner', 'System\\Models\\File', '60', 'q6WsIBmuyNZ1RyFzPf3gljKn0vln8T1nn2zM91BG', 1, '2019-07-09 23:22:34', '2019-07-09 23:22:34'),
(30, 'Alipo\\Page\\Models\\News', 'banner', 'System\\Models\\File', '61', 'q6WsIBmuyNZ1RyFzPf3gljKn0vln8T1nn2zM91BG', 1, '2019-07-09 23:22:36', '2019-07-09 23:22:36'),
(53, 'Alipo\\GeneralOption\\Models\\General', 'logo', 'System\\Models\\File', '4', 'x6TKbK3hH9AbLivSiUQoBIeUEtmYQz9WReOeTiCT', 0, '2019-07-09 23:43:41', '2019-07-09 23:43:41'),
(54, 'Alipo\\GeneralOption\\Models\\General', 'logo', 'System\\Models\\File', '77', 'x6TKbK3hH9AbLivSiUQoBIeUEtmYQz9WReOeTiCT', 1, '2019-07-09 23:43:53', '2019-07-09 23:43:53'),
(55, 'Alipo\\GeneralOption\\Models\\General', 'footer_logo', 'System\\Models\\File', '5', 'x6TKbK3hH9AbLivSiUQoBIeUEtmYQz9WReOeTiCT', 0, '2019-07-09 23:43:54', '2019-07-09 23:43:54'),
(56, 'Alipo\\GeneralOption\\Models\\General', 'footer_logo', 'System\\Models\\File', '78', 'x6TKbK3hH9AbLivSiUQoBIeUEtmYQz9WReOeTiCT', 1, '2019-07-09 23:44:01', '2019-07-09 23:44:01'),
(79, 'Alipo\\GeneralOption\\Models\\General', 'footer_logo', 'System\\Models\\File', '80', '4p6OcPhatBmYF9qm5nAMAA4p3i8V3uxiK3F4WC4q', 0, '2019-07-10 02:07:59', '2019-07-10 02:07:59'),
(109, 'Alipo\\Page\\Models\\Facility', 'banner', 'System\\Models\\File', '15', 'l3c3lG5RLFYXh4orLHu78sDDBrG9qYY6wOe9jF12', 0, '2019-07-10 07:50:04', '2019-07-10 07:50:04'),
(110, 'Alipo\\Page\\Models\\Facility', 'banner', 'System\\Models\\File', '16', 'l3c3lG5RLFYXh4orLHu78sDDBrG9qYY6wOe9jF12', 0, '2019-07-10 07:50:07', '2019-07-10 07:50:07'),
(111, 'Alipo\\Page\\Models\\Facility', 'banner', 'System\\Models\\File', '17', 'l3c3lG5RLFYXh4orLHu78sDDBrG9qYY6wOe9jF12', 0, '2019-07-10 07:50:09', '2019-07-10 07:50:09'),
(117, 'Alipo\\Page\\Models\\Introduction', 'banner', 'System\\Models\\File', '111', 'YvZy17fjOT5VrrsttQsyjNUy27EkVhGnKn05lKsv', 1, '2019-07-10 07:52:03', '2019-07-10 07:52:03'),
(184, 'Alipo\\Page\\Models\\Process', 'banner', 'System\\Models\\File', '145', 'CXBtXecvXNs5QZtJpx7RG4RTHYs81j0y5d3SlCDC', 1, '2019-07-27 22:06:59', '2019-07-27 22:06:59'),
(185, 'Alipo\\Page\\Models\\Home', 'gallery_image', 'System\\Models\\File', '160', '45dDary26xniybsVParhHYRGPM4nzsHhdGgpwgs6', 1, '2019-08-09 14:28:26', '2019-08-09 14:28:26'),
(186, 'Alipo\\Page\\Models\\Home', 'gallery_image', 'System\\Models\\File', '161', '45dDary26xniybsVParhHYRGPM4nzsHhdGgpwgs6', 1, '2019-08-09 14:28:26', '2019-08-09 14:28:26'),
(187, 'Alipo\\Page\\Models\\Home', 'gallery_image', 'System\\Models\\File', '162', '45dDary26xniybsVParhHYRGPM4nzsHhdGgpwgs6', 1, '2019-08-09 14:28:29', '2019-08-09 14:28:29'),
(188, 'Alipo\\Page\\Models\\Home', 'gallery_image', 'System\\Models\\File', '163', '45dDary26xniybsVParhHYRGPM4nzsHhdGgpwgs6', 1, '2019-08-09 14:28:29', '2019-08-09 14:28:29'),
(197, 'Alipo\\Page\\Models\\Home', 'gallery_image', 'System\\Models\\File', '195', 'ssEN4gUXtxY4hZlbb2x46X094Xun4falKbJPhiOZ', 0, '2019-08-24 08:06:49', '2019-08-24 08:06:49'),
(211, 'Alipo\\Page\\Models\\ChuDauTu', 'banner', 'System\\Models\\File', '206', 'H9XNf4SJMQAv6a5MKpSUNYwIoPRc6YFitDrYQWHy', 1, '2019-08-25 02:08:53', '2019-08-25 02:08:53'),
(212, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '191', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 0, '2019-09-06 09:22:26', '2019-09-06 09:22:26'),
(213, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '190', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 0, '2019-09-06 09:22:28', '2019-09-06 09:22:28'),
(214, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '189', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 0, '2019-09-06 09:22:34', '2019-09-06 09:22:34'),
(215, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '188', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 0, '2019-09-06 09:22:36', '2019-09-06 09:22:36'),
(216, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '218', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 1, '2019-09-06 09:22:47', '2019-09-06 09:22:47'),
(217, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '220', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 1, '2019-09-06 09:22:50', '2019-09-06 09:22:50'),
(218, 'Alipo\\Page\\Models\\Home', 'banner', 'System\\Models\\File', '219', '5uONDVdAesiF9u3AqIKsBCnG71s4wrcTm1IsOlWJ', 1, '2019-09-06 09:22:50', '2019-09-06 09:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2017_10_23_000024_Db_System_Mail_Layouts_Add_Options_Field', 1),
(26, '2013_10_01_000001_Db_Backend_Users', 2),
(27, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(28, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(29, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(30, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(31, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(32, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(33, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(34, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(35, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(36, '2018_12_16_000011_Db_Backend_Add_Deleted_At', 2),
(37, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(38, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(39, '2017_10_01_000003_Db_Cms_Theme_Logs', 3),
(40, '2018_11_01_000001_Db_Cms_Theme_Templates', 3);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_attributes`
--

CREATE TABLE `rainlab_translate_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_data` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_attributes`
--

INSERT INTO `rainlab_translate_attributes` (`id`, `locale`, `model_id`, `model_type`, `attribute_data`) VALUES
(1, 'en', '1', 'Alipo\\Page\\Models\\Home', '{\"title\":\"\",\"introduction_title\":\"\",\"introduction_des\":\"\",\"facilities_des\":\"\",\"type_house_dinfo\":null}'),
(2, 'en', '2', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"home\",\"description\":\"\"}'),
(3, 'en', '3', 'Alipo\\Project\\Models\\Project', '{\"title\":\"\",\"description\":\"\"}'),
(4, 'en', '1', 'Alipo\\Page\\Models\\Location', '{\"title\":\"\",\"node_position\":null,\"description\":\"\"}'),
(5, 'en', '1', 'Alipo\\Page\\Models\\Introduction', '{\"title\":\"\",\"description\":null}'),
(6, 'en', '1', 'Alipo\\GeneralOption\\Models\\General', '{\"title1\":\"\",\"des1\":\"\",\"phone\":\"\",\"social_network\":null,\"title2\":\"\",\"des2\":\"\",\"copyright\":\"\"}'),
(7, 'en', '1', 'Alipo\\Page\\Models\\News', '{\"title\":\"\",\"description\":\"\"}'),
(8, 'en', '1', 'Alipo\\Page\\Models\\TypeOfHouse', '{\"title\":\"\"}'),
(9, 'en', '1', 'Alipo\\Page\\Models\\Facility', '{\"title\":\"\"}'),
(10, 'en', '2', 'Alipo\\Page\\Models\\FacilityDetail', '{\"title\":\"\",\"description\":\"\"}'),
(11, 'en', '3', 'Alipo\\Page\\Models\\FacilityDetail', '{\"title\":\"\",\"description\":\"\"}'),
(12, 'en', '1', 'Alipo\\Page\\Models\\FacilityDetail', '{\"title\":\"\",\"description\":\"\"}'),
(13, 'en', '4', 'System\\Models\\File', '{\"title\":\"\",\"description\":\"\"}'),
(14, 'en', '1', 'Alipo\\Page\\Models\\Contact', '{\"title\":\"\",\"description\":\"\"}'),
(15, 'en', '2', 'Alipo\\Project\\Models\\Project', '{\"title\":\"\",\"description\":\"\"}'),
(16, 'en', '1', 'Alipo\\Project\\Models\\Project', '{\"title\":\"\",\"description\":\"\"}'),
(17, 'en', '4', 'Alipo\\Page\\Models\\FacilityDetail', '{\"title\":\"\",\"description\":\"\"}'),
(18, 'en', '1', 'Alipo\\Page\\Models\\Process', '{\"title\":\"\",\"slug\":\"\",\"info\":\"\"}'),
(19, 'en', '9', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(20, 'en', '5', 'Alipo\\Page\\Models\\FacilityDetail', '{\"title\":\"\",\"description\":\"\"}'),
(21, 'en', '6', 'Alipo\\Page\\Models\\FacilityDetail', '{\"title\":\"\",\"description\":\"\"}'),
(22, 'en', '10', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(23, 'en', '11', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(24, 'en', '12', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(25, 'en', '13', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(26, 'en', '14', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(27, 'en', '15', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(28, 'en', '16', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(29, 'en', '17', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(30, 'en', '18', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(31, 'en', '19', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(32, 'en', '20', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(33, 'en', '21', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(34, 'en', '22', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(35, 'en', '23', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(36, 'en', '24', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(37, 'en', '1', 'Alipo\\Page\\Models\\Gallery', '{\"title\":\"\",\"slug\":\"\",\"images\":null,\"videos\":[]}'),
(38, 'en', '1', 'Alipo\\Page\\Models\\ChuDauTu', '{\"title\":\"\",\"description\":null}');

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_indexes`
--

CREATE TABLE `rainlab_translate_indexes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_locales`
--

CREATE TABLE `rainlab_translate_locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_locales`
--

INSERT INTO `rainlab_translate_locales` (`id`, `code`, `name`, `is_default`, `is_enabled`, `sort_order`) VALUES
(1, 'en', 'English', 0, 1, 1),
(2, 'vi', 'Tiếng Việt', 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_messages`
--

CREATE TABLE `rainlab_translate_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_data` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_messages`
--

INSERT INTO `rainlab_translate_messages` (`id`, `code`, `message_data`) VALUES
(1, 'hình.phối.cảnh', '{\"x\":\"H\\u00ecnh ph\\u1ed1i c\\u1ea3nh\"}'),
(2, 'mặt.bằng', '{\"x\":\"M\\u1eb7t b\\u1eb1ng\"}'),
(3, 'hình.cắt.đứng', '{\"x\":\"H\\u00ecnh c\\u1eaft \\u0111\\u1ee9ng\"}'),
(4, 'các.mẫu.nhà.khác', '{\"x\":\"C\\u00c1C M\\u1eaaU NH\\u00c0 KH\\u00c1C\"}'),
(5, 'mẫu.nhà', '{\"x\":\"M\\u1eaaU NH\\u00c0\"}'),
(6, 'stella.mega.city', '{\"x\":\"STELLA MEGA CITY\"}'),
(7, 'phồn.vinh.cuộc.sống.tây.đô', '{\"x\":\"PH\\u1ed2N VINH CU\\u1ed8C S\\u1ed0NG T\\u00c2Y \\u0110\\u00d4\"}'),
(8, 'quy.mô', '{\"x\":\"Quy m\\u00f4\"}'),
(9, 'ha', '{\"x\":\"ha\"}'),
(10, 'chủ.đầu.tư', '{\"x\":\"Ch\\u1ee7 \\u0111\\u1ea7u t\\u01b0\"}'),
(11, 'tổng.vốn.đầu.tư', '{\"x\":\"T\\u1ed5ng v\\u1ed1n \\u0111\\u1ea7u t\\u01b0\"}'),
(12, 'tỷ', '{\"x\":\"T\\u1ef7\"}'),
(13, 'tiện.ích.nội.khu', '{\"x\":\"Ti\\u1ec7n \\u00edch n\\u1ed9i khu\"}'),
(14, 'sổ.đỏ', '{\"x\":\"S\\u1ed5 \\u0111\\u1ecf\"}'),
(15, 'từng.nền', '{\"x\":\"T\\u1eeaNG N\\u1ec0N\"}'),
(16, 'xem.thêm', '{\"x\":\"Xem th\\u00eam\"}'),
(17, 'tin.tức', '{\"x\":\"TIN T\\u1ee8C\"}'),
(18, 'tiện.ích.5.sao', '{\"x\":\"TI\\u1ec6N \\u00cdCH 5 SAO\"}'),
(19, 'đạt.chuẩn.quốc.tế', '{\"x\":\"\\u0110\\u1ea0T CHU\\u1ea8N QU\\u1ed0C T\\u1ebe\"}'),
(20, 'lý.do.chọn', '{\"x\":\"L\\u00dd DO CH\\u1eccN\"}'),
(21, 'vị.trí.đắc.địa', '{\"x\":\"V\\u1ecb tr\\u00ed \\u0111\\u1eafc \\u0111\\u1ecba\"}'),
(22, 'tiện.ích.vượt.trội', '{\"x\":\"Ti\\u1ec7n \\u00edch v\\u01b0\\u1ee3t tr\\u1ed9i\"}'),
(23, 'quy.hoạch.đồng.bộ', '{\"x\":\"Quy ho\\u1ea1ch \\u0111\\u1ed3ng b\\u1ed9\"}'),
(24, 'pháp.lý.minh.bạch', '{\"x\":\"Ph\\u00e1p l\\u00fd minh b\\u1ea1ch\"}'),
(25, 'chủ.đầu.tư.cam.kết.đồg.hành.cùng.dự.án', '{\"x\":\"Ch\\u1ee7 \\u0111\\u1ea7u t\\u01b0 cam k\\u1ebft\\n                                \\u0111\\u1ed3g h\\u00e0nh c\\u00f9ng d\\u1ef1 \\u00e1n\"}'),
(26, 'giá.trị.đầu.tư.gia.tăng.vượt.trội', '{\"x\":\"Gi\\u00e1 tr\\u1ecb \\u0111\\u1ea7u t\\u01b0\\n                            gia t\\u0103ng v\\u01b0\\u1ee3t tr\\u1ed9i\"}'),
(27, 'họ.tên', '{\"x\":\"H\\u1ecd t\\u00ean \"}'),
(28, 'email', '{\"x\":\"Email \"}'),
(29, 'số.điện.thoại', '{\"x\":\"S\\u1ed1 \\u0111i\\u1ec7n tho\\u1ea1i \"}'),
(30, 'nội.dung', '{\"x\":\"N\\u1ed9i dung \"}'),
(31, 'đăng.ký', '{\"x\":\"\\u0110\\u0103ng k\\u00fd\"}'),
(32, 'thư.viện', '{\"x\":\"Th\\u01b0 vi\\u1ec7n\"}'),
(33, 'hình.ảnh', '{\"x\":\"H\\u00ecnh \\u1ea3nh\"}'),
(34, 'video', '{\"x\":\"Video\"}'),
(35, 'điện.thoại', '{\"x\":\"\\u0110i\\u1ec7n tho\\u1ea1i \"}'),
(36, 'lời.nhắn', '{\"x\":\"L\\u1eddi nh\\u1eafn \"}'),
(37, 'vị.trí.vàng.giá.trị.vàng', '{\"x\":\"V\\u1eca TR\\u00cd V\\u00c0NG - GI\\u00c1 TR\\u1eca V\\u00c0NG\"}'),
(38, 'mặt.bằng.tổng.thể', '{\"x\":\"M\\u1eb6T B\\u1eb0NG T\\u1ed4NG TH\\u1ec2\"}'),
(39, 'tiện.ích', '{\"x\":\"TI\\u1ec6N \\u00cdCH\"}'),
(40, 'vi', '{\"x\":\"Vi\"}'),
(41, 'en', '{\"x\":\"En\"}'),
(42, 'tham.quan.dự.án', '{\"x\":\"Tham quan d\\u1ef1 \\u00e1n\"}'),
(43, 'liên.hệ', '{\"x\":\"Li\\u00ean h\\u1ec7\"}'),
(44, 'email.đã.được.gửi.đi.thành.công', '{\"x\":\"Email \\u0111\\u00e3 \\u0111\\u01b0\\u1ee3c g\\u1eedi \\u0111i th\\u00e0nh c\\u00f4ng\"}'),
(45, 'cảm.ơn.quý.khách.đã.liên.hệ', '{\"x\":\"C\\u1ea3m \\u01a1n qu\\u00fd kh\\u00e1ch \\u0111\\u00e3 li\\u00ean h\\u1ec7\"}'),
(46, 'holine', '{\"x\":\"Holine\"}'),
(47, 'hotline', '{\"x\":\"Hotline\"}'),
(48, 'vị.trí.độc.tôn', '{\"x\":\"V\\u1ecb tr\\u00ed \\u0111\\u1ed9c t\\u00f4n\"}'),
(49, 'tiện.ích.nội.khu.đẳng.cấp.5.sao', '{\"x\":\"Ti\\u1ec7n \\u00edch n\\u1ed9i khu \\u0111\\u1eb3ng c\\u1ea5p 5 sao\"}'),
(50, 'phápsổ.đỏ.từng.nền', '{\"x\":\"Ph\\u00e1pS\\u1ed5 \\u0111\\u1ecf t\\u1eebng n\\u1ec1n\"}'),
(51, 'sổ.đỏ.từng.nền', '{\"x\":\"S\\u1ed5 \\u0111\\u1ecf t\\u1eebng n\\u1ec1n\"}'),
(52, 'liên.hệ.đại.lý.phân.phối.dự.án', '{\"x\":\"Li\\u00ean h\\u1ec7 \\u0111\\u1ea1i l\\u00fd ph\\u00e2n ph\\u1ed1i d\\u1ef1 \\u00e1n\"}'),
(53, 'đăng.ký.nhận.thông.tin', '{\"x\":\"\\u0110\\u0103ng k\\u00fd nh\\u1eadn th\\u00f4ng tin\"}'),
(54, 'liên.hệ.đại.lý.phân.phối', '{\"x\":\"Li\\u00ean h\\u1ec7 \\u0111\\u1ea1i l\\u00fd ph\\u00e2n ph\\u1ed1i\"}'),
(55, 'welcome', '{\"x\":\"welcome\"}'),
(56, 'vị.trí.độc.tôn.giá.trị.vàng', '{\"x\":\"V\\u1eca TR\\u00cd \\u0110\\u1ed8C T\\u00d4N - GI\\u00c1 TR\\u1eca V\\u00c0NG\"}'),
(57, 'thông.tin.của.quý.khách.đã.được.gửi.đi', '{\"x\":\"Th\\u00f4ng tin c\\u1ee7a qu\\u00fd kh\\u00e1ch \\u0111\\u00e3 \\u0111\\u01b0\\u1ee3c g\\u1eedi \\u0111i.\"}'),
(58, 'thông.tin.đã.được.gửi.thành.công', '{\"x\":\"Th\\u00f4ng tin \\u0111\\u00e3 \\u0111\\u01b0\\u1ee3c g\\u1eedi th\\u00e0nh c\\u00f4ng\"}');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_event_logs`
--

INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(1, 'error', 'Swift_TransportException: Unable to connect with TLS encryption in /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/EsmtpTransport.php:349\nStack trace:\n#0 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(148): Swift_Transport_EsmtpTransport->doHeloCommand()\n#1 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Mailer.php(65): Swift_Transport_AbstractSmtpTransport->start()\n#2 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(451): Swift_Mailer->send(Object(Swift_Message), Array)\n#3 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/october/rain/src/Mail/Mailer.php(115): Illuminate\\Mail\\Mailer->sendSwiftMessage(Object(Swift_Message))\n#4 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(221): October\\Rain\\Mail\\Mailer->send(\'laminsanneh.fle...\', Array, Object(Closure))\n#5 /Applications/XAMPP/xamppfiles/htdocs/megacity/plugins/laminsanneh/flexicontact/components/ContactForm.php(118): Illuminate\\Support\\Facades\\Facade::__callStatic(\'send\', Array)\n#6 /Applications/XAMPP/xamppfiles/htdocs/megacity/modules/cms/Classes/ComponentBase.php(187): LaminSanneh\\FlexiContact\\components\\ContactForm->onMailSent()\n#7 /Applications/XAMPP/xamppfiles/htdocs/megacity/modules/cms/Classes/Controller.php(849): Cms\\Classes\\ComponentBase->runAjaxHandler(\'onMailSent\')\n#8 /Applications/XAMPP/xamppfiles/htdocs/megacity/modules/cms/Classes/Controller.php(740): Cms\\Classes\\Controller->runAjaxHandler(\'contactForm::on...\')\n#9 /Applications/XAMPP/xamppfiles/htdocs/megacity/modules/cms/Classes/Controller.php(373): Cms\\Classes\\Controller->execAjaxHandlers()\n#10 /Applications/XAMPP/xamppfiles/htdocs/megacity/modules/cms/Classes/Controller.php(224): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#11 /Applications/XAMPP/xamppfiles/htdocs/megacity/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'lien-he\')\n#12 [internal function]: Cms\\Classes\\CmsController->run(\'lien-he\')\n#13 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#14 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#15 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#16 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#17 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#18 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /Applications/XAMPP/xamppfiles/htdocs/megacity/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#21 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#24 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#27 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#39 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#40 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#41 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#42 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#43 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#46 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#50 /Applications/XAMPP/xamppfiles/htdocs/megacity/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#51 /Applications/XAMPP/xamppfiles/htdocs/megacity/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#52 {main}', NULL, '2019-10-06 01:41:24', '2019-10-06 01:41:24');

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_files`
--

INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
(50, '5d257e0e0125d223383567.jpg', '7.jpg', 341875, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 50, '2019-07-09 22:56:30', '2019-07-09 22:56:30'),
(51, '5d257ed128ba0461877453.jpg', 'bg.jpg', 557891, 'image/jpeg', NULL, NULL, 'introduction_bg', '1', 'Alipo\\Page\\Models\\Home', 1, 51, '2019-07-09 22:59:45', '2019-07-09 22:59:45'),
(59, '5d25842aa7f61313996764.jpg', '2.jpg', 242484, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 59, '2019-07-09 23:22:34', '2019-07-09 23:22:34'),
(60, '5d25842aee66a086447043.jpg', '1.jpg', 326844, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 60, '2019-07-09 23:22:34', '2019-07-09 23:22:34'),
(61, '5d25842c556bd922293616.jpg', '3.jpg', 257157, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 61, '2019-07-09 23:22:36', '2019-07-09 23:22:36'),
(77, '5d25892904ec8136201594.png', 'SMCT-Logo.png', 11008, 'image/png', NULL, NULL, NULL, NULL, NULL, 1, 77, '2019-07-09 23:43:53', '2019-07-09 23:43:53'),
(78, '5d258931397b6898930978.png', 'SMCT-Footer-Logo.png', 57243, 'image/png', NULL, NULL, NULL, NULL, NULL, 1, 78, '2019-07-09 23:44:01', '2019-07-09 23:44:01'),
(92, '5d25ae1dad91d156498714.jpg', 'photo-2-1560397511124669000399.jpg', 162615, 'image/jpeg', NULL, NULL, 'thumbnail', '1', 'Alipo\\Post\\Models\\Post', 1, 92, '2019-07-10 02:21:33', '2019-07-10 02:22:03'),
(111, '5d25fb93de20d380152182.jpg', 'd22b3aee38f8dca685e9.jpg', 485549, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 111, '2019-07-10 07:52:03', '2019-07-10 07:52:03'),
(112, '5d25fbc2ebdea406231739.jpg', '05Kita_Trucchinh.jpg', 385075, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Location', 1, 112, '2019-07-10 07:52:50', '2019-07-10 07:52:54'),
(113, '5d25fbc446d59722492570.jpg', '852c291b2b0dcf53961c.jpg', 416618, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Location', 1, 113, '2019-07-10 07:52:52', '2019-07-10 07:52:54'),
(114, '5d25fbc670011323252228.jpg', 'd22b3aee38f8dca685e9.jpg', 485549, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Location', 1, 114, '2019-07-10 07:52:54', '2019-07-10 07:53:41'),
(115, '5d25fc93d975d766775778.jpg', '852c291b2b0dcf53961c.jpg', 416618, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Facility', 1, 115, '2019-07-10 07:56:19', '2019-07-10 07:56:43'),
(116, '5d25fc9419b00621608405.jpg', '05Kita_Trucchinh.jpg', 385075, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Facility', 1, 116, '2019-07-10 07:56:20', '2019-07-10 07:56:43'),
(117, '5d25fc965bdf0199898009.jpg', 'd22b3aee38f8dca685e9.jpg', 485549, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Facility', 1, 117, '2019-07-10 07:56:22', '2019-07-10 07:56:43'),
(118, '5d25fce754ae6126512454.jpg', 'd22b3aee38f8dca685e9.jpg', 485549, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\TypeOfHouse', 1, 118, '2019-07-10 07:57:43', '2019-07-10 07:57:45'),
(119, '5d25fce79d824263844859.jpg', '852c291b2b0dcf53961c.jpg', 416618, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\TypeOfHouse', 1, 119, '2019-07-10 07:57:43', '2019-07-10 07:57:45'),
(120, '5d25fd112dad7737347672.jpg', '04Kita_MasterplanNight.jpg', 689216, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\News', 1, 120, '2019-07-10 07:58:25', '2019-07-10 07:58:26'),
(121, '5d25fd3680ebc025992147.jpg', '852c291b2b0dcf53961c.jpg', 416618, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Contact', 1, 121, '2019-07-10 07:59:02', '2019-07-10 07:59:03'),
(123, '5d25fe1926404771141444.jpg', 'map1.jpg', 910805, 'image/jpeg', NULL, NULL, 'postion_image', '1', 'Alipo\\Page\\Models\\Location', 1, 123, '2019-07-10 08:02:49', '2019-07-10 08:02:51'),
(130, '5d2700ff8dc15231526258.jpg', 'Vườn-bonsai_.jpg', 2252526, 'image/jpeg', NULL, NULL, 'images', '4', 'Alipo\\Page\\Models\\FacilityDetail', 1, 130, '2019-07-11 02:27:27', '2019-07-11 02:27:29'),
(131, '5d270110db48c317373879.jpg', 'TT-Hội-nghị.jpg', 2324028, 'image/jpeg', NULL, NULL, 'images', '3', 'Alipo\\Page\\Models\\FacilityDetail', 1, 131, '2019-07-11 02:27:46', '2019-07-11 02:27:49'),
(133, '5d270138ea399983046274.jpg', 'TT-TM-KS.jpg', 703008, 'image/jpeg', NULL, NULL, 'images', '2', 'Alipo\\Page\\Models\\FacilityDetail', 1, 133, '2019-07-11 02:28:24', '2019-07-11 02:28:25'),
(134, '5d27020c4347c872371729.jpg', '22222.jpg', 2299145, 'image/jpeg', NULL, NULL, 'images', '1', 'Alipo\\Page\\Models\\FacilityDetail', 1, 134, '2019-07-11 02:31:57', '2019-07-11 02:31:58'),
(137, '5d27fbf158e27731951748.jpg', '7.jpg', 341875, 'image/jpeg', NULL, NULL, 'picture', '1', 'Alipo\\Project\\Models\\Project', 1, 137, '2019-07-11 20:18:09', '2019-07-11 20:18:12'),
(138, '5d2edcd29d4f0541789597.jpg', 'Stella-1.jpg', 45836, 'image/jpeg', NULL, NULL, 'thumbnail', '4', 'Alipo\\Post\\Models\\Post', 1, 138, '2019-07-17 01:31:14', '2019-07-17 01:33:35'),
(140, '5d2ee0b929304279025407.jpg', 'Stella.jpg', 55544, 'image/jpeg', NULL, NULL, 'thumbnail', '5', 'Alipo\\Post\\Models\\Post', 1, 140, '2019-07-17 01:47:53', '2019-07-17 01:48:48'),
(141, '5d2ee21c11b3f397676136.jpg', 'stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do.jpg', 192851, 'image/jpeg', NULL, NULL, 'thumbnail', '6', 'Alipo\\Post\\Models\\Post', 1, 141, '2019-07-17 01:53:48', '2019-07-17 01:54:36'),
(142, '5d2ee33c0abee060790004.jpg', 'stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do.jpg', 192851, 'image/jpeg', NULL, NULL, 'thumbnail', '7', 'Alipo\\Post\\Models\\Post', 1, 142, '2019-07-17 01:58:36', '2019-07-17 01:58:44'),
(143, '5d2ee43f9e09c979157473.jpg', 'megacity3_oeso.jpg', 142021, 'image/jpeg', NULL, NULL, 'thumbnail', '8', 'Alipo\\Post\\Models\\Post', 1, 143, '2019-07-17 02:02:55', '2019-07-17 02:02:56'),
(144, '5d2ee4c21767b619566222.jpg', 'stella-mega-city-can-tho-phon-vinh-cuoc-song-tay-do-1.jpg', 190191, 'image/jpeg', NULL, NULL, 'thumbnail', '9', 'Alipo\\Post\\Models\\Post', 1, 144, '2019-07-17 02:05:06', '2019-07-17 02:05:07'),
(145, '5d3d2d73677e7763405685.jpg', 'Massage_02.jpg', 176848, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 145, '2019-07-27 22:06:59', '2019-07-27 22:06:59'),
(150, '5d3d3017ec30d966733669.jpg', 'facilities-slider-01.jpg', 519985, 'image/jpeg', NULL, NULL, 'process_image', '1', 'Alipo\\Page\\Models\\Process', 1, 150, '2019-07-27 22:18:15', '2019-07-27 22:19:02'),
(151, '5d3d30180aa69289280741.jpg', 'facilities-slider-02.jpg', 505379, 'image/jpeg', NULL, NULL, 'process_image', '1', 'Alipo\\Page\\Models\\Process', 1, 151, '2019-07-27 22:18:16', '2019-07-27 22:19:02'),
(152, '5d3d30186dc14492035843.jpg', 'facilities-slider-03.jpg', 519985, 'image/jpeg', NULL, NULL, 'process_image', '1', 'Alipo\\Page\\Models\\Process', 1, 152, '2019-07-27 22:18:16', '2019-07-27 22:19:02'),
(153, '5d3d30188eca7034476900.jpg', 'facilities-slider-04.jpg', 309219, 'image/jpeg', NULL, NULL, 'process_image', '1', 'Alipo\\Page\\Models\\Process', 1, 153, '2019-07-27 22:18:16', '2019-07-27 22:19:02'),
(159, '5d47c511adbcf078279971.jpg', 'Untitled-1-2.jpg', 1725126, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Process', 1, 159, '2019-08-04 22:56:33', '2019-08-04 22:56:38'),
(160, '5d4de57a84986327268141.jpg', '5d25fa831ad47647141332.jpg', 385075, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 160, '2019-08-09 14:28:26', '2019-08-09 14:28:26'),
(161, '5d4de57a8d9fb356711160.jpg', '5d25fa877b04e593051331 (1).jpg', 485549, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 161, '2019-08-09 14:28:26', '2019-08-09 14:28:26'),
(162, '5d4de57d7c844287031660.jpg', '5d25fa8602796032361536.jpg', 416618, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 162, '2019-08-09 14:28:29', '2019-08-09 14:28:29'),
(163, '5d4de57d7dd41450931039.jpg', '5d25fa877b04e593051331.jpg', 485549, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 163, '2019-08-09 14:28:29', '2019-08-09 14:28:29'),
(174, '5d4df32b7d6ac208181142.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_1', '1', 'Alipo\\Page\\Models\\Home', 1, 174, '2019-08-09 15:26:51', '2019-08-09 15:27:26'),
(175, '5d4df3381bf3d994116088.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_2', '1', 'Alipo\\Page\\Models\\Home', 1, 175, '2019-08-09 15:27:04', '2019-08-09 15:27:26'),
(176, '5d4df33c8e8d7006212761.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_3', '1', 'Alipo\\Page\\Models\\Home', 1, 176, '2019-08-09 15:27:08', '2019-08-09 15:27:26'),
(177, '5d4df33f2ba5e001770649.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_4', '1', 'Alipo\\Page\\Models\\Home', 1, 177, '2019-08-09 15:27:11', '2019-08-09 15:27:26'),
(178, '5d4df342d7118157418731.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_5', '1', 'Alipo\\Page\\Models\\Home', 1, 178, '2019-08-09 15:27:14', '2019-08-09 15:27:26'),
(179, '5d4df345acf5b847107676.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_6', '1', 'Alipo\\Page\\Models\\Home', 1, 179, '2019-08-09 15:27:17', '2019-08-09 15:27:26'),
(180, '5d4df349933af562724563.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_7', '1', 'Alipo\\Page\\Models\\Home', 1, 180, '2019-08-09 15:27:21', '2019-08-09 15:27:26'),
(181, '5d4df34c9c158892867255.jpg', 'bg-type-of-houses.jpg', 785616, 'image/jpeg', NULL, NULL, 'image_8', '1', 'Alipo\\Page\\Models\\Home', 1, 181, '2019-08-09 15:27:24', '2019-08-09 15:27:26'),
(182, '5d4efd0421e9f918391439.jpg', '8e87bb2caa414e1f1750.jpg', 1541828, 'image/jpeg', NULL, NULL, 'images', '5', 'Alipo\\Page\\Models\\FacilityDetail', 1, 182, '2019-08-10 10:21:10', '2019-08-10 10:21:13'),
(183, '5d4efd343c47c338541903.jpg', '09Kita_TruongQuocTe.jpg', 10610523, 'image/jpeg', NULL, NULL, 'images', '6', 'Alipo\\Page\\Models\\FacilityDetail', 1, 183, '2019-08-10 10:22:01', '2019-08-10 10:22:13'),
(192, '5d5c9398da3a0530914882.png', 'logo.png', 11008, 'image/png', NULL, NULL, 'logo', '1', 'Alipo\\GeneralOption\\Models\\General', 1, 192, '2019-08-20 17:43:04', '2019-08-20 17:43:05'),
(193, '5d60b3230b324377673372.jpg', 'bg-footer.jpg', 88673, 'image/jpeg', NULL, NULL, 'background', '1', 'Alipo\\GeneralOption\\Models\\General', 1, 193, '2019-08-23 20:46:43', '2019-08-23 20:48:13'),
(194, '5d60b32f60d4a439282839.png', 'logo-footer.png', 15414, 'image/png', NULL, NULL, 'footer_logo', '1', 'Alipo\\GeneralOption\\Models\\General', 1, 194, '2019-08-23 20:46:55', '2019-08-23 20:48:13'),
(196, '5d61890d0b192728076040.jpg', 'bg-contact.jpg', 134023, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Gallery', 1, 196, '2019-08-24 11:59:25', '2019-08-24 12:02:05'),
(199, '5d62300420a51539764612.jpg', '5d25fb91c553a010940191.jpg', 385075, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Introduction', 1, 199, '2019-08-24 23:51:48', '2019-08-24 23:51:49'),
(206, '5d62502597c6b484836034.jpg', 'bg-tong-quan.jpg', 244017, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 206, '2019-08-25 02:08:53', '2019-08-25 02:08:53'),
(208, '5d6252f8f0f7c141851123.jpg', 'facilities-slider-04.jpg', 309219, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\ChuDauTu', 1, 208, '2019-08-25 02:20:56', '2019-08-25 02:20:58'),
(216, '5d6d4aad87ee5220719837.png', 'logo-header.png', 31710, 'image/png', NULL, NULL, 'logo', '3', 'Backend\\Models\\BrandSetting', 1, 216, '2019-09-02 10:00:29', '2019-09-02 10:00:56'),
(217, '5d6d4ab55d650738477558.png', 'favicon.png', 7390, 'image/png', NULL, NULL, 'favicon', '3', 'Backend\\Models\\BrandSetting', 1, 217, '2019-09-02 10:00:37', '2019-09-02 10:00:56'),
(218, '5d7287d646351339124236.jpg', '5d4f0040b1e9c147286098.jpg', 623450, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 218, '2019-09-06 09:22:47', '2019-09-06 09:22:47'),
(219, '5d7287d9a2fed400409722.jpg', '5d4f004365dd6862027485.jpg', 672981, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 219, '2019-09-06 09:22:49', '2019-09-06 09:22:50'),
(220, '5d7287da52a67334851487.jpg', '5d4f004375eb4495804463.jpg', 498027, 'image/jpeg', NULL, NULL, NULL, NULL, NULL, 1, 220, '2019-09-06 09:22:50', '2019-09-06 09:22:50'),
(222, '5d728800d4068129619095.png', '5d4f0040bed56118542314.png', 386293, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Home', 1, 222, '2019-09-06 09:23:28', '2019-09-06 09:24:17'),
(223, '5d728802b486f221910567.jpg', '5d4f004365dd6862027485.jpg', 420669, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Home', 1, 223, '2019-09-06 09:23:30', '2019-09-06 09:24:17'),
(224, '5d728803bf8e3180611491.jpg', '5d4f004375eb4495804463.jpg', 308625, 'image/jpeg', NULL, NULL, 'banner', '1', 'Alipo\\Page\\Models\\Home', 1, 224, '2019-09-06 09:23:31', '2019-09-06 09:24:17'),
(225, '5d728a69197bc578753829.jpg', '5d6293e309eca528701155.jpg', 482256, 'image/jpeg', NULL, NULL, 'image', '4', 'Alipo\\Page\\Models\\FacilityDetail', 1, 225, '2019-09-06 09:33:45', '2019-09-06 09:33:47'),
(226, '5d728a78b6371652198696.jpg', 'trungtamhoinghi.jpg', 503976, 'image/jpeg', NULL, NULL, 'image', '3', 'Alipo\\Page\\Models\\FacilityDetail', 1, 226, '2019-09-06 09:34:00', '2019-09-06 09:34:09'),
(227, '5d728a91089ea448269284.jpg', 'quntruong.jpg', 435031, 'image/jpeg', NULL, NULL, 'image', '1', 'Alipo\\Page\\Models\\FacilityDetail', 1, 227, '2019-09-06 09:34:25', '2019-09-06 09:34:30'),
(228, '5d728aa31af80995056871.jpg', 'hethongtruonghoc.jpg', 546404, 'image/jpeg', NULL, NULL, 'image', '6', 'Alipo\\Page\\Models\\FacilityDetail', 1, 228, '2019-09-06 09:34:43', '2019-09-06 09:34:44'),
(230, '5d728ad2c1c35844761192.jpg', 'quntruong.jpg', 435031, 'image/jpeg', NULL, NULL, 'image', '2', 'Alipo\\Page\\Models\\FacilityDetail', 1, 230, '2019-09-06 09:35:30', '2019-09-06 09:35:55'),
(231, '5d728af726fb6052486837.jpg', 'congvien.jpg', 482256, 'image/jpeg', NULL, NULL, 'image', '5', 'Alipo\\Page\\Models\\FacilityDetail', 1, 231, '2019-09-06 09:36:07', '2019-09-06 09:36:08'),
(232, '5d728ce8a4a6c329460421.jpg', '5d27fbb2c4a57943777281.jpg', 690718, 'image/jpeg', NULL, NULL, 'layout', '1', 'Alipo\\Project\\Models\\Project', 1, 232, '2019-09-06 09:44:24', '2019-09-06 09:44:29'),
(233, '5d728cec27512044538188.jpg', '5d27fbb2c4a57943777281.jpg', 690718, 'image/jpeg', NULL, NULL, 'spherical_Face', '1', 'Alipo\\Project\\Models\\Project', 1, 233, '2019-09-06 09:44:28', '2019-09-06 09:44:29'),
(234, '5d91ab906892d292508664.jpg', '178651569593014-1569641443.jpg', 66721, 'image/jpeg', NULL, NULL, 'thumbnail', '10', 'Alipo\\Post\\Models\\Post', 1, 234, '2019-09-30 00:15:28', '2019-09-30 00:15:34'),
(235, '5d91acf97500e110986487.png', 'Screen Shot 2019-09-30 at 2.20.57 PM.png', 227888, 'image/png', NULL, NULL, 'thumbnail', '11', 'Alipo\\Post\\Models\\Post', 1, 235, '2019-09-30 00:21:29', '2019-09-30 00:21:31'),
(236, '5d91b45c3833b194577045.jpg', 'Du-an-moi-tai-Can-Tho-giup-nguoi-dan-cham-tay-den-uoc-mo-cuoc-song-an-lanh--thinh-vuong-can-tho-1-1569567033-8-width660height371.jpg', 152564, 'image/jpeg', NULL, NULL, 'thumbnail', '12', 'Alipo\\Post\\Models\\Post', 1, 236, '2019-09-30 00:53:00', '2019-09-30 00:53:03'),
(238, '5d91b5a72c39e845862854.jpg', '2-1569659750.jpg', 152555, 'image/jpeg', NULL, NULL, 'thumbnail', '13', 'Alipo\\Post\\Models\\Post', 1, 238, '2019-09-30 00:58:31', '2019-09-30 00:58:34'),
(239, '5d91b700986f4424665131.jpg', 'photo-2-15696772333881180126621.jpg', 133941, 'image/jpeg', NULL, NULL, 'thumbnail', '14', 'Alipo\\Post\\Models\\Post', 1, 239, '2019-09-30 01:04:16', '2019-09-30 01:04:19'),
(240, '5d91b8cd16744908093752.jpg', 'photo-2-15696772333881180126621.jpg', 138988, 'image/jpeg', NULL, NULL, 'thumbnail', '15', 'Alipo\\Post\\Models\\Post', 1, 240, '2019-09-30 01:11:57', '2019-09-30 01:11:59'),
(241, '5d91b984b7c81641791438.jpg', '2-1569659750.jpg', 300110, 'image/jpeg', NULL, NULL, 'thumbnail', '16', 'Alipo\\Post\\Models\\Post', 1, 241, '2019-09-30 01:15:00', '2019-09-30 01:15:03');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `options`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2019-07-07 10:08:09', '2019-07-07 10:08:09'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2019-07-07 10:08:09', '2019-07-07 10:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_partials`
--

INSERT INTO `system_mail_partials` (`id`, `name`, `code`, `content_html`, `content_text`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'Header', 'header', '<tr>\n    <td class=\"header\">\n        {% if url %}\n            <a href=\"{{ url }}\">\n                {{ body }}\n            </a>\n        {% else %}\n            <span>\n                {{ body }}\n            </span>\n        {% endif %}\n    </td>\n</tr>', '*** {{ body|trim }} <{{ url }}>', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(2, 'Footer', 'footer', '<tr>\n    <td>\n        <table class=\"footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n            <tr>\n                <td class=\"content-cell\" align=\"center\">\n                    {{ body|md_safe }}\n                </td>\n            </tr>\n        </table>\n    </td>\n</tr>', '-------------------\n{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(3, 'Button', 'button', '<table class=\"action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td align=\"center\">\n                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                            <tr>\n                                <td>\n                                    <a href=\"{{ url }}\" class=\"button button-{{ type ?: \'primary\' }}\" target=\"_blank\">\n                                        {{ body }}\n                                    </a>\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }} <{{ url }}>', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(4, 'Panel', 'panel', '<table class=\"panel\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td class=\"panel-content\">\n            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td class=\"panel-item\">\n                        {{ body|md_safe }}\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(5, 'Table', 'table', '<div class=\"table\">\n    {{ body|md_safe }}\n</div>', '{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(6, 'Subcopy', 'subcopy', '<table class=\"subcopy\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td>\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '-----\n{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(7, 'Promotion', 'promotion', '<table class=\"promotion\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_templates`
--

INSERT INTO `system_mail_templates` (`id`, `code`, `subject`, `description`, `content_html`, `content_text`, `layout_id`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'laminsanneh.flexicontact::emails.message', 'stella-megacity.com', 'Liên Hệ Tham Quan Dự Án', '<h2>Đăng Ký Tham Quan Dự Án</h2>\r\n<p>Name: {{name}}</p>\r\n<p>Email: {{email}}</p>\r\n<p>Phone: {{phone}}</p>\r\n<p>Note: {{note}}</p>', '', 1, 1, '2019-07-23 01:31:20', '2019-09-23 02:10:13'),
(2, 'backend::mail.invite', NULL, 'Invite new admin to the site', NULL, NULL, 2, 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20'),
(3, 'backend::mail.restore', NULL, 'Reset an admin password', NULL, NULL, 2, 0, '2019-07-23 01:31:20', '2019-07-23 01:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0'),
(2, 'cms', 'theme', 'active', '\"cantho\"'),
(3, 'system', 'update', 'retry', '1570433289'),
(4, 'system', 'core', 'build', '455');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'BenFreke.MenuManager', 'script', '1.0.1', 'create_menus_table.php', '2019-07-07 10:08:08'),
(2, 'BenFreke.MenuManager', 'comment', '1.0.1', 'First version of MenuManager', '2019-07-07 10:08:08'),
(3, 'BenFreke.MenuManager', 'comment', '1.0.2', 'Added active state to menu; Added ability to select active menu item; Added controllable depth to component', '2019-07-07 10:08:08'),
(4, 'BenFreke.MenuManager', 'comment', '1.0.3', 'Made it possible for menu items to not link anywhere; Put a check in so the active node must be a child of the parentNode', '2019-07-07 10:08:08'),
(5, 'BenFreke.MenuManager', 'comment', '1.0.4', 'Fixed bug where re-ordering stopped working', '2019-07-07 10:08:08'),
(6, 'BenFreke.MenuManager', 'comment', '1.0.5', 'Moved link creation code into the model in preparation for external links; Brought list item class creation into the model; Fixed typo with default menu class', '2019-07-07 10:08:08'),
(7, 'BenFreke.MenuManager', 'comment', '1.0.6', 'Removed NestedSetModel, thanks @daftspunk', '2019-07-07 10:08:08'),
(8, 'BenFreke.MenuManager', 'script', '1.1.0', 'add_is_external_field.php', '2019-07-07 10:08:08'),
(9, 'BenFreke.MenuManager', 'script', '1.1.0', 'add_link_target_field.php', '2019-07-07 10:08:08'),
(10, 'BenFreke.MenuManager', 'comment', '1.1.0', 'Added ability to link to external sites. Thanks @adisos', '2019-07-07 10:08:08'),
(11, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_1.php', '2019-07-07 10:08:08'),
(12, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_2.php', '2019-07-07 10:08:08'),
(13, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_3.php', '2019-07-07 10:08:08'),
(14, 'BenFreke.MenuManager', 'comment', '1.1.1', 'Added ability to enable/disable individual menu links; Added ability for url parameters & query string; Fixed issue of \"getLinkHref()\" pulling through full page url with parameters rather than the ACTUAL page url', '2019-07-07 10:08:08'),
(15, 'BenFreke.MenuManager', 'comment', '1.1.2', 'Reformatted code for better maintainability and better practises', '2019-07-07 10:08:08'),
(16, 'BenFreke.MenuManager', 'comment', '1.1.3', 'Fixed bug that prevented multiple components displaying', '2019-07-07 10:08:08'),
(17, 'BenFreke.MenuManager', 'comment', '1.2.0', 'Fixed validation and fields bug; Added list classes', '2019-07-07 10:08:08'),
(18, 'BenFreke.MenuManager', 'comment', '1.3.0', 'Added translation ability thanks to @alxy', '2019-07-07 10:08:08'),
(19, 'BenFreke.MenuManager', 'comment', '1.3.1', 'JSON validation of parameters added; Correct active menu now being set based on parameters; PR sent by @whsol, thanks!', '2019-07-07 10:08:08'),
(20, 'BenFreke.MenuManager', 'comment', '1.3.2', 'Fix for param check that is breaking active node lookup. Thanks @alxy', '2019-07-07 10:08:08'),
(21, 'BenFreke.MenuManager', 'comment', '1.3.3', 'Fix for JSON comment having single quotes. Thanks @adisos', '2019-07-07 10:08:08'),
(22, 'BenFreke.MenuManager', 'comment', '1.3.4', 'Fix for JSON validation not firing', '2019-07-07 10:08:08'),
(23, 'BenFreke.MenuManager', 'script', '1.4.0', 'fix_menu_table.php', '2019-07-07 10:08:08'),
(24, 'BenFreke.MenuManager', 'comment', '1.4.0', 'Fix for POST operations. PR by @matissjanis, TR translations (@mahony0) and permission registration (@nnmer)', '2019-07-07 10:08:08'),
(25, 'BenFreke.MenuManager', 'comment', '1.4.1', 'Fixed bug caused by deleting needed method of Menu class. Thanks @MatissJA', '2019-07-07 10:08:08'),
(26, 'BenFreke.MenuManager', 'comment', '1.4.2', 'Fixed bug with URLs not saving correctly', '2019-07-07 10:08:08'),
(27, 'BenFreke.MenuManager', 'comment', '1.4.3', 'Fixed bug where getBaseFileName method was moved to a different object', '2019-07-07 10:08:08'),
(28, 'BenFreke.MenuManager', 'comment', '1.4.4', 'Fixed bug with incorrect labels. Thanks @ribsousa', '2019-07-07 10:08:08'),
(29, 'BenFreke.MenuManager', 'comment', '1.4.5', 'Fixed bug where getBaseFileName method was moved to a different object', '2019-07-07 10:08:08'),
(30, 'BenFreke.MenuManager', 'comment', '1.4.6', 'Merged PRs that fix bug with plugin not working with stable release', '2019-07-07 10:08:08'),
(31, 'BenFreke.MenuManager', 'comment', '1.4.7', 'Merged PR to fix syntax errors with fresh install of 1.4.6. Thanks @devlifeX', '2019-07-07 10:08:08'),
(32, 'BenFreke.MenuManager', 'comment', '1.4.8', 'Merged PR to fix re-order errors. Thanks @CptMeatball', '2019-07-07 10:08:08'),
(33, 'BenFreke.MenuManager', 'comment', '1.5.0', 'Fixed bugs preventing postgres and sqlite support', '2019-07-07 10:08:08'),
(34, 'BenFreke.MenuManager', 'comment', '1.5.1', 'Added homepage to plugin information. Thanks @gergo85', '2019-07-07 10:08:08'),
(35, 'BenFreke.MenuManager', 'comment', '1.5.2', 'Added French translation. Thanks @Glitchbone', '2019-07-07 10:08:08'),
(36, 'BenFreke.MenuManager', 'comment', '1.5.2', 'Added code of conduct', '2019-07-07 10:08:08'),
(37, 'BenFreke.MenuManager', 'comment', '1.5.3', 'Added ability to delete menus. Thanks @osmanzeki', '2019-07-07 10:08:08'),
(38, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2019-07-07 10:08:08'),
(39, 'Alipo.Page', 'script', '1.0.2', 'create_contacts_table.php', '2019-07-07 10:08:08'),
(40, 'Alipo.Page', 'script', '1.0.2', 'create_facilities_table.php', '2019-07-07 10:08:08'),
(41, 'Alipo.Page', 'script', '1.0.2', 'create_homes_table.php', '2019-07-07 10:08:08'),
(42, 'Alipo.Page', 'script', '1.0.2', 'create_introductions_table.php', '2019-07-07 10:08:08'),
(43, 'Alipo.Page', 'script', '1.0.2', 'create_locations_table.php', '2019-07-07 10:08:08'),
(44, 'Alipo.Page', 'script', '1.0.2', 'create_news_table.php', '2019-07-07 10:08:08'),
(45, 'Alipo.Page', 'script', '1.0.2', 'create_type_of_houses_table.php', '2019-07-07 10:08:08'),
(46, 'Alipo.Post', 'script', '1.0.1', 'create_categories_table.php', '2019-07-07 10:08:08'),
(47, 'Alipo.Post', 'script', '1.0.1', 'create_category_posts_table.php', '2019-07-07 10:08:09'),
(48, 'Alipo.Post', 'script', '1.0.1', 'create_posts_table.php', '2019-07-07 10:08:09'),
(49, 'Alipo.GeneralOption', 'script', '1.0.1', 'create_generals_table.php', '2019-07-07 10:08:09'),
(50, 'Alipo.Project', 'script', '1.0.2', 'create_projects_table.php', '2019-07-07 10:08:09'),
(51, 'LaminSanneh.FlexiContact', 'comment', '1.0.0', 'First Version with basic functionality', '2019-07-07 10:08:09'),
(52, 'LaminSanneh.FlexiContact', 'comment', '1.0.1', 'Fixed email subject to send actual subject set in backend', '2019-07-07 10:08:09'),
(53, 'LaminSanneh.FlexiContact', 'comment', '1.0.2', 'Added validation to contact form fields', '2019-07-07 10:08:09'),
(54, 'LaminSanneh.FlexiContact', 'comment', '1.0.3', 'Changed body input field type from text to textarea', '2019-07-07 10:08:09'),
(55, 'LaminSanneh.FlexiContact', 'comment', '1.0.4', 'Updated default component markup to use more appropriate looking twitter bootstrap classes', '2019-07-07 10:08:09'),
(56, 'LaminSanneh.FlexiContact', 'comment', '1.0.5', 'Corrected spelling for Marketing on the backend settings', '2019-07-07 10:08:09'),
(57, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Added ability to include bootstrap or not on component config', '2019-07-07 10:08:09'),
(58, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Add proper validation message outputting', '2019-07-07 10:08:09'),
(59, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Added option to include or exclude main script file', '2019-07-07 10:08:09'),
(60, 'LaminSanneh.FlexiContact', 'comment', '1.0.7', 'Updated contact component default markup file', '2019-07-07 10:08:09'),
(61, 'LaminSanneh.FlexiContact', 'comment', '1.0.7', 'Updated readme file', '2019-07-07 10:08:09'),
(62, 'LaminSanneh.FlexiContact', 'comment', '1.1.0', 'Mail template is now registered properly', '2019-07-07 10:08:09'),
(63, 'LaminSanneh.FlexiContact', 'comment', '1.2.0', 'Add Proper validation that can be localized', '2019-07-07 10:08:09'),
(64, 'LaminSanneh.FlexiContact', 'comment', '1.2.1', 'Added permissions to the settings page. PR by @matissjanis', '2019-07-07 10:08:09'),
(65, 'LaminSanneh.FlexiContact', 'comment', '1.2.2', 'Added polish language features', '2019-07-07 10:08:09'),
(66, 'LaminSanneh.FlexiContact', 'comment', '1.2.3', 'Modified mail templatedefault text', '2019-07-07 10:08:09'),
(67, 'LaminSanneh.FlexiContact', 'comment', '1.3.0', '!!! Added captcha feature, which requires valid google captcha credentials to work', '2019-07-07 10:08:09'),
(68, 'LaminSanneh.FlexiContact', 'comment', '1.3.1', 'Set replyTo instead of from-header when sending', '2019-07-07 10:08:09'),
(69, 'LaminSanneh.FlexiContact', 'comment', '1.3.2', 'Added german translation language file', '2019-07-07 10:08:09'),
(70, 'LaminSanneh.FlexiContact', 'comment', '1.3.3', 'Added option to allow user to enable or disable captcha in contact form', '2019-07-07 10:08:09'),
(71, 'LaminSanneh.FlexiContact', 'comment', '1.3.4', 'Made sure captcha enabling or disabling doesnt produce bug', '2019-07-07 10:08:09'),
(72, 'AnandPatel.WysiwygEditors', 'comment', '1.0.1', 'First version of Wysiwyg Editors.', '2019-07-07 10:08:09'),
(73, 'AnandPatel.WysiwygEditors', 'comment', '1.0.2', 'Automatic Injection to CMS & other code editors and October CMS`s Rich Editor added.', '2019-07-07 10:08:09'),
(74, 'AnandPatel.WysiwygEditors', 'comment', '1.0.3', 'Automatic Injection to RainLab Static Pages & other plugin`s option is appear only if installed.', '2019-07-07 10:08:09'),
(75, 'AnandPatel.WysiwygEditors', 'comment', '1.0.4', 'New Froala editor added (on request from emzero439), Height & width property added for editor, setting moved to My Setting tab and minor changes in settings.', '2019-07-07 10:08:09'),
(76, 'AnandPatel.WysiwygEditors', 'comment', '1.0.5', 'Automatic Injection to Radiantweb`s Problog and ProEvents (option available in settings-content).', '2019-07-07 10:08:09'),
(77, 'AnandPatel.WysiwygEditors', 'comment', '1.0.6', 'CKEditor updated and bug fixes.', '2019-07-07 10:08:09'),
(78, 'AnandPatel.WysiwygEditors', 'comment', '1.0.7', 'Integrated elFinder (file browser) with TinyMCE & CKEditor, Image upload/delete for Froala Editor.', '2019-07-07 10:08:09'),
(79, 'AnandPatel.WysiwygEditors', 'comment', '1.0.8', 'Added security to File Browser`s route (Authenticate users can only access File Browser).', '2019-07-07 10:08:09'),
(80, 'AnandPatel.WysiwygEditors', 'comment', '1.0.9', 'Updated CKEditor, Froala and TinyMCE.', '2019-07-07 10:08:09'),
(81, 'AnandPatel.WysiwygEditors', 'comment', '1.1.0', 'Support multilanguage, update elFinder and cleanup code.', '2019-07-07 10:08:09'),
(82, 'AnandPatel.WysiwygEditors', 'comment', '1.1.1', 'Added Turkish language.', '2019-07-07 10:08:09'),
(83, 'AnandPatel.WysiwygEditors', 'comment', '1.1.2', 'Added Hungarian language.', '2019-07-07 10:08:09'),
(84, 'AnandPatel.WysiwygEditors', 'comment', '1.1.3', 'Fixed issue related to RC (Elfinder fix remaining).', '2019-07-07 10:08:09'),
(85, 'AnandPatel.WysiwygEditors', 'comment', '1.1.4', 'Fixed Elfinder issue.', '2019-07-07 10:08:09'),
(86, 'AnandPatel.WysiwygEditors', 'comment', '1.1.5', 'Updated CKEditor, Froala and TinyMCE.', '2019-07-07 10:08:09'),
(87, 'AnandPatel.WysiwygEditors', 'comment', '1.1.6', 'Changed destination folder.', '2019-07-07 10:08:09'),
(88, 'AnandPatel.WysiwygEditors', 'comment', '1.1.7', 'Added Czech language.', '2019-07-07 10:08:09'),
(89, 'AnandPatel.WysiwygEditors', 'comment', '1.1.8', 'Added Russian language.', '2019-07-07 10:08:09'),
(90, 'AnandPatel.WysiwygEditors', 'comment', '1.1.9', 'Fix hook and other issues (thanks to Vojta Svoboda).', '2019-07-07 10:08:09'),
(91, 'AnandPatel.WysiwygEditors', 'comment', '1.2.0', 'Put settings inside CMS sidemenu (thanks to Amanda Tresbach).', '2019-07-07 10:08:09'),
(92, 'AnandPatel.WysiwygEditors', 'comment', '1.2.1', 'Remove el-finder (Which fix issue of composer), added OC media manager support for image in TinyMCE & CkEditor, TinyMCE version updated, Fix Image upload for froala editor', '2019-07-07 10:08:09'),
(93, 'AnandPatel.WysiwygEditors', 'comment', '1.2.2', 'Add multilingual support, Add new languages, Update the Hungarian language, Add the missing English language files (Special thanks to Szabó Gergő)', '2019-07-07 10:08:09'),
(94, 'AnandPatel.WysiwygEditors', 'comment', '1.2.3', 'Added toolbar customization option (Special thanks to Szabó Gergő).', '2019-07-07 10:08:09'),
(95, 'AnandPatel.WysiwygEditors', 'comment', '1.2.4', 'Added support for Content Plus Plugin & News and Newsletter plugin (thanks to Szabó Gergő)', '2019-07-07 10:08:09'),
(96, 'AnandPatel.WysiwygEditors', 'comment', '1.2.5', 'Minor improvements and bugfixes.', '2019-07-07 10:08:09'),
(97, 'AnandPatel.WysiwygEditors', 'comment', '1.2.6', 'Updated the CKEditor and TinyMCE editors.', '2019-07-07 10:08:09'),
(98, 'AnandPatel.WysiwygEditors', 'comment', '1.2.7', 'Show locale switcher when using multilocale editor.', '2019-07-07 10:08:09'),
(99, 'AnandPatel.WysiwygEditors', 'comment', '1.2.8', 'Added French language', '2019-07-07 10:08:09'),
(100, 'AnandPatel.WysiwygEditors', 'comment', '1.2.9', 'Added permission for preferences', '2019-07-07 10:08:09'),
(101, 'RainLab.Translate', 'script', '1.0.1', 'create_messages_table.php', '2019-07-07 10:08:09'),
(102, 'RainLab.Translate', 'script', '1.0.1', 'create_attributes_table.php', '2019-07-07 10:08:09'),
(103, 'RainLab.Translate', 'script', '1.0.1', 'create_locales_table.php', '2019-07-07 10:08:09'),
(104, 'RainLab.Translate', 'comment', '1.0.1', 'First version of Translate', '2019-07-07 10:08:09'),
(105, 'RainLab.Translate', 'comment', '1.0.2', 'Languages and Messages can now be deleted.', '2019-07-07 10:08:09'),
(106, 'RainLab.Translate', 'comment', '1.0.3', 'Minor updates for latest October release.', '2019-07-07 10:08:09'),
(107, 'RainLab.Translate', 'comment', '1.0.4', 'Locale cache will clear when updating a language.', '2019-07-07 10:08:09'),
(108, 'RainLab.Translate', 'comment', '1.0.5', 'Add Spanish language and fix plugin config.', '2019-07-07 10:08:09'),
(109, 'RainLab.Translate', 'comment', '1.0.6', 'Minor improvements to the code.', '2019-07-07 10:08:09'),
(110, 'RainLab.Translate', 'comment', '1.0.7', 'Fixes major bug where translations are skipped entirely!', '2019-07-07 10:08:09'),
(111, 'RainLab.Translate', 'comment', '1.0.8', 'Minor bug fixes.', '2019-07-07 10:08:09'),
(112, 'RainLab.Translate', 'comment', '1.0.9', 'Fixes an issue where newly created models lose their translated values.', '2019-07-07 10:08:09'),
(113, 'RainLab.Translate', 'comment', '1.0.10', 'Minor fix for latest build.', '2019-07-07 10:08:09'),
(114, 'RainLab.Translate', 'comment', '1.0.11', 'Fix multilingual rich editor when used in stretch mode.', '2019-07-07 10:08:09'),
(115, 'RainLab.Translate', 'comment', '1.1.0', 'Introduce compatibility with RainLab.Pages plugin.', '2019-07-07 10:08:09'),
(116, 'RainLab.Translate', 'comment', '1.1.1', 'Minor UI fix to the language picker.', '2019-07-07 10:08:09'),
(117, 'RainLab.Translate', 'comment', '1.1.2', 'Add support for translating Static Content files.', '2019-07-07 10:08:09'),
(118, 'RainLab.Translate', 'comment', '1.1.3', 'Improved support for the multilingual rich editor.', '2019-07-07 10:08:09'),
(119, 'RainLab.Translate', 'comment', '1.1.4', 'Adds new multilingual markdown editor.', '2019-07-07 10:08:09'),
(120, 'RainLab.Translate', 'comment', '1.1.5', 'Minor update to the multilingual control API.', '2019-07-07 10:08:09'),
(121, 'RainLab.Translate', 'comment', '1.1.6', 'Minor improvements in the message editor.', '2019-07-07 10:08:09'),
(122, 'RainLab.Translate', 'comment', '1.1.7', 'Fixes bug not showing content when first loading multilingual textarea controls.', '2019-07-07 10:08:09'),
(123, 'RainLab.Translate', 'comment', '1.2.0', 'CMS pages now support translating the URL.', '2019-07-07 10:08:09'),
(124, 'RainLab.Translate', 'comment', '1.2.1', 'Minor update in the rich editor and code editor language control position.', '2019-07-07 10:08:09'),
(125, 'RainLab.Translate', 'comment', '1.2.2', 'Static Pages now support translating the URL.', '2019-07-07 10:08:09'),
(126, 'RainLab.Translate', 'comment', '1.2.3', 'Fixes Rich Editor when inserting a page link.', '2019-07-07 10:08:09'),
(127, 'RainLab.Translate', 'script', '1.2.4', 'create_indexes_table.php', '2019-07-07 10:08:09'),
(128, 'RainLab.Translate', 'comment', '1.2.4', 'Translatable attributes can now be declared as indexes.', '2019-07-07 10:08:09'),
(129, 'RainLab.Translate', 'comment', '1.2.5', 'Adds new multilingual repeater form widget.', '2019-07-07 10:08:09'),
(130, 'RainLab.Translate', 'comment', '1.2.6', 'Fixes repeater usage with static pages plugin.', '2019-07-07 10:08:09'),
(131, 'RainLab.Translate', 'comment', '1.2.7', 'Fixes placeholder usage with static pages plugin.', '2019-07-07 10:08:09'),
(132, 'RainLab.Translate', 'comment', '1.2.8', 'Improvements to code for latest October build compatibility.', '2019-07-07 10:08:09'),
(133, 'RainLab.Translate', 'comment', '1.2.9', 'Fixes context for translated strings when used with Static Pages.', '2019-07-07 10:08:09'),
(134, 'RainLab.Translate', 'comment', '1.2.10', 'Minor UI fix to the multilingual repeater.', '2019-07-07 10:08:09'),
(135, 'RainLab.Translate', 'comment', '1.2.11', 'Fixes translation not working with partials loaded via AJAX.', '2019-07-07 10:08:09'),
(136, 'RainLab.Translate', 'comment', '1.2.12', 'Add support for translating the new grouped repeater feature.', '2019-07-07 10:08:09'),
(137, 'RainLab.Translate', 'comment', '1.3.0', 'Added search to the translate messages page.', '2019-07-07 10:08:09'),
(138, 'RainLab.Translate', 'script', '1.3.1', 'builder_table_update_rainlab_translate_locales.php', '2019-07-07 10:08:09'),
(139, 'RainLab.Translate', 'script', '1.3.1', 'seed_all_tables.php', '2019-07-07 10:08:09'),
(140, 'RainLab.Translate', 'comment', '1.3.1', 'Added reordering to languages', '2019-07-07 10:08:09'),
(141, 'RainLab.Translate', 'comment', '1.3.2', 'Improved compatibility with RainLab.Pages, added ability to scan Mail Messages for translatable variables.', '2019-07-07 10:08:09'),
(142, 'RainLab.Translate', 'comment', '1.3.3', 'Fix to the locale picker session handling in Build 420 onwards.', '2019-07-07 10:08:09'),
(143, 'RainLab.Translate', 'comment', '1.3.4', 'Add alternate hreflang elements and adds prefixDefaultLocale setting.', '2019-07-07 10:08:09'),
(144, 'RainLab.Translate', 'comment', '1.3.5', 'Fix MLRepeater bug when switching locales.', '2019-07-07 10:08:09'),
(145, 'RainLab.Translate', 'comment', '1.3.6', 'Fix Middleware to use the prefixDefaultLocale setting introduced in 1.3.4', '2019-07-07 10:08:09'),
(146, 'RainLab.Translate', 'comment', '1.3.7', 'Fix config reference in LocaleMiddleware', '2019-07-07 10:08:09'),
(147, 'RainLab.Translate', 'comment', '1.3.8', 'Keep query string when switching locales', '2019-07-07 10:08:09'),
(148, 'RainLab.Translate', 'comment', '1.4.0', 'Add importer and exporter for messages', '2019-07-07 10:08:09'),
(149, 'RainLab.Translate', 'comment', '1.4.1', 'Updated Hungarian translation. Added Arabic translation. Fixed issue where default texts are overwritten by import. Fixed issue where the language switcher for repeater fields would overlap with the first repeater row.', '2019-07-07 10:08:09'),
(150, 'RainLab.Translate', 'comment', '1.4.2', 'Add multilingual MediaFinder', '2019-07-07 10:08:09'),
(151, 'RainLab.Translate', 'comment', '1.4.3', '!!! Please update OctoberCMS to Build 444 before updating this plugin. Added ability to translate CMS Pages fields (e.g. title, description, meta-title, meta-description)', '2019-07-07 10:08:09'),
(152, 'RainLab.Translate', 'comment', '1.4.4', 'Minor improvements to compatibility with Laravel framework.', '2019-07-07 10:08:09'),
(153, 'RainLab.Translate', 'comment', '1.4.5', 'Fixed issue when using the language switcher', '2019-07-07 10:08:09'),
(154, 'RainLab.Translate', 'comment', '1.5.0', 'Compatibility fix with Build 451', '2019-07-07 10:08:09'),
(155, 'RainLab.Translate', 'comment', '1.6.0', 'Make File Upload widget properties translatable. Merge Repeater core changes into MLRepeater widget. Add getter method to retrieve original translate data.', '2019-07-07 10:08:09'),
(156, 'Alipo.Page', 'script', '1.0.3', 'create_contacts_table.php', '2019-07-08 20:06:40'),
(157, 'Alipo.Page', 'script', '1.0.3', 'create_facilities_table.php', '2019-07-08 20:06:40'),
(158, 'Alipo.Page', 'script', '1.0.3', 'create_homes_table.php', '2019-07-08 20:06:40'),
(159, 'Alipo.Page', 'script', '1.0.3', 'create_introductions_table.php', '2019-07-08 20:06:40'),
(160, 'Alipo.Page', 'script', '1.0.3', 'create_locations_table.php', '2019-07-08 20:06:40'),
(161, 'Alipo.Page', 'script', '1.0.3', 'create_news_table.php', '2019-07-08 20:06:40'),
(162, 'Alipo.Page', 'script', '1.0.3', 'create_type_of_houses_table.php', '2019-07-08 20:06:40'),
(163, 'Alipo.Page', 'script', '1.0.3', 'create_facility_details_table.php', '2019-07-08 20:06:40'),
(164, 'Alipo.Project', 'script', '1.0.3', 'create_projects_table.php', '2019-07-09 00:20:02'),
(165, 'Alipo.Project', 'script', '1.0.4', 'create_projects_table.php', '2019-07-15 17:37:30'),
(166, 'Alipo.Page', 'script', '1.0.4', 'create_contacts_table.php', '2019-07-27 22:05:14'),
(167, 'Alipo.Page', 'script', '1.0.4', 'create_facilities_table.php', '2019-07-27 22:05:14'),
(168, 'Alipo.Page', 'script', '1.0.4', 'create_homes_table.php', '2019-07-27 22:05:14'),
(169, 'Alipo.Page', 'script', '1.0.4', 'create_introductions_table.php', '2019-07-27 22:05:14'),
(170, 'Alipo.Page', 'script', '1.0.4', 'create_locations_table.php', '2019-07-27 22:05:14'),
(171, 'Alipo.Page', 'script', '1.0.4', 'create_news_table.php', '2019-07-27 22:05:14'),
(172, 'Alipo.Page', 'script', '1.0.4', 'create_type_of_houses_table.php', '2019-07-27 22:05:14'),
(173, 'Alipo.Page', 'script', '1.0.4', 'create_facility_details_table.php', '2019-07-27 22:05:14'),
(174, 'Alipo.Page', 'script', '1.0.4', 'create_processes_table.php', '2019-07-27 22:05:14'),
(175, 'Alipo.Page', 'script', '1.0.6', 'create_contacts_table.php', '2019-08-24 11:53:54'),
(176, 'Alipo.Page', 'script', '1.0.6', 'create_facilities_table.php', '2019-08-24 11:53:54'),
(177, 'Alipo.Page', 'script', '1.0.6', 'create_homes_table.php', '2019-08-24 11:53:54'),
(178, 'Alipo.Page', 'script', '1.0.6', 'create_introductions_table.php', '2019-08-24 11:53:54'),
(179, 'Alipo.Page', 'script', '1.0.6', 'create_locations_table.php', '2019-08-24 11:53:54'),
(180, 'Alipo.Page', 'script', '1.0.6', 'create_news_table.php', '2019-08-24 11:53:54'),
(181, 'Alipo.Page', 'script', '1.0.6', 'create_type_of_houses_table.php', '2019-08-24 11:53:54'),
(182, 'Alipo.Page', 'script', '1.0.6', 'create_facility_details_table.php', '2019-08-24 11:53:54'),
(183, 'Alipo.Page', 'script', '1.0.6', 'create_processes_table.php', '2019-08-24 11:53:54'),
(184, 'Alipo.Page', 'script', '1.0.6', 'create_galleries_table.php', '2019-08-24 11:53:54'),
(185, 'Alipo.Page', 'script', '1.0.6', 'create_chu_dau_tus_table.php', '2019-08-24 11:53:54'),
(186, 'Alipo.Page', 'script', '1.0.8', 'create_contacts_table.php', '2019-08-24 11:55:21'),
(187, 'Alipo.Page', 'script', '1.0.8', 'create_facilities_table.php', '2019-08-24 11:55:21'),
(188, 'Alipo.Page', 'script', '1.0.8', 'create_homes_table.php', '2019-08-24 11:55:21'),
(189, 'Alipo.Page', 'script', '1.0.8', 'create_introductions_table.php', '2019-08-24 11:55:21'),
(190, 'Alipo.Page', 'script', '1.0.8', 'create_locations_table.php', '2019-08-24 11:55:21'),
(191, 'Alipo.Page', 'script', '1.0.8', 'create_news_table.php', '2019-08-24 11:55:21'),
(192, 'Alipo.Page', 'script', '1.0.8', 'create_type_of_houses_table.php', '2019-08-24 11:55:21'),
(193, 'Alipo.Page', 'script', '1.0.8', 'create_facility_details_table.php', '2019-08-24 11:55:21'),
(194, 'Alipo.Page', 'script', '1.0.8', 'create_processes_table.php', '2019-08-24 11:55:21'),
(195, 'Alipo.Page', 'script', '1.0.8', 'create_galleries_table.php', '2019-08-24 11:55:21'),
(196, 'Alipo.Page', 'script', '1.0.8', 'create_chu_dau_tus_table.php', '2019-08-24 11:55:21'),
(197, 'Alipo.Project', 'script', '1.0.8', 'create_projects_table.php', '2019-08-24 11:55:21'),
(198, 'Alipo.Page', 'script', '1.0.9', 'create_contacts_table.php', '2019-08-25 02:08:22'),
(199, 'Alipo.Page', 'script', '1.0.9', 'create_facilities_table.php', '2019-08-25 02:08:22'),
(200, 'Alipo.Page', 'script', '1.0.9', 'create_homes_table.php', '2019-08-25 02:08:22'),
(201, 'Alipo.Page', 'script', '1.0.9', 'create_introductions_table.php', '2019-08-25 02:08:22'),
(202, 'Alipo.Page', 'script', '1.0.9', 'create_locations_table.php', '2019-08-25 02:08:22'),
(203, 'Alipo.Page', 'script', '1.0.9', 'create_news_table.php', '2019-08-25 02:08:22'),
(204, 'Alipo.Page', 'script', '1.0.9', 'create_type_of_houses_table.php', '2019-08-25 02:08:22'),
(205, 'Alipo.Page', 'script', '1.0.9', 'create_facility_details_table.php', '2019-08-25 02:08:22'),
(206, 'Alipo.Page', 'script', '1.0.9', 'create_processes_table.php', '2019-08-25 02:08:22'),
(207, 'Alipo.Page', 'script', '1.0.9', 'create_galleries_table.php', '2019-08-25 02:08:22'),
(208, 'Alipo.Page', 'script', '1.0.9', 'create_chu_dau_tus_table.php', '2019-08-25 02:08:22'),
(209, 'Alipo.Page', 'script', '1.1.0', 'create_contacts_table.php', '2019-09-01 23:27:44'),
(210, 'Alipo.Page', 'script', '1.1.0', 'create_facilities_table.php', '2019-09-01 23:27:44'),
(211, 'Alipo.Page', 'script', '1.1.0', 'create_homes_table.php', '2019-09-01 23:27:44'),
(212, 'Alipo.Page', 'script', '1.1.0', 'create_introductions_table.php', '2019-09-01 23:27:44'),
(213, 'Alipo.Page', 'script', '1.1.0', 'create_locations_table.php', '2019-09-01 23:27:44'),
(214, 'Alipo.Page', 'script', '1.1.0', 'create_news_table.php', '2019-09-01 23:27:44'),
(215, 'Alipo.Page', 'script', '1.1.0', 'create_type_of_houses_table.php', '2019-09-01 23:27:44'),
(216, 'Alipo.Page', 'script', '1.1.0', 'create_facility_details_table.php', '2019-09-01 23:27:44'),
(217, 'Alipo.Page', 'script', '1.1.0', 'create_processes_table.php', '2019-09-01 23:27:44'),
(218, 'Alipo.Page', 'script', '1.1.0', 'create_galleries_table.php', '2019-09-01 23:27:44'),
(219, 'Alipo.Page', 'script', '1.1.0', 'create_chu_dau_tus_table.php', '2019-09-01 23:27:44'),
(220, 'Alipo.Page', 'script', '1.1.1', 'create_contacts_table.php', '2019-09-11 10:49:09'),
(221, 'Alipo.Page', 'script', '1.1.1', 'create_facilities_table.php', '2019-09-11 10:49:09'),
(222, 'Alipo.Page', 'script', '1.1.1', 'create_homes_table.php', '2019-09-11 10:49:09'),
(223, 'Alipo.Page', 'script', '1.1.1', 'create_introductions_table.php', '2019-09-11 10:49:09'),
(224, 'Alipo.Page', 'script', '1.1.1', 'create_locations_table.php', '2019-09-11 10:49:09'),
(225, 'Alipo.Page', 'script', '1.1.1', 'create_news_table.php', '2019-09-11 10:49:09'),
(226, 'Alipo.Page', 'script', '1.1.1', 'create_type_of_houses_table.php', '2019-09-11 10:49:09'),
(227, 'Alipo.Page', 'script', '1.1.1', 'create_facility_details_table.php', '2019-09-11 10:49:09'),
(228, 'Alipo.Page', 'script', '1.1.1', 'create_processes_table.php', '2019-09-11 10:49:09'),
(229, 'Alipo.Page', 'script', '1.1.1', 'create_galleries_table.php', '2019-09-11 10:49:09'),
(230, 'Alipo.Page', 'script', '1.1.1', 'create_chu_dau_tus_table.php', '2019-09-11 10:49:09'),
(231, 'Alipo.Page', 'script', '1.1.2', 'create_contacts_table.php', '2019-09-11 10:49:32'),
(232, 'Alipo.Page', 'script', '1.1.2', 'create_facilities_table.php', '2019-09-11 10:49:32'),
(233, 'Alipo.Page', 'script', '1.1.2', 'create_homes_table.php', '2019-09-11 10:49:32'),
(234, 'Alipo.Page', 'script', '1.1.2', 'create_introductions_table.php', '2019-09-11 10:49:32'),
(235, 'Alipo.Page', 'script', '1.1.2', 'create_locations_table.php', '2019-09-11 10:49:32'),
(236, 'Alipo.Page', 'script', '1.1.2', 'create_news_table.php', '2019-09-11 10:49:32'),
(237, 'Alipo.Page', 'script', '1.1.2', 'create_type_of_houses_table.php', '2019-09-11 10:49:32'),
(238, 'Alipo.Page', 'script', '1.1.2', 'create_facility_details_table.php', '2019-09-11 10:49:32'),
(239, 'Alipo.Page', 'script', '1.1.2', 'create_processes_table.php', '2019-09-11 10:49:32'),
(240, 'Alipo.Page', 'script', '1.1.2', 'create_galleries_table.php', '2019-09-11 10:49:32'),
(241, 'Alipo.Page', 'script', '1.1.2', 'create_chu_dau_tus_table.php', '2019-09-11 10:49:32'),
(242, 'Alipo.Cms', 'script', '3.0.2', 'create_home_pages_table.php', '2019-09-22 08:04:12'),
(243, 'Alipo.Cms', 'script', '3.0.2', 'create_design_pages_table.php', '2019-09-22 08:04:13'),
(244, 'Alipo.Cms', 'script', '3.0.2', 'create_neighbourhood_pages_table.php', '2019-09-22 08:04:13'),
(245, 'Alipo.Cms', 'script', '3.0.2', 'create_unit2_bedrooms_table.php', '2019-09-22 08:04:13'),
(246, 'Alipo.Cms', 'script', '3.0.2', 'create_unit3_bedrooms_table.php', '2019-09-22 08:04:13'),
(247, 'Alipo.Cms', 'script', '3.0.2', 'create_unit4_bedrooms_table.php', '2019-09-22 08:04:13'),
(248, 'Alipo.Cms', 'script', '3.0.2', 'create_general_options_table.php', '2019-09-22 08:04:13'),
(249, 'Alipo.Cms', 'script', '3.0.2', 'create_contact_pages_table.php', '2019-09-22 08:04:13'),
(250, 'Alipo.Cms', 'script', '3.0.2', 'create_developer_pages_table.php', '2019-09-22 08:04:13'),
(251, 'Alipo.Cms', 'script', '3.0.2', 'create_duplexes_table.php', '2019-09-22 08:04:13'),
(252, 'Alipo.Cms', 'script', '3.0.2', 'create_media_pages_table.php', '2019-09-22 08:04:13'),
(253, 'Alipo.Cms', 'script', '3.0.2', 'create_penthouses_table.php', '2019-09-22 08:04:13'),
(254, 'Alipo.Cms', 'script', '3.0.2', 'create_service_pages_table.php', '2019-09-22 08:04:13'),
(255, 'Alipo.Cms', 'script', '3.0.2', 'create_duplex_as_table.php', '2019-09-22 08:04:13'),
(256, 'Alipo.Cms', 'script', '3.0.2', 'create_duplex_b1s_table.php', '2019-09-22 08:04:13'),
(257, 'Alipo.Cms', 'script', '3.0.2', 'create_duplex_bs_table.php', '2019-09-22 08:04:13'),
(258, 'Alipo.Cms', 'script', '3.0.2', 'create_penthouse_as_table.php', '2019-09-22 08:04:13'),
(259, 'Alipo.Cms', 'script', '3.0.2', 'create_penthouse_bs_table.php', '2019-09-22 08:04:13'),
(260, 'Alipo.Cms', 'script', '3.0.2', 'create_unit3_bedroom_type_as_table.php', '2019-09-22 08:04:13'),
(261, 'Alipo.Cms', 'script', '3.0.2', 'create_unit3_bedroom_type_bs_table.php', '2019-09-22 08:04:13'),
(262, 'Alipo.Cms', 'script', '3.0.2', 'create_unit3_bedroom_type_cs_table.php', '2019-09-22 08:04:13'),
(263, 'Utopigs.Seo', 'script', '1.0.1', 'create_data_table.php', '2019-09-22 08:04:14'),
(264, 'Utopigs.Seo', 'comment', '1.0.1', 'First version of Utopigs Seo plugin', '2019-09-22 08:04:14'),
(265, 'Utopigs.Seo', 'comment', '1.0.2', 'Fix bug with elements with nested items', '2019-09-22 08:04:14'),
(266, 'Utopigs.Seo', 'script', '1.1.0', 'create_sitemaps_table.php', '2019-09-22 08:04:14'),
(267, 'Utopigs.Seo', 'comment', '1.1.0', 'Add sitemap functionality', '2019-09-22 08:04:14'),
(268, 'Utopigs.Seo', 'comment', '1.1.0', 'Fix some bugs with nested items', '2019-09-22 08:04:14'),
(269, 'Utopigs.Seo', 'comment', '1.1.1', 'Fix browser render issue', '2019-09-22 08:04:14'),
(270, 'RainLab.Translate', 'comment', '1.6.1', 'Add ability for models to provide translated computed data, add option to disable locale prefix routing', '2019-09-22 08:07:49'),
(271, 'RainLab.Translate', 'comment', '1.6.2', 'Implement localeUrl filter, add per-locale theme configuration support', '2019-09-22 08:07:49'),
(272, 'RainLab.GoogleAnalytics', 'comment', '1.0.1', 'Initialize plugin', '2019-09-22 20:20:53'),
(273, 'RainLab.GoogleAnalytics', 'comment', '1.0.2', 'Fixed a minor bug in the Top Pages widget', '2019-09-22 20:20:53'),
(274, 'RainLab.GoogleAnalytics', 'comment', '1.0.3', 'Minor improvements to the code', '2019-09-22 20:20:53'),
(275, 'RainLab.GoogleAnalytics', 'comment', '1.0.4', 'Fixes a bug where the certificate upload fails', '2019-09-22 20:20:53'),
(276, 'RainLab.GoogleAnalytics', 'comment', '1.0.5', 'Minor fix to support the updated Google Analytics library', '2019-09-22 20:20:53'),
(277, 'RainLab.GoogleAnalytics', 'comment', '1.0.6', 'Fixes dashboard widget using latest Google Analytics library', '2019-09-22 20:20:53'),
(278, 'RainLab.GoogleAnalytics', 'comment', '1.0.7', 'Removes Client ID from settings because the workflow no longer needs it', '2019-09-22 20:20:53'),
(279, 'RainLab.GoogleAnalytics', 'comment', '1.1.0', '!!! Updated to the latest Google API library', '2019-09-22 20:20:53'),
(280, 'RainLab.GoogleAnalytics', 'comment', '1.2.0', 'Update Guzzle library to version 6.0', '2019-09-22 20:20:53'),
(281, 'RainLab.GoogleAnalytics', 'comment', '1.2.1', 'Update the plugin compatibility with RC8 Google API client', '2019-09-22 20:20:53'),
(282, 'RainLab.GoogleAnalytics', 'comment', '1.2.2', 'Improve translations, bump version requirement to PHP 7', '2019-09-22 20:20:53'),
(283, 'RainLab.GoogleAnalytics', 'comment', '1.2.3', 'Added a switch for forceSSL', '2019-09-22 20:20:53'),
(284, 'RainLab.GoogleAnalytics', 'comment', '1.2.4', 'Added permission for dashboard widgets. Added Turkish, Spanish and Estonian translations.', '2019-09-22 20:20:53'),
(285, 'Alipo.GeneralOption', 'script', '1.0.2', 'create_generals_table.php', '2019-10-06 00:05:50'),
(286, 'Alipo.GeneralOption', 'script', '1.0.2', 'create_contact_infos_table.php', '2019-10-06 00:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'BenFreke.MenuManager', '1.5.3', '2019-07-07 10:08:08', 0, 0),
(2, 'October.Demo', '1.0.1', '2019-07-07 10:08:08', 0, 0),
(3, 'Alipo.Page', '1.1.2', '2019-09-11 10:49:32', 0, 0),
(4, 'Alipo.Post', '1.0.1', '2019-07-07 10:08:09', 0, 0),
(5, 'Alipo.GeneralOption', '1.0.2', '2019-10-06 00:05:50', 0, 0),
(6, 'Alipo.Project', '1.0.8', '2019-08-24 11:55:21', 0, 0),
(7, 'LaminSanneh.FlexiContact', '1.3.4', '2019-07-07 10:08:09', 0, 0),
(8, 'AnandPatel.WysiwygEditors', '1.2.9', '2019-07-07 10:08:09', 0, 0),
(9, 'RainLab.Translate', '1.6.2', '2019-09-22 08:07:49', 0, 0),
(10, 'Alipo.Cms', '3.0.2', '2019-09-22 08:04:13', 0, 0),
(11, 'Utopigs.Seo', '1.1.1', '2019-09-22 08:04:14', 0, 0),
(12, 'RainLab.GoogleAnalytics', '1.2.4', '2019-09-22 20:20:53', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
(1, 'laminsanneh_flexicontact_settings', '{\"recipient_email\":\"info@kita-group.com.vn1\",\"recipient_name\":\"Megacity Planding page\",\"subject\":\"Mail from Megacity Planding page\",\"confirmation_text\":\"Thanks for your contact!\",\"enable_captcha\":\"0\",\"site_key\":\"\",\"secret_key\":\"\",\"enable_server_captcha_validation\":\"0\"}'),
(2, 'system_mail_settings', '{\"send_mode\":\"smtp\",\"sender_name\":\"Stella Mega City\",\"sender_email\":\"media.kitagroup@gmail.com\",\"sendmail_path\":\"\\/usr\\/sbin\\/sendmail -bs\",\"smtp_address\":\"smtp.gmail.com\",\"smtp_port\":\"587\",\"smtp_user\":\"media.kitagroup@gmail.com\",\"smtp_password\":\"ghrskkdkbbvtxmgq\",\"smtp_authorization\":\"1\",\"smtp_encryption\":\"tls\",\"mailgun_domain\":\"\",\"mailgun_secret\":\"\",\"mandrill_secret\":\"\",\"ses_key\":\"\",\"ses_secret\":\"\",\"ses_region\":\"\"}'),
(3, 'backend_brand_settings', '{\"app_name\":\"Stella Megacity\",\"app_tagline\":\"Stella Megacity\",\"primary_color\":\"#34495e\",\"secondary_color\":\"#e67e22\",\"accent_color\":\"#3498db\",\"menu_mode\":\"inline\",\"custom_css\":\"\"}'),
(4, 'cms_maintenance_settings', '{\"is_enabled\":\"0\",\"cms_page\":\"types-houses-detail.htm\",\"theme_map\":{\"cantho\":\"types-houses-detail.htm\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `utopigs_seo_data`
--

CREATE TABLE `utopigs_seo_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utopigs_seo_sitemaps`
--

CREATE TABLE `utopigs_seo_sitemaps` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `utopigs_seo_sitemaps`
--

INSERT INTO `utopigs_seo_sitemaps` (`id`, `theme`, `data`, `created_at`, `updated_at`) VALUES
(1, 'cantho', '[]', '2019-10-06 01:24:29', '2019-10-06 01:24:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alipo_cms_contact_pages`
--
ALTER TABLE `alipo_cms_contact_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_design_pages`
--
ALTER TABLE `alipo_cms_design_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_developer_pages`
--
ALTER TABLE `alipo_cms_developer_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_duplexes`
--
ALTER TABLE `alipo_cms_duplexes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_duplex_as`
--
ALTER TABLE `alipo_cms_duplex_as`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_duplex_b1s`
--
ALTER TABLE `alipo_cms_duplex_b1s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_duplex_bs`
--
ALTER TABLE `alipo_cms_duplex_bs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_general_options`
--
ALTER TABLE `alipo_cms_general_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_home_pages`
--
ALTER TABLE `alipo_cms_home_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_media_pages`
--
ALTER TABLE `alipo_cms_media_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_neighbourhood_pages`
--
ALTER TABLE `alipo_cms_neighbourhood_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_penthouses`
--
ALTER TABLE `alipo_cms_penthouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_penthouse_as`
--
ALTER TABLE `alipo_cms_penthouse_as`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_penthouse_bs`
--
ALTER TABLE `alipo_cms_penthouse_bs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_service_pages`
--
ALTER TABLE `alipo_cms_service_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_unit2_bedrooms`
--
ALTER TABLE `alipo_cms_unit2_bedrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_unit3_bedrooms`
--
ALTER TABLE `alipo_cms_unit3_bedrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_unit3_bedroom_type_as`
--
ALTER TABLE `alipo_cms_unit3_bedroom_type_as`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_unit3_bedroom_type_bs`
--
ALTER TABLE `alipo_cms_unit3_bedroom_type_bs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_unit3_bedroom_type_cs`
--
ALTER TABLE `alipo_cms_unit3_bedroom_type_cs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_unit4_bedrooms`
--
ALTER TABLE `alipo_cms_unit4_bedrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_generaloption_contact_infos`
--
ALTER TABLE `alipo_generaloption_contact_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_generaloption_generals`
--
ALTER TABLE `alipo_generaloption_generals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_chu_dau_tus`
--
ALTER TABLE `alipo_page_chu_dau_tus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_contacts`
--
ALTER TABLE `alipo_page_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_facilities`
--
ALTER TABLE `alipo_page_facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_facility_details`
--
ALTER TABLE `alipo_page_facility_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_galleries`
--
ALTER TABLE `alipo_page_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_homes`
--
ALTER TABLE `alipo_page_homes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_introductions`
--
ALTER TABLE `alipo_page_introductions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_locations`
--
ALTER TABLE `alipo_page_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_news`
--
ALTER TABLE `alipo_page_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_processes`
--
ALTER TABLE `alipo_page_processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_page_type_of_houses`
--
ALTER TABLE `alipo_page_type_of_houses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_post_categories`
--
ALTER TABLE `alipo_post_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_post_category_posts`
--
ALTER TABLE `alipo_post_category_posts`
  ADD PRIMARY KEY (`post_id`,`category_id`);

--
-- Indexes for table `alipo_post_posts`
--
ALTER TABLE `alipo_post_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_project_projects`
--
ALTER TABLE `alipo_project_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `benfreke_menumanager_menus`
--
ALTER TABLE `benfreke_menumanager_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `benfreke_menumanager_menus_parent_id_index` (`parent_id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_templates_source_index` (`source`),
  ADD KEY `cms_theme_templates_path_index` (`path`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_attributes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_attributes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_attributes_model_type_index` (`model_type`);

--
-- Indexes for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_indexes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_indexes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_indexes_model_type_index` (`model_type`),
  ADD KEY `rainlab_translate_indexes_item_index` (`item`);

--
-- Indexes for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_locales_code_index` (`code`),
  ADD KEY `rainlab_translate_locales_name_index` (`name`);

--
-- Indexes for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_messages_code_index` (`code`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- Indexes for table `utopigs_seo_data`
--
ALTER TABLE `utopigs_seo_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utopigs_seo_sitemaps`
--
ALTER TABLE `utopigs_seo_sitemaps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utopigs_seo_sitemaps_theme_index` (`theme`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alipo_cms_contact_pages`
--
ALTER TABLE `alipo_cms_contact_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_design_pages`
--
ALTER TABLE `alipo_cms_design_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_developer_pages`
--
ALTER TABLE `alipo_cms_developer_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_duplexes`
--
ALTER TABLE `alipo_cms_duplexes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_duplex_as`
--
ALTER TABLE `alipo_cms_duplex_as`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_duplex_b1s`
--
ALTER TABLE `alipo_cms_duplex_b1s`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_duplex_bs`
--
ALTER TABLE `alipo_cms_duplex_bs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_general_options`
--
ALTER TABLE `alipo_cms_general_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_home_pages`
--
ALTER TABLE `alipo_cms_home_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_media_pages`
--
ALTER TABLE `alipo_cms_media_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_neighbourhood_pages`
--
ALTER TABLE `alipo_cms_neighbourhood_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_penthouses`
--
ALTER TABLE `alipo_cms_penthouses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_penthouse_as`
--
ALTER TABLE `alipo_cms_penthouse_as`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_penthouse_bs`
--
ALTER TABLE `alipo_cms_penthouse_bs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_service_pages`
--
ALTER TABLE `alipo_cms_service_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_unit2_bedrooms`
--
ALTER TABLE `alipo_cms_unit2_bedrooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_unit3_bedrooms`
--
ALTER TABLE `alipo_cms_unit3_bedrooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_unit3_bedroom_type_as`
--
ALTER TABLE `alipo_cms_unit3_bedroom_type_as`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_unit3_bedroom_type_bs`
--
ALTER TABLE `alipo_cms_unit3_bedroom_type_bs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_unit3_bedroom_type_cs`
--
ALTER TABLE `alipo_cms_unit3_bedroom_type_cs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_unit4_bedrooms`
--
ALTER TABLE `alipo_cms_unit4_bedrooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_generaloption_contact_infos`
--
ALTER TABLE `alipo_generaloption_contact_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `alipo_generaloption_generals`
--
ALTER TABLE `alipo_generaloption_generals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_chu_dau_tus`
--
ALTER TABLE `alipo_page_chu_dau_tus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_contacts`
--
ALTER TABLE `alipo_page_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_facilities`
--
ALTER TABLE `alipo_page_facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_facility_details`
--
ALTER TABLE `alipo_page_facility_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `alipo_page_galleries`
--
ALTER TABLE `alipo_page_galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_homes`
--
ALTER TABLE `alipo_page_homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_introductions`
--
ALTER TABLE `alipo_page_introductions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_locations`
--
ALTER TABLE `alipo_page_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_news`
--
ALTER TABLE `alipo_page_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_processes`
--
ALTER TABLE `alipo_page_processes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_page_type_of_houses`
--
ALTER TABLE `alipo_page_type_of_houses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_post_categories`
--
ALTER TABLE `alipo_post_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_post_posts`
--
ALTER TABLE `alipo_post_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `alipo_project_projects`
--
ALTER TABLE `alipo_project_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `benfreke_menumanager_menus`
--
ALTER TABLE `benfreke_menumanager_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;

--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=287;

--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `utopigs_seo_data`
--
ALTER TABLE `utopigs_seo_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utopigs_seo_sitemaps`
--
ALTER TABLE `utopigs_seo_sitemaps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
