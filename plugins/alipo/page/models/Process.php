<?php namespace Alipo\Page\Models;

use Model;

/**
 * Process Model
 */
class Process extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_page_processes';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'title',
        'slug',
        'info',
    ];
    public $rules = [
        'title' => 'required',
        'slug' => 'required',
        'info' => 'required',
    ];


    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['info'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'banner' => 'System\Models\File',
        'process_image' => 'System\Models\File',

        
    ];}
