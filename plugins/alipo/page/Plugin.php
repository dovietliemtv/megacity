<?php namespace Alipo\Page;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Page Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Page',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Page', 'page', 'plugins/alipo/page/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Alipo\Page\Components\Home' => 'homePage',
            'Alipo\Page\Components\Introduction' => 'introduction',
            'Alipo\Page\Components\Location' => 'location',
            'Alipo\Page\Components\Facility' => 'facility',
            'Alipo\Page\Components\FacilityDetail' => 'facilitydetail',
            'Alipo\Page\Components\TypeOfHouse' => 'typeofhouse',
            'Alipo\Page\Components\News' => 'news',
            'Alipo\Page\Components\Contact' => 'contact',
            'Alipo\Page\Components\Process' => 'cpprocess',
            'Alipo\Page\Components\ChuDauTuCp' => 'chudautucp',
            'Alipo\Page\Components\GalleryCp' => 'gallerycp',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'alipo.page.*' => [
                'tab' => 'Page',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'page' => [
                'label'       => 'Trang',
                'url'         => Backend::url('alipo/page/home/update/1'),
                'icon'        => 'icon-file-powerpoint-o',
                'permissions' => ['alipo.page.*'],
                'order'       => 500,
                'sideMenu' => [
                    'homepage' => [
                        'label'       => 'Trang chủ',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/home/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'introduction' => [
                        'label'       => 'Tổng quan',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/introduction/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'location' => [
                        'label'       => 'Vị trí',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/location/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'facility' => [
                        'label'       => 'Tiện ích',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/facility/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'typeofhouse' => [
                        'label'       => 'Mặt bằng',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/typeofhouse/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'news' => [
                        'label'       => 'Tin tức',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/news/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'gallery' => [
                        'label'       => 'Thư viện',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/gallery/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'process' => [
                        'label'       => 'Tiến độ',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/process/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'contact' => [
                        'label'       => 'Liên hệ',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/contact/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'chudautu' => [
                        'label'       => 'Chủ đầu tư',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/chudautu/update/1'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Pages'
                    ],
                    'facilitydetail' => [
                        'label'       => 'Những tiện ích',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/page/facilitydetail'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Facilities'
                    ],
                ]
            ],
        ];
    }
}
