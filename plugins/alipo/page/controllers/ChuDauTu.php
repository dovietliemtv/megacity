<?php namespace Alipo\Page\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Chu Dau Tu Back-end Controller
 */
class ChuDauTu extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alipo.Page', 'page', 'chudautu');
    }
}
