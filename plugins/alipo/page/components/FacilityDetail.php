<?php namespace Alipo\Page\Components;

use Cms\Classes\ComponentBase;
use Alipo\Page\Models\FacilityDetail as FacilityDetailModel;

class FacilityDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'FacilityDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun() {
        parent::onRun(); // TODO: Change the autogenerated stub
        $this->page['facilityList'] = FacilityDetailModel::all()->sortByDesc('created_at');
        $this->page['facilityList6'] = FacilityDetailModel::paginate(6)->sortByDesc('created_at');
        
    }
}
