<?php namespace Alipo\Page\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLocationsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_page_locations')){ 
            Schema::create('alipo_page_locations', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->text('node_position');
                $table->text('description');
                $table->text('map_lat');
                $table->text('map_long');
                $table->timestamps();
            });
    
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_page_locations');
    }
}
