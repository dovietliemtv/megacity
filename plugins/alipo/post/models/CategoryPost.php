<?php namespace Alipo\Post\Models;

use Model;

/**
 * CategoryPost Model
 */
class CategoryPost extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_post_category_posts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
