<?php namespace Alipo\GeneralOption\Models;

use Model;

/**
 * General Model
 */
class General extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_generaloption_generals';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'title1',
        'des1',
        'phone',
        'social_network',
        'title2',
        'des2',
        'copyright',
    ];
    public $rules = [
        'title1' => 'required',
        'des1' => 'required',
        'phone' => 'required',
        'social_network' => 'required',
        'title2' => 'required',
        'des2' => 'required',
        'copyright' => 'required',
    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['social_network'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'logo' => 'System\Models\File',
        'background' => 'System\Models\File',
        
    ];
    public $attachMany = [];
}
