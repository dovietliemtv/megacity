<?php namespace Alipo\Project;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Project Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Project',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Project', 'project', 'plugins/alipo/project/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Alipo\Project\Components\Project' => 'project',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'alipo.project.*' => [
                'tab' => 'Project',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'project' => [
                'label'       => 'Mặt bằng',
                'url'         => Backend::url('alipo/project/Project'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.project.*'],
                'order'       => 500,
                'sideMenu' => [
                    'project' => [
                        'label'       => 'Tạo mới',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/project/project/create'),
                        'permissions' => ['alipo.page.page'],
                        'group'       => 'Projects'
                    ],
                ],
            ],
        ];
    }
}
