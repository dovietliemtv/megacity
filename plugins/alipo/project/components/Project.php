<?php namespace Alipo\Project\Components;

use Cms\Classes\ComponentBase;
use Alipo\Project\Models\Project as ProjectModel;

class Project extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Project Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        parent::onRun(); // TODO: Change the autogenerated stub
        $this->page['projectList'] = ProjectModel::all()->sortByDesc('created_at');
        $this->page['projectDetail'] = $this->loadPost();

        $slug = $this->param('slug');
        $this->page['relateProject'] = ProjectModel::where('slug','!=', $slug)->take(10)->get()->sortByDesc('created_at');


    }

    protected function loadPost()
    {
        $slug = $this->param('slug');

        $post = new ProjectModel;

        $post = $post->where('slug', $slug);

        $post = $post->first();

        return $post;
    }

}
