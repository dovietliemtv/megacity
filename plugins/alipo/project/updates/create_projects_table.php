<?php namespace Alipo\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_project_projects')){ 
            Schema::create('alipo_project_projects', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->text('num_floor');
                $table->text('area');
                $table->text('architecture');
                $table->text('description');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_project_projects');
    }
}
